{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https:/elderscrollsng.com/skyrim/dymod.json",
    "title": "Skyrim Special Edition Dymod Manifest",
    "description": "Describes metadata for a dymod.",
    "type": "object",
    "properties": {
        "schemaVersion": {
            "description": "The version number of the schema.",
            "type": "string",
            "pattern": "\\d+(\\.\\d+(\\.\\d+)?)?(-(\\d+|\\w+)(\\.(\\d+|\\w+))*)?"
        },
        "id": {
            "description": "An identifier for the dymod.",
            "type": "string",
            "pattern": "(\\w+|_)(\\w+|_|\\-|\\d+)*"
        },
        "name": {
            "description": "The friendly name of the dymod.",
            "type": "string"
        },
        "description": {
            "description": "A description of the dymod.",
            "type": "string"
        },
        "disabled": {
            "description": "Whether the dymod is disabled.",
            "type": "boolean"
        },
        "authors": {
            "description": "A list of the dymod authors.",
            "type": "array",
            "items": {
                "description": "An author's contact information.",
                "type": "object",
                "properties": {
                    "name": {
                        "description": "The author's full name.",
                        "type": "string"
                    },
                    "email": {
                        "description": "The author's email address.",
                        "type": "string"
                    }
                },
                "required": [
                    "name"
                ]
            }
        },
        "url": {
            "description": "The URL for the dymod's information.",
            "type": "string"
        },
        "version": {
            "description": "The version number of the dymod.",
            "type": "string",
            "pattern": "\\d+(\\.\\d+(\\.\\d+)?)?(-(\\d+|\\w+)(\\.(\\d+|\\w+))*)?"
        },
        "dependencies": {
            "description": "A list of dependencies, dymods and plugins which your dymod depends on.",
            "type": "array",
            "items": {
                "description": "A dependency, a mod your dymod requires to function properly.",
                "type": "object",
                "properties": {
                    "oneOf": {
                        "description": "The names of the dependant mod. At least one must be found.",
                        "type": "array",
                        "items": {
                            "type": "object",
                            "minCount": 1,
                            "properties": {
                                "name": {
                                    "description": "The name of the dependant dymod.",
                                    "type": "string"
                                },
                                "isDymod": {
                                    "description": "Whether the dependency is a dymod. If not a dymod it is assumed to be a plugin name.",
                                    "type": "boolean"
                                },
                                "minVersion": {
                                    "description": "The minimum version of the dependant dymod.",
                                    "type": "string",
                                    "pattern": "\\d+(\\.\\d+(\\.\\d+)?)?(-(\\d+|\\w+)(\\.(\\d+|\\w+))*)?"
                                },
                                "maxVersion": {
                                    "description": "The maximum version of the dependant dymod.",
                                    "type": "string",
                                    "pattern": "\\d+(\\.\\d+(\\.\\d+)?)?(-(\\d+|\\w+)(\\.(\\d+|\\w+))*)?"
                                },
                                "isMaxVersionExclusive": {
                                    "description": "Whether to treat the maximum version as included in the range, or excluded from it.",
                                    "type": "boolean",
                                    "default": "true"
                                },
                                "optional": {
                                    "description": "Whether the dependency is optional.",
                                    "type": "boolean",
                                    "default": "false"
                                }
                            },
                            "required": [
                                "name"
                            ]
                        },
                        "minItems": 1
                    }
                }
            }
        },
        "dependants": {
            "description": "A list of dependant dymods, other dymods which you are declaring depend on yours.",
            "type": "array",
            "items": {
                "description": "A dependant dymod, one which your dymod declares depends on it.",
                "type": "object",
                "properties": {
                    "name": {
                        "description": "The name of the dependant dymod.",
                        "type": "string"
                    },
                    "minVersion": {
                        "description": "The minimum version of the dependant dymod.",
                        "type": "string",
                        "pattern": "\\d+(\\.\\d+(\\.\\d+)?)?(-(\\d+|\\w+)(\\.(\\d+|\\w+))*)?"
                    },
                    "maxVersion": {
                        "description": "The maximum version of the dependant dymod.",
                        "type": "string",
                        "pattern": "\\d+(\\.\\d+(\\.\\d+)?)?(-(\\d+|\\w+)(\\.(\\d+|\\w+))*)?"
                    },
                    "isMaxVersionExclusive": {
                        "description": "Whether to treat the maximum version as included in the range, or excluded from it.",
                        "type": "boolean",
                        "default": "true"
                    }
                },
                "required": [
                    "name"
                ]
            }
        }
    },
    "required": [
        "schemaVersion",
        "id",
        "name"
    ]
}
