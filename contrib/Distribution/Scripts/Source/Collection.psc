scriptName Collection extends ScriptObject hidden

Bool property Empty
    Bool function get()
        return Size == 0
    endFunction
endProperty

Int property Size
    Int function get()
        return __GetSize()
    endFunction
endProperty
Int function __GetSize() native

ValueIterator function GetValueIterator() native

Bool function Clear() native
