scriptName PriorityQueue extends ScriptObject hidden

PriorityQueue function Create() global native

function Enqueue(Int priority, Any value) native

Any function Dequeue() native

Any function Peek() native

Bool property Empty
    Bool function get()
        return Size == 0
    endFunction
endProperty

Int property Size
    Int function get()
        return __GetSize()
    endFunction
endProperty
Int function __GetSize() native
