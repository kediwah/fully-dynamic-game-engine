scriptName MatchResults extends ScriptObject

MatchResult[] property Results
    MatchResult[] function get()
        return __GetResults()
    endFunction
endProperty
MatchResult[] function __GetResults() native

MatchResult function GetByName(String captureName) native
