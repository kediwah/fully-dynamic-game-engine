scriptName StackFrame extends ScriptObject hidden

String property ClassName
    String function get()
        return __GetClassName()
    endFunction
endProperty
String function __GetClassName() native

String property StateName
    String function get()
        return __GetStateName()
    endFunction
endProperty
String function __GetStateName() native

String property FunctionName
    String function get()
        return __GetFunctionName()
    endFunction
endProperty
String function __GetFunctionName() native

Bool property IsNative
    Bool function get()
        return __IsNative()
    endFunction
endProperty
Bool function __IsNative() native

String property ModuleName
    String function get()
        return __GetModuleName()
    endFunction
endProperty
String function __GetModuleName() native

String property FileName
    String function get()
        return __GetFileName()
    endFunction
endProperty
String function __GetFileName() native

Int property LineNumber
    Int function get()
        return __GetLineNumber()
    endFunction
endProperty
Int function __GetLineNumber() native

Int property MemoryOffset
    Int function get()
        return __GetMemoryOffset()
    endFunction
endProperty
Int function __GetMemoryOffset() native
