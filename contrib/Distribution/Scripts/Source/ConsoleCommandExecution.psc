scriptName ConsoleCommandExecution extends ScriptObject hidden

Bool property HasExplicitTarget
    Bool function get()
        return __HasExplicitTarget()
    endFunction
endProperty
Bool function __HasExplicitTarget() native

Bool property HasTarget
    Bool function get()
        return __HasTarget()
    endFunction
endProperty
Bool function __HasTarget() native

Any function GetTarget(String typeName, Any default = None, Bool allowImplicit = true) native

Any function GetParameter(String typeName, String name, Any default = None) native

Bool function HasFlag(String name, String shortName = "") native

Any function NextArgument(String typeName, Any default = None) native

Any property Result
    function set(Any value)
        __SetResult(value)
    endFunction
endProperty
function __SetResult(Any result) native
