scriptName ScriptObject hidden

ScriptObject function CreateNewThis() global native

Error function Initialize(ScriptObject object, String variableName, Any value) global native

Int[] function GetHashCode() native

Bool function Equals(ScriptObject other) native

String function ToString() native

function RegisterForUpdate(Float interval) native

function RegisterForSingleUpdate(Float interval) native

function RegisterForUpdateGameTime(Float interval) native

function RegisterForSingleUpdateGameTime(Float interval) native

function UnregisterForUpdate() native

function UnregisterForUpdateGameTime() native

function RegisterForSleep() native

function UnregisterForSleep() native

function RegisterForAnimationEvent(ObjectReference sender, String eventName) native

function UnregisterForAnimationEvent(ObjectReference sender, String eventName) native

function RegisterForLOS(Actor viewer, ObjectReference target) native

function RegisterForLOSGain(Actor viewer, ObjectReference target) native

function RegisterForLOSLost(Actor viewer, ObjectReference target) native

function UnregisterForLOS(Actor viewer, ObjectReference target) native

function RegisterForTrackedStatsEvent() native

function UnregisterForTrackedStatsEvent() native

event OnInit()
endEvent

event OnUpdate()
endEvent

event OnUpdateGameTime()
endEvent

event OnAnimationEvent(ObjectReference source, String eventName)
endEvent

event OnAnimationEventUnregistered(ObjectReference source, String eventName)
endEvent

event OnSleepStart(Float sleepStartTime, Float desiredSleepEndTime)
endEvent

event OnSleepStop(Bool interrupted)
endEvent

event OnTrackedStatsEvent(String stat, Int statValue)
endEvent

event OnGainLOS(Actor viewer, ObjectReference target)
endEvent

event OnLostLOS(Actor viewer, ObjectReference target)
endEvent
