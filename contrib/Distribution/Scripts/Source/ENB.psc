scriptName ENB extends ScriptObject

ENB function GetENB() global native

Bool property Present
    Bool function get()
        return __IsPresent()
    endFunction
endProperty
Bool function __IsPresent() native

Bool property Enabled
    Bool function get()
        return __IsEnabled()
    endFunction
    function set(Bool value)
        __SetEnabled(value)
    endFunction
endProperty
Bool function __IsEnabled() native
function __SetEnabled(Bool value) native

Int property RuntimeVersion
    Int function get()
        return __GetRuntimeVersion()
    endFunction
endProperty
Int function __GetRuntimeVersion() native

Int property SDKVersion
    Int function get()
        return __GetSDKVersion()
    endFunction
endProperty
Int function __GetSDKVersion() native

Bool property EditorActive
    Bool function get()
        return __IsEditorActive()
    endFunction
endProperty
Bool function __IsEditorActive() native

Bool property EffectsWindowActive
    Bool function get()
        return __IsEffectsWindowActive()
    endFunction
endProperty
Bool function __IsEffectsWindowActive() native

Int property ScreenWidth
    Int function get()
        return __GetScreenWidth()
    endFunction
endProperty
Int function __GetScreenWidth() native

Int property ScreenHeight
    Int function get()
        return __GetScreenHeight()
    endFunction
endProperty
Int function __GetScreenHeight() native

Int property CursorXPosition
    Int function get()
        return __GetCursorXPosition()
    endFunction
endProperty
Int function __GetCursorXPosition() native

Int property CursorYPosition
    Int function get()
        return __GetCursorYPosition()
    endFunction
endProperty
Int function __GetCursorYPosition() native

Bool property LeftMouseButtonClicked
    Bool function get()
        return __IsLeftMouseButtonClicked()
    endFunction
endProperty
Bool function __IsLeftMouseButtonClicked() native

Bool property MiddleMouseButtonClicked
    Bool function get()
        return __IsMiddleMouseButtonClicked()
    endFunction
endProperty
Bool function __IsMiddleMouseButtonClicked() native

Bool property RightMouseButtonClicked
    Bool function get()
        return __IsRightMouseButtonClicked()
    endFunction
endProperty
Bool function __IsRightMouseButtonClicked() native

Any function GetParameter(String fileName, String category, String key) native

Bool function SetParameter(String fileName, String category, String key, Any value) native
