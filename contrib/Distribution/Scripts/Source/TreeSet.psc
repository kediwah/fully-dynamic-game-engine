scriptName TreeSet extends SetCollection hidden

TreeSet function Create() global native

ValueIterator function GetReverseValueIterator() native

ValueIterator function GetReverseValueIteratorFrom(Any value) native
