scriptName FullyDynamicGameEngine hidden

Bool function IsPresent() global
    return GetVersion() != None
endFunction

Version function GetVersion() global native
