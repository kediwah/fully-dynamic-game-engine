scriptName StaticConsoleCommand extends ConsoleCommand

import Debug

String function GetNamespace()
    Trace("Namespace has not been specified on StaticConsoleCommand " + self + ".")
    return ""
endFunction

String function GetName()
    Trace("Name has not been specified on StaticConsoleCommand " + self + ".")
    return ""
endFunction

String function GetShortName()
    return ""
endFunction

event OnExecute(ConsoleCommandExecution exec)
    Trace("Execution handler has not been specified on StaticConsoleCommand " + self + ".")
endEvent
