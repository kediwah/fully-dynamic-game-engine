scriptName DynamicArray extends RandomAccessCollection hidden

DynamicArray function Create(Int initialCapacity = 0) global native

Int property Capacity
    Int function get()
        return __GetCapacity()
    endFunction
endProperty
Int function __GetCapacity() native

function Append(Any value) native

Any function PopBack() native

function Reserve(Int capacity) native

function Resize(Int size) native
