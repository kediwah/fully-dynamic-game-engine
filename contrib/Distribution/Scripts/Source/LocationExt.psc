scriptName LocationExt extends Location

LocationExt function Extend(Location wrapped) global native

LocationExt[] function GetAllParents() native

LocationExt[] function GetChildLocations(Bool recursive = False) native

CellExt[] function GetChildCells(Bool recursive = False) native
