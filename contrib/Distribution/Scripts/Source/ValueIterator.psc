scriptName ValueIterator extends ScriptObject hidden

Bool property Valid
    Bool function get()
        return __IsValid()
    endFunction
endProperty
Bool function __IsValid() native

Bool property HasMore
    Bool function get()
        return __HasMore()
    endFunction
endProperty
Bool function __HasMore() native

Bool property Done
    Bool function get()
        return !HasMore
    endFunction
endProperty

Any property Value
    Any function get()
        return __GetValue()
    endFunction
    function set(Any value)
        SetValue(value)
    endFunction
endProperty
Any function __GetValue() native

Bool function SetValue(Any value) native

Bool function Next() native

Any function GetValueAndNext() native

Any function NextAndGetValue() native
