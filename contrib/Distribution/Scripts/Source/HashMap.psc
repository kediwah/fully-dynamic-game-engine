scriptName HashMap extends MapCollection hidden

HashMap function Create(Int initialCapacity = 0) global native

function Reserve(Int capacity) native

KeyValueIterator function GetReverseKeyValueIterator() native

KeyValueIterator function GetKeyValueIteratorFrom(Any key) native

KeyValueIterator function GetReverseKeyValueIteratorFrom(Any key) native
