scriptName Pair extends ScriptObject hidden

Pair function Create(Any first = None, Any second = None) global native

Any property First
    Any function get()
        return __GetFirst()
    endFunction
endProperty
Any function __GetFirst() native

Any property Second
    Any function get()
        return __GetSecond()
    endFunction
endProperty
Any function __GetSecond() native
