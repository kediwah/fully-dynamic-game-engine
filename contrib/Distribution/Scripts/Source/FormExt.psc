scriptName FormExt hidden

Form function GetFormObject(Form target, String className) global native

Form function GetFormByPortableID(String portableID) global native

Form function GetFormByEditorID(String editorID) global native

String function GetEditorID(Form form) global native
