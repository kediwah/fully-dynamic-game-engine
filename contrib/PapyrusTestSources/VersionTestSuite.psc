scriptName VersionTestSuite extends AutoTestSuite hidden

VersionTestSuite function Create() global
    return ScriptObject.CreateNewThis() as VersionTestSuite
endFunction

function TestConstructorMajorOnly()
    Version v = Version.Create(2)
    Assert.AreIntsEqual(v.Major, 2)
    Assert.AreIntsEqual(v.Minor, 0)
    Assert.AreIntsEqual(v.Patch, 0)
endFunction

function TestConstructorMajorMinor()
    Version v = Version.Create(3, 2)
    Assert.AreIntsEqual(v.Major, 3)
    Assert.AreIntsEqual(v.Minor, 2)
    Assert.AreIntsEqual(v.Patch, 0)
endFunction

function TestConstructorMajorMinorPatch()
    Version v = Version.Create(3, 4, 6)
    Assert.AreIntsEqual(v.Major, 3)
    Assert.AreIntsEqual(v.Minor, 4)
    Assert.AreIntsEqual(v.Patch, 6)
endFunction

function TestInvalidConstructor()
    Assert.IsObjectNone(Version.Create(-1, 4, 6))
    Assert.IsObjectNone(Version.Create(1, -4, 6))
    Assert.IsObjectNone(Version.Create(1, 4, -6))
endFunction
