scriptName RegexTestSuite extends AutoTestSuite hidden

function TestPattern()
    Regex r = Regex.Create("pattern", false)
    Assert.AreStringsEqual(r.Pattern, "pattern")
endFunction

function TestIsOptimized()
    Regex r = Regex.Create("pattern", true)
    Assert.IsTrue(r.IsOptimized)
endFunction

function TestUnoptimizedDefault()
    Regex r = Regex.Create("pattern")
    Assert.IsObjectNotNone(r)
    Assert.IsTrue(r.IsOptimized)
endFunction

function TestIsMatch()
    Regex r = Regex.Create("foo", false)
    Assert.IsTrue(r.IsMatch("testfootest"))
    Assert.IsFalse(r.IsMatch("testfo1test"))
endFunction

function TestIsExactMatch()
    Regex r = Regex.Create("foo", false)
    Assert.IsTrue(r.IsExactMatch("foo"))
    Assert.IsFalse(r.IsExactMatch("fo1"))
    Assert.IsFalse(r.IsExactMatch("testfootest"))
endFunction

; function TestMatchFailure()
;     Regex r = Regex.Create("foo", false)
;     Assert.IsObjectNone(r.Match("testfo1test"))
;     Assert.IsObjectNone(r.Match("fo1"))
; endFunction

; function TestExactMatchFailure()
;     Regex r = Regex.Create("foo", false)
;     Assert.IsObjectNone(r.ExactMatch("fo1"))
;     Assert.IsObjectNone(r.ExactMatch("testfootest"))
; endFunction
