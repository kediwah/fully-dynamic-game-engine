scriptName LinkedListIteratorTestSuite extends AutoTestSuite hidden

function TestEmpty()
    LinkedList array = LinkedList.Create()
    ValueIterator i = array.GetValueIterator()
    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.Value)
    Assert.IsFalse(i.Next())
    Assert.IsFalse(i.GetValueAndNext())
    Assert.IsFalse(i.NextAndGetValue())
endFunction

function TestSingleEntry()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    ValueIterator i = array.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestDoubleEntry()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    ValueIterator i = array.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsTrue(i.Next())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestGetValueAndNext()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    ValueIterator i = array.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.GetValueAndNext().GetInt())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.GetValueAndNext().GetInt())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.GetValueAndNext())
endFunction

function TestEmptyReverse()
    LinkedList array = LinkedList.Create()
    ValueIterator i = array.GetReverseValueIterator()
    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.Value)
    Assert.IsFalse(i.Next())
    Assert.IsFalse(i.GetValueAndNext())
    Assert.IsFalse(i.NextAndGetValue())
endFunction

function TestSingleEntryReverse()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    ValueIterator i = array.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestDoubleEntryReverse()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    ValueIterator i = array.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.Value.GetInt())
    Assert.IsTrue(i.Next())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestGetValueAndNextReverse()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    ValueIterator i = array.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.GetValueAndNext().GetInt())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.GetValueAndNext().GetInt())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.GetValueAndNext())
endFunction

function TestEmptyListIterator()
    LinkedList array = LinkedList.Create()
    LinkedListIterator i = array.GetListIterator()
    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.Value)
    Assert.IsFalse(i.Next())
    Assert.IsFalse(i.GetValueAndNext())
    Assert.IsFalse(i.NextAndGetValue())
endFunction

function TestSingleEntryListIterator()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    LinkedListIterator i = array.GetListIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestDoubleEntryListIterator()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    LinkedListIterator i = array.GetListIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsTrue(i.Next())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestGetValueAndNextListIterator()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    LinkedListIterator i = array.GetListIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.GetValueAndNext().GetInt())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.GetValueAndNext().GetInt())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.GetValueAndNext())
endFunction

function TestEmptyReverseListIterator()
    LinkedList array = LinkedList.Create()
    LinkedListIterator i = array.GetReverseListIterator()
    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.Value)
    Assert.IsFalse(i.Next())
    Assert.IsFalse(i.GetValueAndNext())
    Assert.IsFalse(i.NextAndGetValue())
endFunction

function TestSingleEntryReverseListIterator()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    LinkedListIterator i = array.GetReverseListIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestDoubleEntryReverseListIterator()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    LinkedListIterator i = array.GetReverseListIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.Value.GetInt())
    Assert.IsTrue(i.Next())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestGetValueAndNextReverseListIterator()
    LinkedList array = LinkedList.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    LinkedListIterator i = array.GetReverseListIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.GetValueAndNext().GetInt())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.GetValueAndNext().GetInt())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.GetValueAndNext())
endFunction
