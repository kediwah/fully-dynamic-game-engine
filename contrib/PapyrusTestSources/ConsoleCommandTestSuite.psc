scriptName ConsoleCommandTestSuite extends AutoTestSuite hidden

ConsoleCommand cmd
Bool success

event OnPreTestSuite()
endEvent

event OnPostTestSuite()
endEvent

event OnPreTest()
    success = False
endEvent

event OnPostTest()
    cmd = None
endEvent

event OnExecute(ConsoleCommandExecution exec)
    Assert.IsTrue(exec.HasFlag("notification", "n"))
    Assert.AreIntsEqual(10, exec.GetParameter("Int", "arg").GetInt())
    Assert.AreStringsEqual("10", exec.GetParameter("String", "arg").GetString())
    Assert.AreStringsEqual("Foo", exec.NextArgument("String").GetString())
    Assert.IsTrue(exec.NextArgument("Bool").GetBool())

    success = true
endEvent

function TestConsoleCommand()
    cmd = ConsoleCommand.Create("fdge", "TestCmd")
    Assert.IsObjectNotNone(cmd)
    Assert.IsTrue(cmd.RegisterForExecute())
    Assert.IsTrue(ConsoleCommand.Run("TestCmd --arg=10 -n Foo tRUe"))
    Float start = Utility.GetCurrentRealTime()
    while Utility.GetCurrentRealTime() - start < 1.0 && !success
    endWhile
    Assert.IsTrue(success)
endFunction
