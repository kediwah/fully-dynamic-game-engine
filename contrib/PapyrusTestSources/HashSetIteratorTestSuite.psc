scriptName HashSetIteratorTestSuite extends AutoTestSuite hidden

function TestEmpty()
    HashSet set = HashSet.Create()
    ValueIterator i = set.GetValueIterator()
    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.Value)
    Assert.IsFalse(i.Next())
    Assert.IsFalse(i.GetValueAndNext())
    Assert.IsFalse(i.NextAndGetValue())
endFunction

function TestSingleEntry()
    HashSet set = HashSet.Create()
    set.Put(Any.OfInt(10))
    ValueIterator i = set.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestDoubleEntry()
    HashSet set = HashSet.Create()
    set.Put(Any.OfInt(10))
    set.Put(Any.OfInt(20))
    ValueIterator i = set.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsTrue(i.Next())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestGetValueAndNext()
    HashSet set = HashSet.Create()
    set.Put(Any.OfInt(10))
    set.Put(Any.OfInt(20))
    ValueIterator i = set.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.GetValueAndNext().GetInt())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.GetValueAndNext().GetInt())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.GetValueAndNext())
endFunction

function TestEmptyReverse()
    HashSet set = HashSet.Create()
    ValueIterator i = set.GetReverseValueIterator()
    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.Value)
    Assert.IsFalse(i.Next())
    Assert.IsFalse(i.GetValueAndNext())
    Assert.IsFalse(i.NextAndGetValue())
endFunction

function TestSingleEntryReverse()
    HashSet set = HashSet.Create()
    set.Put(Any.OfInt(10))
    ValueIterator i = set.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestDoubleEntryReverse()
    HashSet set = HashSet.Create()
    set.Put(Any.OfInt(10))
    set.Put(Any.OfInt(20))
    ValueIterator i = set.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.Value.GetInt())
    Assert.IsTrue(i.Next())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestGetValueAndNextReverse()
    HashSet set = HashSet.Create()
    set.Put(Any.OfInt(10))
    set.Put(Any.OfInt(20))
    ValueIterator i = set.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.GetValueAndNext().GetInt())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.GetValueAndNext().GetInt())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.GetValueAndNext())
endFunction
