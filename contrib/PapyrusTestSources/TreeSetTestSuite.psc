scriptName TreeSetTestSuite extends AutoTestSuite hidden

function TestDefaultConstructor()
    TreeSet set = TreeSet.Create()
    Assert.IsObjectNotNone(set)
    Assert.AreIntsEqual(0, set.Size, "Set should have a size of 0.")
    Assert.IsTrue(set.Empty)
endFunction

function TestGetNonExistent()
    TreeSet set = TreeSet.Create()
    Assert.IsObjectNone(set.Get(Any.OfInt(10)))
endFunction

function TestPutGet()
    TreeSet set = TreeSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Set should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Assert.AreIntsEqual(10, set.Get(Any.OfInt(10)).GetInt())
endFunction

function TestPutContains()
    TreeSet set = TreeSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Set should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Assert.IsTrue(set.Contains(Any.OfInt(10)))
endFunction

function TestPutOverwrite()
    TreeSet set = TreeSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Any overwritten = set.Put(Any.OfInt(10))
    Assert.AreIntsEqual(10, overwritten.GetInt())
    Assert.AreIntsEqual(10, set.Get(Any.OfInt(10)).GetInt())
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
endFunction

function TestTryPutGet()
    TreeSet set = TreeSet.Create()
    Assert.IsTrue(set.TryPut(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Assert.AreIntsEqual(10, set.Get(Any.OfInt(10)).GetInt())
endFunction

function TestTryPutOverwrite()
    TreeSet set = TreeSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Assert.IsFalse(set.TryPut(Any.OfInt(10)))
    Assert.AreIntsEqual(10, set.Get(Any.OfInt(10)).GetInt())
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
endFunction

function TestDelete()
    TreeSet set = TreeSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.AreIntsEqual(10, set.Delete(Any.OfInt(10)).GetInt())
    Assert.AreIntsEqual(0, set.Size, "Map should be empty.")
    Assert.IsTrue(set.Empty)
endFunction

