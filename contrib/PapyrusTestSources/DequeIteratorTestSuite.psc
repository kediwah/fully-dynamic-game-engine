scriptName DequeIteratorTestSuite extends AutoTestSuite hidden

function TestEmpty()
    Deque array = Deque.Create()
    ValueIterator i = array.GetValueIterator()
    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.Value)
    Assert.IsFalse(i.Next())
    Assert.IsFalse(i.GetValueAndNext())
    Assert.IsFalse(i.NextAndGetValue())
endFunction

function TestSingleEntry()
    Deque array = Deque.Create()
    array.Append(Any.OfInt(10))
    ValueIterator i = array.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestDoubleEntry()
    Deque array = Deque.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    ValueIterator i = array.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsTrue(i.Next())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestGetValueAndNext()
    Deque array = Deque.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    ValueIterator i = array.GetValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.GetValueAndNext().GetInt())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.GetValueAndNext().GetInt())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.GetValueAndNext())
endFunction

function TestEmptyReverse()
    Deque array = Deque.Create()
    ValueIterator i = array.GetReverseValueIterator()
    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.Value)
    Assert.IsFalse(i.Next())
    Assert.IsFalse(i.GetValueAndNext())
    Assert.IsFalse(i.NextAndGetValue())
endFunction

function TestSingleEntryReverse()
    Deque array = Deque.Create()
    array.Append(Any.OfInt(10))
    ValueIterator i = array.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestDoubleEntryReverse()
    Deque array = Deque.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    ValueIterator i = array.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.Value.GetInt())
    Assert.IsTrue(i.Next())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.Value.GetInt())
    Assert.IsFalse(i.Next())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
endFunction

function TestGetValueAndNextReverse()
    Deque array = Deque.Create()
    array.Append(Any.OfInt(10))
    array.Append(Any.OfInt(20))
    ValueIterator i = array.GetReverseValueIterator()

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(20, i.GetValueAndNext().GetInt())

    Assert.IsFalse(i.Done)
    Assert.IsTrue(i.HasMore)
    Assert.IsTrue(i.Valid)
    Assert.AreIntsEqual(10, i.GetValueAndNext().GetInt())

    Assert.IsTrue(i.Done)
    Assert.IsFalse(i.HasMore)
    Assert.IsFalse(i.Valid)
    Assert.IsObjectNone(i.GetValueAndNext())
endFunction
