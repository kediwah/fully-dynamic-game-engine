scriptName HashMapTestSuite extends AutoTestSuite hidden

function TestMapDefaultConstructor()
    HashMap map = HashMap.Create()
    Assert.IsObjectNotNone(map)
    Assert.AreIntsEqual(0, map.Size, "Map should have a size of 0.")
    Assert.IsTrue(map.Empty)
endFunction

function TestMapReservingConstructor()
    HashMap map = HashMap.Create(16)
    Assert.IsObjectNotNone(map)
    Assert.AreIntsEqual(0, map.Size, "Map should have a size of 0.")
    Assert.IsTrue(map.Empty)
endFunction

function TestMapReserve()
    HashMap map = HashMap.Create(16)
    Assert.IsObjectNotNone(map)
    map.Reserve(16)
endFunction

function TestMapGetNonExistent()
    HashMap map = HashMap.Create()
    Assert.IsObjectNone(map.Get(Any.OfInt(10)))
endFunction

function TestMapPutGet()
    HashMap map = HashMap.Create()
    Assert.IsObjectNone(map.Put(Any.OfInt(10), Any.OfString("Foo")))
    Assert.AreIntsEqual(1, map.Size, "Map should have a size of 1.")
    Assert.IsFalse(map.Empty)
    Assert.AreStringsEqual("Foo", map.Get(Any.OfInt(10)).GetString())
endFunction

function TestMapPutOverwrite()
    HashMap map = HashMap.Create()
    Assert.IsObjectNone(map.Put(Any.OfInt(10), Any.OfString("Foo")))
    Assert.AreIntsEqual(1, map.Size, "Map should have a size of 1.")
    Assert.IsFalse(map.Empty)
    Any overwritten = map.Put(Any.OfInt(10), Any.OfString("Bar"))
    Assert.AreStringsEqual(overwritten.GetString(), "Foo")
    Assert.AreStringsEqual("Bar", map.Get(Any.OfInt(10)).GetString())
    Assert.AreIntsEqual(1, map.Size, "Map should have a size of 1.")
    Assert.IsFalse(map.Empty)
endFunction

function TestMapTryPutGet()
    HashMap map = HashMap.Create()
    Assert.IsTrue(map.TryPut(Any.OfInt(10), Any.OfString("Foo")))
    Assert.AreIntsEqual(1, map.Size, "Map should have a size of 1.")
    Assert.IsFalse(map.Empty)
    Assert.AreStringsEqual("Foo", map.Get(Any.OfInt(10)).GetString())
endFunction

function TestMapTryPutOverwrite()
    HashMap map = HashMap.Create()
    Assert.IsObjectNone(map.Put(Any.OfInt(10), Any.OfString("Foo")))
    Assert.AreIntsEqual(1, map.Size, "Map should have a size of 1.")
    Assert.IsFalse(map.Empty)
    Assert.IsFalse(map.TryPut(Any.OfInt(10), Any.OfString("Bar")))
    Assert.AreStringsEqual("Foo", map.Get(Any.OfInt(10)).GetString())
    Assert.AreIntsEqual(1, map.Size, "Map should have a size of 1.")
    Assert.IsFalse(map.Empty)
endFunction

function TestMapDelete()
    HashMap map = HashMap.Create()
    Assert.IsObjectNone(map.Put(Any.OfInt(10), Any.OfString("Foo")))
    Assert.AreIntsEqual(1, map.Size, "Map should have a size of 1.")
    Assert.AreStringsEqual("Foo", map.Delete(Any.OfInt(10)).GetString())
    Assert.AreIntsEqual(0, map.Size, "Map should be empty.")
    Assert.IsTrue(map.Empty)
endFunction
