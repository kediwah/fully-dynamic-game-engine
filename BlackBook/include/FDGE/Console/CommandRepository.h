#pragma once

#include <articuno/articuno.h>

#include "CommandSpec.h"

namespace FDGE::Console {
    class CommandRepository;

    class CommandRegistration {
    public:
        inline CommandRegistration() noexcept = default;

        CommandRegistration(const CommandRegistration&) = delete;

        inline CommandRegistration(CommandRegistration&& other) noexcept
            : _id(other._id) {
            other._id = 0;
        }

        inline ~CommandRegistration();

        inline CommandRegistration& operator=(CommandRegistration&& other) noexcept {
            _id = other._id;
            other._id = 0;
            return *this;
        }

    protected:
        inline explicit CommandRegistration(uint64_t id) noexcept
                : _id(id) {
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_id, "id");
        }

        uint64_t _id{0};

        friend class articuno::access;
        friend class CommandRepository;
    };

    class __declspec(dllexport) CommandRepository {
    public:
        CommandRegistration Register(const CommandSpec& command);

        CommandRegistration Register(const CommandSpec&& command);

        void RegisterForever(const CommandSpec& command);

        void RegisterForever(const CommandSpec&& command);

        [[nodiscard]] static CommandRepository& GetSingleton() noexcept;

    private:
        inline CommandRepository() noexcept = default;

        uint64_t RegisterImpl(const CommandSpec& command);

        void Unregister(const CommandSpec& command);

        void Unregister(uint64_t id);

        friend class CommandRegistration;
    };

    CommandRegistration::~CommandRegistration() {
        if (_id) {
            CommandRepository::GetSingleton().Unregister(_id);
        }
    }
}
