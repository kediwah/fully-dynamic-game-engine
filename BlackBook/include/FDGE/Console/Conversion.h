#pragma once

#include <FDGE/Skyrim/Concepts.h>
#include <FDGE/Skyrim/FormIndex.h>
#include <FDGE/Skyrim/FormType.h>
#include <gluino/type_traits.h>
#include <RE/Skyrim.h>

namespace FDGE::Console::Conversion {
    struct ConversionData {
        std::string_view Argument;
        std::string_view Name;
        std::string_view ShortName;
    };

    template <class T>
    inline T Convert(const ConversionData& data) noexcept {
        static_assert(::gluino::dependent_false<T>, "Unsupported console command conversion.");
        return T();
    }

    template <class T>
    requires (gluino::string_of<T, char>)
    inline T Convert(const ConversionData& data) {
        return data.Argument.data();
    }

    template <class T>
    requires (gluino::string_view_of<T, char>)
    inline T Convert(const ConversionData& data) noexcept {
        return data.Argument.data();
    }

    template <std::signed_integral T>
    inline T Convert(const ConversionData& data) {
        int radix = 10;
        std::string_view argument = data.Argument;
        if (argument.starts_with("0x") || argument.starts_with("0X")) {
            radix = 16;
            argument = argument.data() + 2;
        } else if (argument.starts_with("-0x") || argument.starts_with("-0X")) {
            radix = 16;
            argument = argument.data() + 3;
            if (argument[0] == '-') {
                throw std::invalid_argument(fmt::format("Expected a signed integer for argument '{}'.", data.Name));
            }
        }
        char* end;
        auto result = std::strtoll(argument.data(), &end, radix);
        if (argument.data() == end) {
            throw std::invalid_argument(fmt::format("Expected a signed integer for argument '{}'.", data.Name));
        }
        if (result > std::numeric_limits<T>::max()) {
            throw std::invalid_argument(
                    fmt::format("Integer {} passed for argument '{}' exceeds the maximum value for its type {}.",
                                argument, data.Name, std::numeric_limits<T>::max()));
        }
        if (result < std::numeric_limits<T>::min()) {
            throw std::invalid_argument(
                    fmt::format("Integer {} passed for argument '{}' exceeds the minimum value for its type {}.",
                                argument, data.Name, std::numeric_limits<T>::min()));
        }
        return static_cast<T>(result);
    }

    template <std::unsigned_integral T>
    inline T Convert(const ConversionData& data) {
        int radix = 10;
        std::string_view argument = data.Argument;
        if (argument.starts_with("0x") || argument.starts_with("0X")) {
            radix = 16;
            argument = argument.data() + 2;
        }
        char* end;
        auto result = std::strtoull(argument.data(), &end, radix);
        if (argument.data() == end) {
            throw std::invalid_argument(fmt::format("Expected an unsigned integer for argument '{}'.", data.Name));
        }
        if (result > std::numeric_limits<T>::max()) {
            throw std::invalid_argument(
                    fmt::format("Integer {} passed for argument '{}' exceeds the maximum value for its type {}.",
                                argument, data.Name, std::numeric_limits<T>::max()));
        }
        return static_cast<T>(result);
    }

    template <std::unsigned_integral T>
    requires (std::same_as<T, bool>)
    bool Convert(const ConversionData& data) {
        gluino::str_equal_to<false> iequals;
        auto argument = data.Argument;
        if (iequals(argument, "true") || iequals(argument, "yes") || iequals(argument, "on")) {
            return true;
        }
        if (iequals(argument, "false") || iequals(argument, "no") || iequals(argument, "off")) {
            return false;
        }
        char* end;
        auto result = std::strtoll(argument.data(), &end, 10);
        if (!result && argument.data() == end) {
            if (data.Name.empty() && data.ShortName.empty()) {
                throw std::invalid_argument("Expected argument to be a boolean.");
            } else if (data.ShortName.empty()) {
                throw std::invalid_argument(fmt::format("Expected a boolean for argument '--{}'.", data.Name));
            }
            throw std::invalid_argument(fmt::format("Expected a boolean for argument '--{}' (or '-{}').",
                                                    data.Name, data.ShortName));
        }
        return result;
    }

    template <class T>
    requires (std::same_as<T, float_t>)
    float_t Convert(const ConversionData& data) {
        char* end;
        auto result = std::strtof(data.Argument.data(), &end);
        if (data.Argument.data() == end) {
            if (data.Name.empty() && data.ShortName.empty()) {
                throw std::invalid_argument("Expected argument to be a float.");
            } else if (data.ShortName.empty()) {
                throw std::invalid_argument(fmt::format("Expected a float for argument '--{}'.", data.Name));
            }
            throw std::invalid_argument(fmt::format("Expected a float for argument '--{}' (or '-{}').",
                                                    data.Name, data.ShortName));
        }
        return result;
    }

    template <class T>
    requires (std::same_as<T, Skyrim::FormType>)
    Skyrim::FormType Convert(const ConversionData& data) {
        try {
            char* end;
            auto result = std::strtoull(data.Argument.data(), &end, 10);
            if (data.Argument.data() + data.Argument.size() == end) {
                return {static_cast<RE::FormType>(result)};
            } else {
                return {data.Argument};
            }
        } catch (const std::invalid_argument&) {
            throw std::invalid_argument(fmt::format("Expected a valid form type by it's integer value, record name, "
                                                    "or friendly name."));
        }
    }

    template <class T>
    requires (std::same_as<T, double_t>)
    double_t Convert(const ConversionData& data) {
        char* end;
        auto result = std::strtof(data.Argument.data(), &end);
        if (data.Argument.data() == end) {
            if (data.Name.empty() && data.ShortName.empty()) {
                throw std::invalid_argument("Expected argument to be a double.");
            } else if (data.ShortName.empty()) {
                throw std::invalid_argument(fmt::format("Expected a double for argument '--{}'.", data.Name));
            }
            throw std::invalid_argument(fmt::format("Expected a double for argument '--{}' (or '-{}').",
                                                    data.Name, data.ShortName));
        }
        return result;
    }

    template <class T>
    requires (RE::BSScript::is_form_pointer_v<T>)
    T Convert(const ConversionData& data) {
        return Convert<gluino::optional_ptr<std::remove_pointer_t<T>>>(data).get_raw();
    }

    static_assert(std::same_as<typename gluino::optional_ptr<RE::Actor>::element_type, RE::Actor>);

    template <gluino::pointer_like T>
    requires (Skyrim::Form<typename T::element_type>)
    T Convert(const ConversionData& data) {
        FDGE::Skyrim::FormIndex index;
        if (data.Argument[0] == '[' && data.Argument[data.Argument.size() - 1] == ']') {
            // Treat as a form ID.
            auto inner = data.Argument.substr(1, data.Argument.size() - 2);
            char* end;
            auto formID = std::strtoul(inner.data() + 1, &end, 16);
            if (end != inner.data() + inner.size()) {
                throw std::invalid_argument(fmt::format("'{}' is not a valid hexadecimal form ID.", inner));
            }
            return index.Lookup<typename T::element_type>(formID);
        }

        std::optional<Skyrim::PortableID> id;
        if (gluino::str_equal_to<false>()(data.Argument, "player")) {
            id = FDGE::Skyrim::PortableID("Skyrim.esm", 0x14);
        } else {
            id = FDGE::Skyrim::PortableID::Parse(data.Argument);
        }
        gluino::optional_ptr<typename T::element_type> result;
        if (id.has_value()) {
            if (id->IsNull()) {
                return nullptr;
            }
            auto foundForm = index.Lookup(id.value());
            if (foundForm.is_valid()) {
                result = foundForm->template As<typename T::element_type>();
            }
        } else {
            char* end;
            RE::FormID formID = std::strtoul(data.Argument.data(), &end, 16);
            if (formID == 0) {
                if (data.Argument.data() == end) {
                    if (data.Name.empty() && data.ShortName.empty()) {
                        throw std::invalid_argument("Expected a form, format should be a hexadecimal form ID, "
                                                    "editor ID, or portable ID.");
                    } else if (data.ShortName.empty()) {
                        throw std::invalid_argument(fmt::format("Argument '--{}' expects a form, format should be a"
                                                                " hexadecimal form ID, editor ID, or portable ID.",
                                                                data.Name));
                    }
                    throw std::invalid_argument(
                            fmt::format("Argument '--{}' (or '-{}') expects a form, format should be a hexadecimal "
                                        "form ID, editor ID, or portable ID.", data.Name, data.ShortName));
                }
                return nullptr;
            }
            result = index.Lookup<typename T::element_type>(formID);
        }
        if (!result) {
            if (data.Name.empty() && data.ShortName.empty()) {
                throw std::invalid_argument("Could not find form identified by '{}'.");
            } else if (data.ShortName.empty()) {
                throw std::invalid_argument(
                        fmt::format("Could not find form identified by '{}' for argument '--{}'.",
                                    data.Argument, data.Name));
            }
            throw std::invalid_argument(
                    fmt::format("Could not find form identified by '{}' for argument '--{}' (or '-{}').",
                                data.Argument, data.Name, data.ShortName));
        }
        return result.get_raw();
    }

    template <gluino::pointer_like T>
    requires (Skyrim::Form<typename T::element_type> &&
              std::same_as<T, gluino::safe_ptr<typename T::element_type>>)
    T Convert(const ConversionData& data) {
        auto result = Convert<typename T::element_type>(data);
        if (!result) {
            if (data.Name.empty() && data.ShortName.empty()) {
                throw std::invalid_argument("Argument requires a non-null form reference.");
            } else if (data.ShortName.empty()) {
                throw std::invalid_argument(
                        fmt::format("Argument '--{}' requires a non-null form reference.", data.Name));
            }
            throw std::invalid_argument(
                    fmt::format("Argument '--{}' (or '-{}') requires a non-null form reference.",
                                data.Name, data.ShortName));
        }
        return result;
    }

    template <class T>
    requires (RE::BSScript::is_array_v<T>)
    T Convert(const ConversionData& data) {
        T results;
        size_t pos;
        auto argument = data.Argument;
        while ((pos = argument.find(",")) != std::string::npos) {
            auto token = argument.substr(0, pos);
            ConversionData nestedData{token, data.Name, data.ShortName};
            results.emplace_back(Convert<typename T::value_type>(nestedData));
            argument = argument.data() + pos;
        }
        ConversionData final{argument, data.Name, data.ShortName};
        results.emplace_back(Convert<typename T::value_type>(final));
        return results;
    }
}
