#pragma once

#include <gluino/extension.h>
#include <FDGE/Util/EventSource.h>
#include <FDGE/Util/Mutex.h>
#include <RE/Skyrim.h>

#include "Dymod/Events.h"
#include "Skyrim/ActorEvent.h"
#include "Skyrim/CellsLoadingEvent.h"
#include "Skyrim/ReferenceLoadedEvent.h"

namespace FDGE {
    namespace detail {
        class SystemManager;
    }

    class SkyrimPluginsLoadedEvent {
    public:
        inline explicit SkyrimPluginsLoadedEvent(bool reload = false) noexcept
                : _reload(reload) {
        }

        /**
         * Whether the Skyrim plugins are being reloaded, which happens if the mod list is altered from the in-game mod
         * menu. A value of <code>false</code> indicates that this is the first load at game startup.
         */
        [[nodiscard]] inline bool IsReload() const noexcept {
            return _reload;
        }

    private:
        bool _reload;
    };

    class SkyrimPluginsUnloadedEvent {
    };

    class SKSEPluginsLoadedEvent {
    };

    class SKSEInitializedEvent {
    };

    class InputLoadedEvent {
    };

    class NewGameEvent {
    };

    class SaveGameEvent {
    public:
        [[nodiscard]] inline const std::filesystem::path& GetSaveName() const noexcept {
            return _saveName;
        }

    protected:
        inline explicit SaveGameEvent(std::filesystem::path&& saveName) :
                _saveName(std::move(saveName)) {
        }

    private:
        std::filesystem::path _saveName;
    };

    class SaveGameLoadingEvent : public SaveGameEvent {
    public:
        inline explicit SaveGameLoadingEvent(std::filesystem::path&& saveName) : SaveGameEvent(std::move(saveName)) {
        }
    };

    class SaveGameLoadedEvent : public SaveGameEvent {
    public:
        inline SaveGameLoadedEvent(std::filesystem::path&& saveName, bool successful) :
                SaveGameEvent(std::move(saveName)), _successful(successful) {
        }

        [[nodiscard]] inline bool IsSuccessful() const noexcept {
            return _successful;
        }

    private:
        bool _successful;
    };

    class GameSavingEvent : public SaveGameEvent {
    public:
        inline explicit GameSavingEvent(std::filesystem::path&& saveName) : SaveGameEvent(std::move(saveName)) {
        }
    };

    class GameSavedEvent : public SaveGameEvent {
    public:
        inline explicit GameSavedEvent(std::filesystem::path&& saveName) : SaveGameEvent(std::move(saveName)) {
        }
    };

    class SaveGameDeletingEvent : public SaveGameEvent {
    public:
        inline explicit SaveGameDeletingEvent(std::string_view saveName) : SaveGameEvent(saveName) {
        }
    };

    class SaveGameDeletedEvent : public SaveGameEvent {
    public:
        inline explicit SaveGameDeletedEvent(std::string_view saveName) : SaveGameEvent(saveName) {
        }
    };

    namespace detail {
#pragma warning(push)
#pragma warning(disable: 4251)
        class __declspec(dllexport) System : public Util::EventSource<SkyrimPluginsLoadedEvent>,
                                             public Util::EventSource<SkyrimPluginsUnloadedEvent>,
                                             public Util::EventSource<SKSEInitializedEvent>,
                                             public Util::EventSource<SKSEPluginsLoadedEvent>,
                                             public Util::EventSource<InputLoadedEvent>,
                                             public Util::EventSource<SaveGameLoadingEvent>,
                                             public Util::EventSource<SaveGameLoadedEvent>,
                                             public Util::EventSource<GameSavingEvent>,
                                             public Util::EventSource<GameSavedEvent>,
                                             public Util::EventSource<NewGameEvent>,
                                             public Util::EventSource<SaveGameDeletingEvent>,
                                             public Util::EventSource<SaveGameDeletedEvent> {
        public:
            using EventSource<SkyrimPluginsLoadedEvent>::Listen;
            using EventSource<SkyrimPluginsLoadedEvent>::ListenForever;
            using EventSource<SkyrimPluginsLoadedEvent>::StopListening;
            using EventSource<SkyrimPluginsUnloadedEvent>::Listen;
            using EventSource<SkyrimPluginsUnloadedEvent>::ListenForever;
            using EventSource<SkyrimPluginsUnloadedEvent>::StopListening;
            using EventSource<SKSEInitializedEvent>::Listen;
            using EventSource<SKSEInitializedEvent>::ListenForever;
            using EventSource<SKSEInitializedEvent>::StopListening;
            using EventSource<SKSEPluginsLoadedEvent>::Listen;
            using EventSource<SKSEPluginsLoadedEvent>::ListenForever;
            using EventSource<SKSEPluginsLoadedEvent>::StopListening;
            using EventSource<InputLoadedEvent>::Listen;
            using EventSource<InputLoadedEvent>::ListenForever;
            using EventSource<InputLoadedEvent>::StopListening;
            using EventSource<SaveGameLoadingEvent>::Listen;
            using EventSource<SaveGameLoadingEvent>::ListenForever;
            using EventSource<SaveGameLoadingEvent>::StopListening;
            using EventSource<SaveGameLoadedEvent>::Listen;
            using EventSource<SaveGameLoadedEvent>::ListenForever;
            using EventSource<SaveGameLoadedEvent>::StopListening;
            using EventSource<GameSavingEvent>::Listen;
            using EventSource<GameSavingEvent>::ListenForever;
            using EventSource<GameSavingEvent>::StopListening;
            using EventSource<GameSavedEvent>::Listen;
            using EventSource<GameSavedEvent>::ListenForever;
            using EventSource<GameSavedEvent>::StopListening;
            using EventSource<NewGameEvent>::Listen;
            using EventSource<NewGameEvent>::ListenForever;
            using EventSource<NewGameEvent>::StopListening;
            using EventSource<SaveGameDeletingEvent>::Listen;
            using EventSource<SaveGameDeletingEvent>::ListenForever;
            using EventSource<SaveGameDeletingEvent>::StopListening;
            using EventSource<SaveGameDeletedEvent>::Listen;
            using EventSource<SaveGameDeletedEvent>::ListenForever;
            using EventSource<SaveGameDeletedEvent>::StopListening;

            System(const System&) = delete;

            System& operator=(const System&) = delete;

            void AfterInitialization(std::function<void()> callback) const;

            void AfterDataLoaded(std::function<void()> callback) const;

            template <class T>
            inline void RegisterForSerde() {
                articuno::type_registration source_reg =
                        articuno::type_registration::create<articuno::ryml::yaml_source<>::archive_type, T>();
                articuno::type_registration sink_reg =
                        articuno::type_registration::create<articuno::ryml::yaml_sink<>::archive_type, T>();
                RegisterForSerde(source_reg, sink_reg);
            }

        protected:
            using EventSource<SkyrimPluginsLoadedEvent>::Emit;
            using EventSource<SkyrimPluginsUnloadedEvent>::Emit;
            using EventSource<SKSEInitializedEvent>::Emit;
            using EventSource<SKSEPluginsLoadedEvent>::Emit;
            using EventSource<InputLoadedEvent>::Emit;
            using EventSource<SaveGameLoadingEvent>::Emit;
            using EventSource<SaveGameLoadedEvent>::Emit;
            using EventSource<GameSavingEvent>::Emit;
            using EventSource<GameSavedEvent>::Emit;
            using EventSource<NewGameEvent>::Emit;
            using EventSource<SaveGameDeletingEvent>::Emit;
            using EventSource<SaveGameDeletedEvent>::Emit;

        private:
            System() noexcept = default;

            void RegisterForSerde(articuno::type_registration source_reg, articuno::type_registration sink_reg) const;

            mutable Util::Mutex _lock;
            mutable bool _skseInitialized{false};
            mutable bool _dataLoaded{false};
            mutable std::vector<std::function<void()>> _initCallbacks;
            mutable std::vector<std::function<void()>> _dataLoadedCallbacks;

            friend class SystemManager;
        };

#pragma warning(pop)
    }

    [[nodiscard]] __declspec(dllexport) detail::System& System() noexcept;

}
