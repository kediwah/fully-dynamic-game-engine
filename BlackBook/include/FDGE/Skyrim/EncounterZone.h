#pragma once

#include <FDGE/Util/Maps.h>
#include <gluino/extension.h>
#include <gluino/ptr.h>
#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    namespace detail {
        using CellReference = std::variant<RE::FormID, gluino::safe_ptr<RE::TESObjectCELL>>;
        using ConstCellReference = std::variant<RE::FormID, gluino::safe_ptr<const RE::TESObjectCELL>>;
        using CellSet = Util::parallel_flat_hash_set<CellReference>;
        using ConstCellSet = Util::parallel_flat_hash_set<ConstCellReference>;

        [[nodiscard]] __declspec(dllexport) const Util::parallel_flat_hash_map<RE::FormID, CellSet>& GetEncounterZoneDirectCells();

        [[nodiscard]] __declspec(dllexport) gluino::optional_ptr<RE::BGSEncounterZone> GetLocationEncounterZone(const RE::BGSLocation* location);

        struct GetEncounterZone {
            inline gluino::optional_ptr<RE::BGSEncounterZone> operator()(RE::ExtraEncounterZone* self) const noexcept {
                return self->zone;
            }

            inline gluino::optional_ptr<RE::BGSEncounterZone> operator()(RE::ExtraEncounterZone& self) const noexcept {
                return (*this)(&self);
            }

            inline gluino::optional_ptr<const RE::BGSEncounterZone> operator()(const RE::ExtraEncounterZone* self) const noexcept {
                return self->zone;
            }

            inline gluino::optional_ptr<const RE::BGSEncounterZone> operator()(const RE::ExtraEncounterZone& self) const noexcept {
                return (*this)(&self);
            }

            inline gluino::optional_ptr<RE::BGSEncounterZone> operator()(RE::BGSLocation* self) const noexcept {
                // TODO
                return {};
            }

            inline gluino::optional_ptr<RE::BGSEncounterZone> operator()(RE::BGSLocation& self) const noexcept {
                return (*this)(&self);
            }

            inline gluino::optional_ptr<const RE::BGSEncounterZone> operator()(const RE::BGSLocation* self) const noexcept {
                // TODO
                return {};
            }

            inline gluino::optional_ptr<const RE::BGSEncounterZone> operator()(const RE::BGSLocation& self) const noexcept {
                return (*this)(&self);
            }

            inline gluino::optional_ptr<RE::BGSEncounterZone> operator()(RE::TESObjectCELL* self) const noexcept {
                // TODO
                return {};
            }

            inline gluino::optional_ptr<RE::BGSEncounterZone> operator()(RE::TESObjectCELL& self) const noexcept {
                return (*this)(&self);
            }

            inline gluino::optional_ptr<const RE::BGSEncounterZone> operator()(const RE::TESObjectCELL* self) const noexcept {
                // TODO
                return {};
            }

            inline gluino::optional_ptr<const RE::BGSEncounterZone> operator()(const RE::TESObjectCELL& self) const noexcept {
                return (*this)(&self);
            }
        };

        struct SetEncounterZone {
            inline void operator()(RE::ExtraEncounterZone* self, RE::BGSEncounterZone* zone) const noexcept {
                self->zone = zone;
            }

            inline void operator()(RE::ExtraEncounterZone& self, RE::BGSEncounterZone* zone) const noexcept {
                self.zone = zone;
            }

            inline void operator()(RE::ExtraEncounterZone* self, RE::BGSEncounterZone& zone) const noexcept {
                self->zone = &zone;
            }

            inline void operator()(RE::ExtraEncounterZone& self, RE::BGSEncounterZone& zone) const noexcept {
                self.zone = &zone;
            }
        };

        struct GetLocation {
            [[nodiscard]] inline gluino::optional_ptr<RE::BGSLocation> operator()(
                    RE::BGSEncounterZone* self) const noexcept {
                return self->data.location;
            }

            [[nodiscard]] inline gluino::optional_ptr<const RE::BGSLocation> operator()(
                    const RE::BGSEncounterZone* self) const noexcept {
                return self->data.location;
            }

            [[nodiscard]] inline gluino::optional_ptr<RE::BGSLocation> operator()(
                    RE::BGSEncounterZone& self) const noexcept {
                return self.data.location;
            }

            [[nodiscard]] inline gluino::optional_ptr<const RE::BGSLocation> operator()(
                    const RE::BGSEncounterZone& self) const noexcept {
                return self.data.location;
            }
        };

        struct SetLocation {
            inline void operator()(RE::BGSEncounterZone* self, RE::BGSLocation* location) const noexcept {
                self->data.location = location;
                // TODO:
            }

            inline void operator()(RE::BGSEncounterZone* self, RE::BGSLocation& location) const noexcept {
                self->data.location = &location;
                // TODO:
            }

            inline void operator()(RE::BGSEncounterZone& self, RE::BGSLocation* location) const noexcept {
                self.data.location = location;
                // TODO:
            }

            inline void operator()(RE::BGSEncounterZone& self, RE::BGSLocation& location) const noexcept {
                self.data.location = &location;
                // TODO:
            }
        };

        struct WithDirectCells {
            inline bool operator()(RE::BGSEncounterZone* self, std::function<void(const CellSet&)>&& handler) const noexcept {
                return GetEncounterZoneDirectCells().if_contains(self->GetFormID(), [&](const auto& cells) {
                    handler(cells.second);
                });
            }

            inline bool operator()(RE::BGSEncounterZone& self, std::function<void(const CellSet&)>&& handler) const noexcept {
                return (*this)(&self, std::move(handler));
            }

            inline bool operator()(const RE::BGSEncounterZone* self, std::function<void(const ConstCellSet&)>&& handler) const noexcept {
                return GetEncounterZoneDirectCells().if_contains(self->GetFormID(), [&](const auto& cells) {
                    handler(reinterpret_cast<const ConstCellSet&>(cells.second));
                });
            }

            inline bool operator()(const RE::BGSEncounterZone& self, std::function<void(const ConstCellSet&)>&& handler) const noexcept {
                return (*this)(&self, std::move(handler));
            }
        };
    }

    constexpr gluino::extension<detail::GetEncounterZone> GetEncounterZone{};
    constexpr gluino::extension<detail::SetEncounterZone> SetEncounterZone{};

    constexpr gluino::extension<detail::GetLocation> GetLocation{};
    constexpr gluino::extension<detail::SetLocation> SetLocation{};
}
