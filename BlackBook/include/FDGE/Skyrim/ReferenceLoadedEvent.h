#pragma once

#include <gluino/ptr.h>
#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    class ReferenceLoadedEvent {
    public:
        inline explicit ReferenceLoadedEvent(gluino::safe_ptr<RE::TESObjectREFR> reference) noexcept
                : _reference(reference) {
        }

        [[nodiscard]] inline gluino::safe_ptr<RE::TESObjectREFR> GetReference() const noexcept {
            return _reference;
        }

        [[nodiscard]] inline bool IsInitializing() const noexcept {
            return _initializing;
        }

        [[nodiscard]] inline bool IsActor() const noexcept {
            return _reference->Is(RE::FormType::ActorCharacter);
        }

    private:
        gluino::safe_ptr<RE::TESObjectREFR> _reference;
        bool _initializing;
    };
}
