#pragma once

#include <FDGE/Skyrim/Concepts.h>
#include <FDGE/Util/Maps.h>
#include <gluino/enumeration.h>
#include <gluino/synchronized.h>
#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    template <class T>
    using sync_set = gluino::synchronized<phmap::flat_hash_set<T>, std::recursive_mutex>;

    class Database;

    gluino_enum(SnapshotSupport, uint8_t,
        (None, 0),
        (Partial, 1),
        (Full, 2));

    class FormData {
    public:
        inline FormData() noexcept = default;

        [[nodiscard]] inline const sync_set<RE::BGSListForm*>& GetFormLists() const noexcept {
            return _formLists;
        }

        inline SnapshotSupport SupportsSnapshot() const noexcept {
            return _supportsSnapshot;
        }

        inline bool HasSnapshot() const noexcept {
            return _hasSnapshot();
        }

        inline bool Snapshot() {
            return _snapshot();
        }

        inline bool Revert() {
            return _revert();
        }

    private:
        inline FormData(const sync_set<RE::BGSListForm*>& formLists, std::function<bool()> snapshot,
                        std::function<bool()> revert, std::function<bool()> hasSnapshot, bool fullSnapshots) noexcept
                : _formLists(formLists), _snapshot(std::move(snapshot)), _revert(std::move(revert)),
                  _hasSnapshot(std::move(hasSnapshot)),
                  _supportsSnapshot(fullSnapshots ? SnapshotSupport::Full : SnapshotSupport::Partial) {
        }

        inline explicit FormData(const sync_set<RE::BGSListForm*>& formLists) noexcept
                : _formLists(formLists) {
        }

        static inline bool ReturnFalse() {
            return false;
        }

        static inline sync_set<RE::BGSListForm*> _empty;
        const sync_set<RE::BGSListForm*>& _formLists{_empty};
        std::function<bool()> _snapshot{ReturnFalse};
        std::function<bool()> _revert{ReturnFalse};
        std::function<bool()> _hasSnapshot{ReturnFalse};
        SnapshotSupport _supportsSnapshot{SnapshotSupport::None};

        friend class Database;
    };

    class WorldSpaceData : public virtual FormData {
    public:
        [[nodiscard]] const phmap::parallel_flat_hash_set<RE::FormID>& GetCells() const noexcept;

        [[nodiscard]] const phmap::parallel_flat_hash_set<RE::TESRegion>& GetRegions() const noexcept;
    };

    class LocationData : public virtual FormData {
    public:
        [[nodiscard]] const phmap::parallel_flat_hash_set<RE::FormID>& GetChildLocations() const noexcept;

        [[nodiscard]] const phmap::parallel_flat_hash_set<std::variant<RE::FormID, RE::TESObjectCELL*>>&
        GetChildCells() const noexcept;
    };

    class EncounterZoneData : public virtual FormData {
    public:
        [[nodiscard]] const phmap::parallel_flat_hash_set<std::variant<RE::FormID, RE::TESObjectCELL*>>&
        GetDirectCells() const noexcept;
    };

    template <class T>
    class LeveledFormData : public virtual FormData {
    public:
        [[nodiscard]] const phmap::flat_hash_set<T*> GetLeveledList() const noexcept;
    };

    template <class T>
    class TemplateFormData : public virtual FormData {
    public:
        [[nodiscard]] const phmap::flat_hash_set<T*> GetDerivedForms() const noexcept;
    };

    class CellData : public virtual FormData {
    public:
    };

    class ExteriorCellData : public virtual FormData {
    public:

    };

    class __declspec(dllexport) Database {
    public:
        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<FormData> Lookup(T* form) const noexcept {
            return LookupForm(form);
        }

        template <Form T>
        requires(gluino::subtype_of<T, RE::TESActorBaseData>)
        [[nodiscard]] inline gluino::optional_ptr<
                gluino::mix<TemplateFormData<RE::TESActorBase>, LeveledFormData<RE::TESLevCharacter>>> Lookup(T* form) {
            return LookupNPC(form);
        }

        template <Form T>
        requires(gluino::subtype_of<T, RE::SpellItem>)
        [[nodiscard]] inline gluino::optional_ptr<LeveledFormData<RE::TESLevSpell>> Lookup(T* form) {

        }

        template <LeveledItemForm T>
        [[nodiscard]] inline gluino::optional_ptr<LeveledFormData<RE::TESLevItem>> Lookup(T* form) {

        }

        template <LeveledItemForm T>
        requires(gluino::subtype_of<T, RE::TESObjectWEAP>)
        [[nodiscard]] inline gluino::optional_ptr<
                gluino::mix<TemplateFormData<RE::TESObjectWEAP>, LeveledFormData<RE::TESLevItem>>> Lookup(T* form) {
            return LookupWeapon(form);
        }

        template <LeveledItemForm T>
        requires(gluino::subtype_of<T, RE::TESObjectARMO>)
        [[nodiscard]] inline gluino::optional_ptr<
                gluino::mix<TemplateFormData<RE::TESObjectARMO>, LeveledFormData<RE::TESLevItem>>> Lookup(T* form) {
            return LookupArmor(form);
        }

        [[nodiscard]] const std::vector<RE::TESForm*>& GetForms(RE::FormType formType) const noexcept;

        template <Form T>
        [[nodiscard]] inline const std::vector<RE::TESForm*>& GetForms() const noexcept {
            return reinterpret_cast<const std::vector<T*>&>(GetForms(T::FORMTYPE));
        }

        void RevertAll();

        [[nodiscard]] static Database& GetSingleton() noexcept;

    private:
        FormData LookupForm(RE::TESForm* form) const noexcept;

        void LookupNPC(RE::TESNPC* form) const noexcept;

        void LookupWeapon(RE::TESObjectWEAP* form) const noexcept;

        void LookupArmor(RE::TESObjectARMO* form) const noexcept;

        Database() noexcept;
    };
}
