#pragma once

#include <QtQuick/QQuickItem>

namespace FDGE::UI {
    class OverlayLayer : public QQuickItem {
    public:
        inline explicit OverlayLayer(QQuickItem* item, bool interactive = false, double_t z = 100.0) noexcept
            : _item(item), _mouseCaptured(interactive), _keyboardCaptured(interactive), _focused(interactive),
              _modal(interactive), _z(z) {
            item->setParentItem(this);
        }

        [[nodiscard]] inline QQuickItem* GetItem() const noexcept {
            return _item;
        }

        [[nodiscard]] inline bool IsMouseCaptured() const noexcept {
            return _mouseCaptured;
        }

        inline void SetMouseCaptured(bool mouseCaptured) {
            _mouseCaptured = mouseCaptured;
        }

        [[nodiscard]] inline bool IsKeyboardCaptured() const noexcept {
            return _keyboardCaptured;
        }

        inline void SetKeyboardCaptured(bool keyboardCaptured) {
            _keyboardCaptured = keyboardCaptured;
        }

        [[nodiscard]] inline bool IsFocused() const noexcept {
            return _focused;
        }

        inline void SetFocused(bool focused) {
            _focused = focused;
        }

        [[nodiscard]] inline bool IsModal() const noexcept {
            return _modal;
        }

        inline void SetModal(bool modal) {
            _modal = modal;
        }

    private:
        QQuickItem* _item{nullptr};
        bool _mouseCaptured{false};
        bool _keyboardCaptured{false};
        bool _focused{false};
        bool _modal{false};
        double_t _z{100.0};
    };
}
