#pragma once

#include <thread>

#include <d3d11.h>
#include <FDGE/Util/DataSize.h>
#include <FDGE/Util/EventSource.h>
#include <gluino/ptr.h>
#include <QtCore/QTimer>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQuick/QQuickRenderControl>
#include <QtQuick/QQuickRenderTarget>
#include <QtQuick/QQuickWindow>

#include "OverlayWindow.h"

namespace FDGE::UI {
    class Display {
    public:
        [[nodiscard]] inline gluino::optional_ptr<QGuiApplication> GetUIApplication() noexcept {
            return _application;
        }

        [[nodiscard]] inline gluino::optional_ptr<const QGuiApplication> GetUIApplication() const noexcept {
            return _application;
        }

        [[nodiscard]] inline gluino::optional_ptr<QQmlApplicationEngine> GetUIApplicationEngine() noexcept {
            return _engine;
        }

        [[nodiscard]] inline gluino::optional_ptr<const QQmlApplicationEngine> GetUIApplicationEngine() const noexcept {
            return _engine;
        }

        [[nodiscard]] inline gluino::optional_ptr<QQuickRenderControl> GetUIRenderControl() noexcept {
            return _renderControl;
        }

        [[nodiscard]] inline gluino::optional_ptr<const QQuickRenderControl> GetUIRenderControl() const noexcept {
            return _renderControl;
        }

        [[nodiscard]] inline gluino::optional_ptr<QQuickRenderTarget> GetRenderTarget() noexcept {
            return &_renderTarget;
        }

        [[nodiscard]] inline gluino::optional_ptr<const QQuickRenderTarget> GetRenderTarget() const noexcept {
            return &_renderTarget;
        }

        [[nodiscard]] inline gluino::optional_ptr<OverlayWindow> GetOverlayWindow() noexcept {
            return _overlayWindow;
        }

        [[nodiscard]] inline gluino::optional_ptr<const OverlayWindow> GetOverlayWindow() const noexcept {
            return _overlayWindow;
        }

        [[nodiscard]] inline ID3D11Device* GetRenderDevice() const noexcept {
            return _device;
        }

        [[nodiscard]] inline ID3D11DeviceContext* GetRenderDeviceContext() const noexcept {
            return _deviceContext;
        }

        [[nodiscard]] inline IDXGISwapChain* GetSwapChain() const noexcept {
            return _swapChain;
        }

        [[nodiscard]] inline ID3D11Texture2D* GetUITexture() const noexcept {
            return _uiTexture;
        }

        [[nodiscard]] inline uint32_t GetBufferHeight() const noexcept {
            return _bufferHeight;
        }

        [[nodiscard]] inline uint32_t GetBufferWidth() const noexcept {
            return _bufferWidth;
        }

        [[nodiscard]] inline Util::DataSize GetRenderDeviceVRAM() const noexcept {
            return _deviceVram;
        }

        [[nodiscard]] static Display& GetSingleton() noexcept;

        static inline void RunInUI(std::function<void()> callback) noexcept {
            QTimer::singleShot(0, std::move(callback));
        }

    private:
        Display() = default;

        QGuiApplication* _application{nullptr};
        QQmlApplicationEngine* _engine{nullptr};
        QQuickRenderControl* _renderControl{nullptr};
        QQuickRenderTarget _renderTarget;
        OverlayWindow* _overlayWindow{nullptr};
        ID3D11Device* _device{nullptr};
        ID3D11DeviceContext* _deviceContext{nullptr};
        IDXGISwapChain* _swapChain{nullptr};
        ID3D11Texture2D* _uiTexture{nullptr};
        uint32_t _bufferHeight{0};
        uint32_t _bufferWidth{0};
        Util::DataSize _deviceVram{0};
        std::thread _renderThread;

        friend class DisplayImpl;
    };
}
