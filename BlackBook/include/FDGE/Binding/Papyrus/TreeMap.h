#pragma once

#include "MapCollection.h"

namespace FDGE::Binding::Papyrus {
    class TreeMapIterator;

    class TreeMap : public MapCollection {
    ScriptType(TreeMap)

    public:
        inline TreeMap() noexcept = default;

        [[nodiscard]] Any* Get(Any* key) noexcept override;

        [[nodiscard]] Any* Put(Any* key, Any* value) override;

        [[nodiscard]] bool TryPut(Any* key, Any* value) override;

        [[nodiscard]] bool Contains(Any* key) noexcept override;

        [[nodiscard]] Any* Delete(Any* key) noexcept override;

        bool Clear() noexcept override;

        [[nodiscard]] int32_t GetSize() const noexcept;

        [[nodiscard]] ValueIterator* GetValueIterator() noexcept override;

        [[nodiscard]] KeyValueIterator* GetKeyValueIterator() noexcept override;

    private:

        articuno_serde(ar) {
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_version, "version");
        }

        mutable std::recursive_mutex _lock;
        phmap::btree_map<ScriptObjectHandle<Any>, ScriptObjectHandle<Any>> _value;
        uint64_t _version;

        friend class articuno::access;
        friend class TreeMapIterator;
    };

    class TreeMapIterator : public KeyValueIterator {
    ScriptType(TreeMapIterator)

    public:
        inline TreeMapIterator() noexcept = default;

        TreeMapIterator(TreeMap* parent, bool reverse = false);

        TreeMapIterator(TreeMap* parent, bool reverse, Any* key);

        [[nodiscard]] bool IsValid() noexcept override;

        [[nodiscard]] bool HasMore() noexcept override;

        bool SetValue(Any* value) noexcept override;

        bool Next() noexcept override;

        [[nodiscard]] Pair* GetEntry() noexcept override;

    protected:
        void OnGameLoadComplete() override;

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_parent, "parent");
            ar <=> articuno::kv(_key, "key");
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_version, "version");
            ar <=> articuno::kv(_reverse, "reverse");
            ar <=> articuno::kv(_beforeBegin, "beforeBegin");
        }

        ScriptObjectHandle<TreeMap> _parent;
        ScriptObjectHandle<Any> _key;
        ScriptObjectHandle<Any> _value;
        uint64_t _version{0};
        bool _beforeBegin{false};
        bool _reverse{false};
        typename decltype(TreeMap::_value)::iterator _iter;

        friend class articuno::access;
    };
}
