#pragma once

#include <articuno/articuno.h>
#include <articuno/types/auto.h>

#include "ScriptObject.h"

namespace FDGE::Binding::Papyrus {
    class Version : public ScriptObject {
        ScriptType(Version);

    public:
        inline Version() noexcept = default;

        inline explicit Version(int32_t major, int32_t minor = 0, int32_t patch = 0) noexcept
            : _major(major), _minor(minor), _patch(patch) {
        }

        [[nodiscard]] inline int32_t GetMajor() const noexcept {
            return _major;
        }

        [[nodiscard]] inline int32_t GetMinor() const noexcept {
            return _minor;
        }

        [[nodiscard]] inline int32_t GetPatch() const noexcept {
            return _patch;
        }

        [[nodiscard]] bool Equals(const ScriptObject* other) const noexcept override;

        [[nodiscard]] RE::BSFixedString ToString() const noexcept override;

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_major, "major");
            ar <=> articuno::kv(_minor, "minor");
            ar <=> articuno::kv(_patch, "patch");
        }

        int32_t _major{0};
        int32_t _minor{0};
        int32_t _patch{0};

        friend class articuno::access;
    };
}
