#pragma once

#include "Collection.h"
#include "KeyValueIterator.h"

namespace FDGE::Binding::Papyrus {
    class MapCollection : public Collection {
        ScriptType(MapCollection)

    public:
        [[nodiscard]] virtual Any* Get(Any* key) noexcept = 0;

        [[nodiscard]] virtual Any* Put(Any* key, Any* value) = 0;

        [[nodiscard]] virtual bool TryPut(Any* key, Any* value) = 0;

        [[nodiscard]] virtual bool Contains(Any* key) noexcept = 0;

        [[nodiscard]] virtual Any* Delete(Any* key) noexcept = 0;

        [[nodiscard]] virtual KeyValueIterator* GetKeyValueIterator() noexcept = 0;

    protected:
        inline MapCollection() noexcept = default;

    private:
        articuno_serde() {
        }

        friend class articuno::access;
    };
}
