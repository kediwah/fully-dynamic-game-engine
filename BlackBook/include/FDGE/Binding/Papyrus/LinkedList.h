#pragma once

#include <list>

#include "Collection.h"

namespace FDGE::Binding::Papyrus {
    class LinkedList;

    class LinkedListIterator : public ValueIterator {
    ScriptType(LinkedListIterator)

    public:
        inline LinkedListIterator() noexcept = default;

        explicit LinkedListIterator(LinkedList* parent, bool reverse = false) noexcept;

        LinkedListIterator(LinkedList* parent, bool reverse, uint64_t id) noexcept;

        [[nodiscard]] bool IsValid() noexcept override;

        [[nodiscard]] bool HasMore() noexcept override;

        [[nodiscard]] Any* GetValue() noexcept override;

        bool SetValue(Any* value) noexcept override;

        [[nodiscard]] bool Next() noexcept override;

        [[nodiscard]] Any* RemoveAndNext();

        void InsertBefore(Any* value);

        void InsertAfter(Any* value);

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_parent, "parent");
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_id, "id");
            ar <=> articuno::kv(_version, "version");
            ar <=> articuno::kv(_reverse, "reverse");
        }

        ScriptObjectHandle<LinkedList> _parent;
        ScriptObjectHandle<Any> _value;
        uint64_t _id{0};
        uint64_t _version{0};
        bool _reverse{false};

        friend class articuno::access;
        friend class List;
    };

    class LinkedList : public Collection {
        ScriptType(LinkedList)

    public:
        inline LinkedList() noexcept = default;

        [[nodiscard]] int32_t GetSize() const noexcept override;

        [[nodiscard]] bool Clear() noexcept override;

        [[nodiscard]] Any* PopFront();

        [[nodiscard]] Any* PopBack();

        void Append(Any* value);

        void Prepend(Any* value);

        [[nodiscard]] LinkedListIterator* GetValueIterator() noexcept override;

        [[nodiscard]] LinkedListIterator* GetReverseLinkedListIterator() noexcept;

        [[nodiscard]] LinkedListIterator* GetLinkedListIteratorFrom(int32_t index) noexcept;

        [[nodiscard]] LinkedListIterator* GetReverseLinkedListIteratorFrom(int32_t index) noexcept;

    private:
        struct Node {
            uint64_t Next{0};
            uint64_t Previous{0};
            ScriptObjectHandle<Any> Value;

            articuno_serde(ar) {
                ar <=> articuno::kv(Next, "next");
                ar <=> articuno::kv(Previous, "previous");
                ar <=> articuno::kv(Value, "value");
            }
        };

        articuno_serde(ar) {
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_version, "version");
            ar <=> articuno::kv(_nextID, "nextID");
            ar <=> articuno::kv(_head, "head");
            ar <=> articuno::kv(_tail, "tail");
        }

        mutable std::recursive_mutex _lock;
        phmap::flat_hash_map<uint64_t, Node> _value;
        uint64_t _version{0};
        uint64_t _nextID{1};
        uint64_t _head{0};
        uint64_t _tail{0};

        friend class articuno::access;
        friend class LinkedListIterator;
    };
}
