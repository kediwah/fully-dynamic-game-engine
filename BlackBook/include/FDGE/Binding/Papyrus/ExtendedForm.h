#pragma once

#include <FDGE/Logger.h>
#include <RE/Skyrim.h>

#undef GetObject

namespace FDGE::Binding::Papyrus {
    template <class T>
    class ExtendedForm {
       public:
        using extension_type = std::remove_cv_t<T>;

        using form_type = typename T::form_type;

        static constexpr std::string_view TypeName = extension_type::TypeName;

        inline ExtendedForm() noexcept = default;

        ExtendedForm(RE::BSTSmartPointer<RE::BSScript::Object> object) noexcept : _object(object) {
            if (object) {
                auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
                if (!vm) {
                    FDGE::Logger::Warn("Attempt to create ExtendedForm before Papyrus VM initialized.");
                    return;
                }
                _form = reinterpret_cast<form_type*>(vm->GetObjectHandlePolicy()->GetObjectForHandle(
                    form_type::FORMTYPE, object->GetHandle()));
            }
        }

        inline ExtendedForm(form_type* form) noexcept : _form(form) {
        }

        const RE::BSTSmartPointer<RE::BSScript::Object>& Bind();

        [[nodiscard]] inline const RE::BSTSmartPointer<RE::BSScript::Object>& GetObject() const noexcept {
            return _object;
        }

        [[nodiscard]] inline form_type* GetForm() const noexcept {
            return _form;
        }

        [[nodiscard]] inline operator bool() const noexcept {
            return _object.get() != nullptr;
        }

        [[nodiscard]] inline operator form_type*() noexcept {
            return _form;
        }

        [[nodiscard]] inline operator const form_type*() const noexcept {
            return _form;
        }

        [[nodiscard]] inline form_type& operator*() noexcept {
            return *_form;
        }

        [[nodiscard]] inline const form_type& operator*() const noexcept {
            return *_form;
        }

        [[nodiscard]] inline form_type* operator->() noexcept {
            return _form;
        }

        [[nodiscard]] inline const form_type* operator->() const noexcept {
            return _form;
        }

    private:
        RE::BSTSmartPointer<RE::BSScript::Object> _object;
        form_type* _form{nullptr};
    };

    template <class T>
    struct is_extended_form : public std::false_type {
    };

    template <class T>
    struct is_extended_form<ExtendedForm<T>> : public std::true_type {
    };

    template <class T>
    constexpr bool is_extended_form_v = is_extended_form<T>::value;

    template <class T>
    concept extended_form = is_extended_form_v<T>;

    void PackExtendedFormHandle(RE::BSScript::Variable* destination, const void* extendedForm,
                                std::string_view typeName, RE::VMTypeID typeID);

    template <class T>
    class AttachedObjectHandle {
    public:
        using attached_type = T;

        inline AttachedObjectHandle() noexcept = default;

        AttachedObjectHandle(T* value) {
            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            if (!vm) {
                FDGE::Logger::Warn("Attempt to create an AttachedObjectHandle before Papyrus VM initialized.");
                return;
            }

            RE::VMTypeID typeID;
            if constexpr (RE::BSScript::is_form_pointer_v<T*>) {
                typeID = value ? static_cast<RE::VMTypeID>(value->GetFormType()) : 0;
            } else if constexpr (RE::BSScript::is_active_effect_pointer_v<T*>) {
                typeID = RE::ActiveEffect::VMTYPEID;
            } else {
                typeID = value ? value->GetVMTypeID() : 0;
            }
            RE::BSTSmartPointer<RE::BSScript::ObjectTypeInfo> classPtr;
            if (!vm->GetScriptObjectType(typeID, classPtr) || !classPtr) {
                return;
            }

            auto handle = vm->GetObjectHandlePolicy()->GetHandleForObject(typeID, value);

            RE::BSTSmartPointer<RE::BSScript::Object> objectPtr;
            if (!vm->FindBoundObject(handle, classPtr->GetName(), objectPtr) || !objectPtr) {
                if (vm->CreateObject(classPtr->GetName(), objectPtr) && objectPtr) {
                    RE::BSTSmartPointer<RE::BSScript::ObjectTypeInfo> typeInfo(objectPtr->GetTypeInfo());
                    auto handlePolicy = vm->GetObjectHandlePolicy();
                    if (handlePolicy) {
                        if (handlePolicy->HandleIsType(typeID, handle) &&
                            handlePolicy->IsHandleObjectAvailable(handle)) {
                            auto bindPolicy = vm->GetObjectBindPolicy();
                            if (bindPolicy) {
                                bindPolicy->BindObject(objectPtr, handle);
                            }
                        }
                    }
                }
            }
        }

        inline AttachedObjectHandle(RE::BSTSmartPointer<RE::BSScript::Object> papyrusObject) noexcept :
            _papyrusObject(std::move(papyrusObject)) {
        }

        [[nodiscard]] inline T* GetObject() const noexcept {
            if (!_papyrusObject || !_papyrusObject->GetHandle()) {
                return nullptr;
            }
            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            if (!vm) {
                // TODO: Log
                return nullptr;
            }
            RE::VMTypeID typeID{0};
            if constexpr (RE::BSScript::is_form_pointer_v<attached_type*>) {
                typeID = static_cast<RE::VMTypeID>(attached_type::FORMTYPE);
            } else if constexpr (RE::BSScript::is_active_effect_pointer_v<attached_type*>) {
                typeID = RE::ActiveEffect::VMTYPEID;
            } else if constexpr (RE::BSScript::is_alias_pointer_v<attached_type*>) {
                typeID = attached_type::VMTYPEID;
            }
            return reinterpret_cast<T*>(vm->GetObjectHandlePolicy()->GetObjectForHandle(typeID,
                                                                                        _papyrusObject->GetHandle()));
        }

        [[nodiscard]] inline operator bool() const noexcept {
            return _papyrusObject.get() != nullptr;
        }

        [[nodiscard]] inline operator T*() noexcept {
            return GetObject();
        }

        [[nodiscard]] inline operator const T*() const noexcept {
            return GetObject();
        }

        [[nodiscard]] inline operator RE::BSTSmartPointer<RE::BSScript::Object>() const noexcept {
            return _papyrusObject;
        }

        [[nodiscard]] inline operator RE::BSTSmartPointer<RE::BSScript::Object>&() noexcept {
            return _papyrusObject;
        }

        [[nodiscard]] inline operator const RE::BSTSmartPointer<RE::BSScript::Object>&() const noexcept {
            return _papyrusObject;
        }

        [[nodiscard]] inline auto operator*() const noexcept {
            return *_papyrusObject;
        }

        [[nodiscard]] inline auto operator->() const noexcept {
            return _papyrusObject.operator->();
        }

        [[nodiscard]] bool operator==(const AttachedObjectHandle<T>& other) const noexcept {
            return _papyrusObject.get() == other._papyrusObject.get();
        }

    private:
        articuno_serialize(ar) {
            if (_papyrusObject) {
                ar <=> articuno::kv(_papyrusObject->handle, "handle");
                auto* name = _papyrusObject->GetTypeInfo()->GetName();
                ar <=> articuno::kv(name, "className");
            } else {
                RE::VMHandle zero = 0;
                ar <=> articuno::kv(zero, "handle");
            }
        }

        articuno_deserialize(ar) {
            _papyrusObject.reset();
            RE::VMHandle handle;
            ar <=> articuno::kv(handle, "handle");
            if (!handle) {
                return;
            }
            std::string className;
            ar <=> articuno::kv(className, "className");

            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            if (!vm) {
                // TODO: Log
                return;
            }
            RE::BSSpinLockGuard lock(vm->attachedScriptsLock);
            auto result = vm->attachedScripts.find(handle);
            if (result == vm->attachedScripts.end()) {
                // TODO: Log
                return;
            }
            for (auto& attachedScript : result->second) {
                if (className == attachedScript->GetTypeInfo()->GetName()) {
                    _papyrusObject.reset(attachedScript.get());
                }
            }
            // TODO: Log
        }

        RE::BSTSmartPointer<RE::BSScript::Object> _papyrusObject;

        friend class articuno::access;
    };

    template <class T>
    concept attached_object_handle = std::same_as<T, AttachedObjectHandle<typename T::attached_type>>;
};

namespace RE::BSScript {
    template <class T>
    requires (FDGE::Binding::Papyrus::extended_form<T>)
    struct is_valid_base<T> : public std::true_type {
    };

    template <class T>
    requires (FDGE::Binding::Papyrus::extended_form<T>)
    struct is_parameter_convertible<T> : public std::true_type {
    };

    template <class T>
    requires (FDGE::Binding::Papyrus::extended_form<T>)
    struct is_return_convertible<T> : public std::true_type {
    };

    template <class T>
    requires (FDGE::Binding::Papyrus::extended_form<T>)
    struct GetRawType<T> {
        [[nodiscard]] inline TypeInfo::RawType operator()() const noexcept {
            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            RE::BSTSmartPointer<RE::BSScript::ObjectTypeInfo> classPtr;
            if (!vm || !vm->GetScriptObjectType(T::TypeName, classPtr) || !classPtr) {
                return TypeInfo::RawType::kNone;
            }
            return classPtr->GetRawType();
        }
    };

    template <class T>
    requires ((is_array_v<T> || is_reference_wrapper_v<T>) &&
             FDGE::Binding::Papyrus::extended_form<typename T::value_type>)
    struct GetRawType<T> {
        [[nodiscard]] inline TypeInfo::RawType operator()() const noexcept {
            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            RE::BSTSmartPointer<RE::BSScript::ObjectTypeInfo> classPtr;
            if (!vm || !vm->GetScriptObjectType(T::value_type::TypeName, classPtr) || !classPtr) {
                return TypeInfo::RawType::kNone;
            }
            return *(stl::enumeration{ classPtr->GetRawType() } + TypeInfo::RawType::kObject);
        }
    };

    template <
        class T,
        std::enable_if_t<
            FDGE::Binding::Papyrus::is_extended_form_v<std::remove_cvref_t<T>>,
            int> = 0>
    inline void PackValue(Variable* destination, T&& source) {
        FDGE::Binding::Papyrus::PackExtendedFormHandle(destination, source.GetForm(), std::remove_cvref_t<T>::TypeName,
            static_cast<VMTypeID>(std::remove_cvref_t<T>::form_type::FORMTYPE));
    }

    template <
        class T,
        std::enable_if_t<
            FDGE::Binding::Papyrus::is_extended_form_v<std::remove_cvref_t<T>>,
            int> = 0>
    [[nodiscard]] inline T UnpackValue(const Variable* source) {
        if (!source || !source->IsObject()) {
            return {};
        }
        return {source->GetObject()};
    }

    template <class T>
    struct is_parameter_convertible<FDGE::Binding::Papyrus::AttachedObjectHandle<T>> : public std::true_type {
    };

    template <class T>
    struct is_return_convertible<FDGE::Binding::Papyrus::AttachedObjectHandle<T>> : public std::true_type {
    };

    template <class T>
    requires (std::same_as<T, FDGE::Binding::Papyrus::AttachedObjectHandle<typename T::attached_type>>)
    struct GetRawType<T> {
        [[nodiscard]] inline TypeInfo::RawType operator()() const noexcept {
            if constexpr (is_form_pointer_v<typename T::attached_type*>) {
                return GetRawTypeFromVMType(static_cast<RE::VMTypeID>(T::attached_type::FORMTYPE));
            } else if constexpr (is_active_effect_pointer_v<typename T::attached_type*>) {
                return GetRawTypeFromVMType(ActiveEffect::VMTYPEID);
            } else if constexpr (is_alias_pointer_v<typename T::attached_type*>) {
                return GetRawTypeFromVMType(T::attached_type::VMTYPEID);
            } else {
                return TypeInfo::RawType::kNone;
            }
        }
    };

    template <class T>
    requires ((is_array_v<T> || is_reference_wrapper_v<T>) &&
             FDGE::Binding::Papyrus::attached_object_handle<typename T::value_type>)
    struct GetRawType<T> {
        [[nodiscard]] inline TypeInfo::RawType operator()() const noexcept {
            VMTypeID typeID{0};
            if constexpr (is_form_pointer_v<typename T::value_type::attached_type*>) {
                typeID = static_cast<RE::VMTypeID>(T::value_type::attached_type::FORMTYPE);
            } else if constexpr (is_active_effect_pointer_v<typename T::value_type::attached_type*>) {
                typeID = ActiveEffect::VMTYPEID;
            } else if constexpr (is_alias_pointer_v<typename T::value_type::attached_type*>) {
                typeID = T::value_type::attached_type::VMTYPEID;
            }
            return *(stl::enumeration{ GetRawTypeFromVMType(typeID) } + TypeInfo::RawType::kObject);
        }
    };

    template <class T,
              std::enable_if_t<
                  FDGE::Binding::Papyrus::attached_object_handle<std::remove_cvref_t<T>>, int> = 0>
    inline void PackValue(Variable* destination, T&& source) {
        using attached_type = typename std::remove_cvref_t<T>::attached_type;
        VMTypeID typeID{0};
        if constexpr (is_form_pointer_v<attached_type*>) {
            auto* form = source.GetObject();
            if (form) {
                typeID = static_cast<RE::VMTypeID>(form->GetFormType());
            }
        } else if constexpr (is_active_effect_pointer_v<attached_type*>) {
            typeID = ActiveEffect::VMTYPEID;
        } else if constexpr (is_alias_pointer_v<attached_type*>) {
            auto* alias = source.GetObject();
            if (alias) {
                typeID = alias->GetVMTypeID();
            }
        }
        FDGE::Binding::Papyrus::PackExtendedFormHandle(destination, source.GetObject(),
                                                       source ? source->GetTypeInfo()->GetName() : "", typeID);
    }

    template <class T,
              std::enable_if_t<
                  FDGE::Binding::Papyrus::attached_object_handle<std::remove_cvref_t<T>>, int> = 0>
    [[nodiscard]] inline T UnpackValue(const Variable* source) {
        if (!source || !source->IsObject()) {
            return {};
        }
        return {source->GetObject()};
    }
}
