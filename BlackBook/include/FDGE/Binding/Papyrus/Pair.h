#pragma once

#include "Any.h"

namespace FDGE::Binding::Papyrus {
#pragma warning(push)
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)
    class __declspec(dllexport) Pair : public ScriptObject {
        ScriptType(Pair)

    public:
        inline Pair() noexcept = default;

        inline Pair(Any* first, Any* second) noexcept
            : _first(first), _second(second) {
        }

        [[nodiscard]] inline Any* GetFirst() noexcept {
            return _first ? _first.GetObject() : nullptr;
        }

        [[nodiscard]] inline const Any* GetFirst() const noexcept {
            return _first ? _first.GetObject() : nullptr;
        }

        [[nodiscard]] inline Any* GetSecond() noexcept {
            return _second ? _second.GetObject() : nullptr;
        }

        [[nodiscard]] inline const Any* GetSecond() const noexcept {
            return _second ? _second.GetObject() : nullptr;
        }

        RE::BSFixedString ToString() const noexcept override;

        std::size_t GetHashCode() const noexcept override;

    private:
        articuno_serde(ar) {
            ar <=> articuno::next(_first);
            ar <=> articuno::next(_second);
        }

        ScriptObjectHandle<Any> _first;
        ScriptObjectHandle<Any> _second;

        friend class articuno::access;
    };
#pragma warning(push)
}
