#pragma once

#include <articuno/articuno.h>
#include <articuno/types/auto.h>

#include "ScriptObject.h"

#undef GetClassName

namespace FDGE::Binding::Papyrus {
#pragma warning(push)
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)
    class __declspec(dllexport) StackFrame : public ScriptObject {
        ScriptType(StackFrame);

    public:
        inline StackFrame() noexcept = default;

        explicit StackFrame(RE::BSScript::StackFrame* frame);

        inline StackFrame(std::string& moduleName, std::string& fileName, int32_t lineNumber, int32_t memoryOffset)
            noexcept : _moduleName(std::move(moduleName)), _fileName(std::move(fileName)), _lineNumber(lineNumber),
              _memoryOffset(memoryOffset), _native(true) {
        }

        [[nodiscard]] inline std::string_view GetClassName() const noexcept {
            return _className;
        }

        [[nodiscard]] inline std::string_view GetStateName() const noexcept {
            return _stateName;
        }

        [[nodiscard]] inline std::string_view GetFunctionName() const noexcept {
            return _functionName;
        }

        [[nodiscard]] inline std::string_view GetFileName() const noexcept {
            return _fileName;
        }

        [[nodiscard]] inline std::string_view GetModuleName() const noexcept {
            return _moduleName;
        }

        [[nodiscard]] inline bool IsNative() const noexcept {
            return _native;
        }

        [[nodiscard]] inline int32_t GetLineNumber() const noexcept {
            return _lineNumber;
        }

        [[nodiscard]] inline int32_t GetMemoryOffset() const noexcept {
            return _memoryOffset;
        }

        [[nodiscard]] RE::BSFixedString ToString() const noexcept override;

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_className, "className");
            ar <=> articuno::kv(_stateName, "stateName");
            ar <=> articuno::kv(_functionName, "functionName");
            ar <=> articuno::kv(_fileName, "fileName");
            ar <=> articuno::kv(_moduleName, "moduleName");
            ar <=> articuno::kv(_native, "isNative");
            ar <=> articuno::kv(_lineNumber, "lineNumber");
            ar <=> articuno::kv(_memoryOffset, "memoryOffset");
        }

        std::string _className;
        std::string _stateName;
        std::string _functionName;
        std::string _fileName;
        std::string _moduleName;
        bool _native{false};
        int32_t _lineNumber{0};
        int32_t _memoryOffset{0};

        friend class articuno::access;
    };
#pragma warning(pop)
}
