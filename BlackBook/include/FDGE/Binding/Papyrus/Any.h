#pragma once

#include <FDGE/Skyrim/Serde.h>

#include "ExtendedForm.h"
#include "Error.h"

namespace FDGE::Binding::Papyrus {
#pragma warning(push)
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)
    class __declspec(dllexport) Any : public ScriptObject {
        ScriptType(Any);

    public:
        using value_type =
            std::variant<nullptr_t, bool, int32_t, float_t, std::string, AttachedObjectHandle<RE::TESForm>,
                         AttachedObjectHandle<RE::BGSBaseAlias>, AttachedObjectHandle<RE::ActiveEffect>,
                         ScriptObjectHandle<ScriptObject>, std::vector<bool>, std::vector<int32_t>,
                         std::vector<float_t>, std::vector<std::string>, std::vector<AttachedObjectHandle<RE::TESForm>>,
                         std::vector<AttachedObjectHandle<RE::BGSBaseAlias>>,
                         std::vector<AttachedObjectHandle<RE::ActiveEffect>>,
                         std::vector<ScriptObjectHandle<ScriptObject>>>;

        inline Any() noexcept
            : _value(nullptr) {
        }

        inline explicit Any(value_type value) noexcept
                : _value(std::move(value)) {
        }

        inline explicit Any(Error* value) noexcept
                : _value(value), _error(true) {
        }

        [[nodiscard]] inline value_type& GetValue() noexcept {
            return _value;
        }

        [[nodiscard]] inline const value_type& GetValue() const noexcept {
            return _value;
        }

        [[nodiscard]] inline bool IsError() const noexcept {
            return _error;
        }

        inline void SetError(bool error) noexcept {
            _error = error;
        }

        [[nodiscard]] RE::BSFixedString ToString() const noexcept override;

        [[nodiscard]] bool Equals(const ScriptObject* other) const noexcept override;

        [[nodiscard]] std::size_t GetHashCode() const noexcept override;

    private:
        articuno_serde(ar) {
                ar <=> articuno::kv(_value, "value");
                ar <=> articuno::kv(_error, "isError");
        }

        value_type _value;
        bool _error{false};

        friend class articuno::access;
    };
#pragma warning(pop)
}

namespace std {
    template <>
    struct hash<FDGE::Binding::Papyrus::ScriptObjectHandle<FDGE::Binding::Papyrus::Any>> {
        inline std::size_t operator()(
                const FDGE::Binding::Papyrus::ScriptObjectHandle<FDGE::Binding::Papyrus::Any>& handle) const {
            auto* obj = handle.GetObject();
            return obj ? obj->GetHashCode() : 0;
        };
    };

    template <>
    struct equal_to<FDGE::Binding::Papyrus::ScriptObjectHandle<FDGE::Binding::Papyrus::Any>> {
        inline bool operator()(const FDGE::Binding::Papyrus::ScriptObjectHandle<FDGE::Binding::Papyrus::Any>& a,
                const FDGE::Binding::Papyrus::ScriptObjectHandle<FDGE::Binding::Papyrus::Any>& b) const {
            auto* objA = a.GetObject();
            auto* objB = b.GetObject();
            if (!objA) {
                return objA == objB;
            }
            return objA->Equals(objB);
        };
    };

    template <>
    struct less<FDGE::Binding::Papyrus::ScriptObjectHandle<FDGE::Binding::Papyrus::Any>> {
        inline bool operator()(const FDGE::Binding::Papyrus::ScriptObjectHandle<FDGE::Binding::Papyrus::Any>& a,
                               const FDGE::Binding::Papyrus::ScriptObjectHandle<FDGE::Binding::Papyrus::Any>& b) const {
            auto* objA = a.GetObject();
            auto* objB = b.GetObject();
            return (objA ? objA->GetHashCode() : 0) < (objB ? objB->GetHashCode() : 0);
        }
    };
}
