#pragma once

#include "Any.h"

namespace FDGE::Binding::Papyrus {
    class Queue : public ScriptObject {
    ScriptType(Queue)

    public:
        [[nodiscard]] int32_t GetSize() const noexcept;

        void Enqueue(Any* value);

        Any* Dequeue() noexcept;

        Any* Peek() noexcept;

    private:
        articuno_serde(ar) {
            ar <=> articuno::self(_value);
        }

        mutable std::mutex _lock;
        std::queue<ScriptObjectHandle<Any>> _value;

        friend class articuno::access;
    };
}
