#pragma once

#include "ScriptObject.h"

namespace FDGE::Binding::Papyrus {
    class TestSuite : public ScriptObject {
    ScriptType(TestSuite)
    public:
        static bool RunAll(bool allAsserts = false, bool force = false, std::string_view testGroupPattern = "",
                               std::string_view suitePattern = "", std::string_view testPattern = "",
                               bool forceJUnit = false, bool terminateOnCompletion = false);

        bool Run(bool allAsserts = false, bool force = false);

    protected:
        static std::vector<RE::BSTSmartPointer<RE::BSScript::ObjectTypeInfo>> DiscoverAutoTestSuites();
    };
}
