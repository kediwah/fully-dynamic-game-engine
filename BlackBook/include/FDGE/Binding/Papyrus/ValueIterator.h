#pragma once

#include "Any.h"

namespace FDGE::Binding::Papyrus {
    class ValueIterator : public ScriptObject {
        ScriptType(ValueIterator)

    public:
        [[nodiscard]] virtual bool IsValid() noexcept = 0;

        [[nodiscard]] virtual bool HasMore() noexcept = 0;

        [[nodiscard]] virtual Any* GetValue() noexcept = 0;

        virtual bool SetValue(Any* value) noexcept = 0;

        virtual bool Next() noexcept = 0;

        [[nodiscard]] virtual Any* GetValueAndNext() noexcept;

        [[nodiscard]] virtual Any* NextAndGetValue() noexcept;

    protected:
        inline ValueIterator() noexcept = default;

    private:
        articuno_serde() {
        }

        friend class articuno::access;
    };
}
