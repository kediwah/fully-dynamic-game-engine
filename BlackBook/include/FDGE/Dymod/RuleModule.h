#pragma once

#include <memory>
#include <vector>

#include <gluino/polymorphic_any.h>

#include "Node.h"

namespace FDGE::Dymod {
    class DynamicEvent;

    class Predicate;

    class Rule;

    class RuleModule : public Node {
    public:
        virtual ~RuleModule() noexcept = default;

        [[nodiscard]] inline std::vector<std::unique_ptr<Predicate>>& GetTriggers() noexcept {
            return _triggers;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Predicate>>& GetTriggers() const noexcept {
            return _triggers;
        }

        [[nodiscard]] inline std::vector<Rule>& GetRules() noexcept {
            return _rules;
        }

        [[nodiscard]] inline const std::vector<Rule>& GetRules() const noexcept {
            return _rules;
        }

        void operator()(const DynamicEvent& event) const noexcept;

    protected:
        virtual void Apply(const DynamicEvent& event) const noexcept = 0;

        inline RuleModule() noexcept = default;

    private:
        std::vector<std::unique_ptr<Predicate>> _triggers;
        std::vector<Rule> _rules;
    };
}
