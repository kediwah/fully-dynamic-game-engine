#pragma once

#include <articuno/types/auto.h>

#include "Author.h"
#include "Dependency.h"
#include "Node.h"
#include "SchemaVersioned.h"
#include "Shard.h"

namespace FDGE::Dymod {
    class Mod : public SchemaVersioned, public Node {
    public:
        [[nodiscard]] inline const gluino::semantic_version& GetVersion() const noexcept {
            return _version;
        }

        inline void SetVersion(gluino::semantic_version version) {
            _version = version;
        }

        [[nodiscard]] inline std::vector<Author>& GetAuthors() noexcept {
            return _authors;
        }

        [[nodiscard]] inline const std::vector<Author>& GetAuthors() const noexcept {
            return _authors;
        }

        [[nodiscard]] inline std::u8string_view GetURL() const noexcept {
            return _url;
        }

        inline void SetURL(std::u8string_view url) {
            _url = url.data();
        }

        [[nodiscard]] inline std::vector<Dependency>& GetDependencies() noexcept {
            return _dependencies;
        }

        [[nodiscard]] inline const std::vector<Dependency>& GetDependencies() const noexcept {
            return _dependencies;
        }

        [[nodiscard]] inline std::vector<Shard>& GetShards() noexcept {
            return _shards;
        }

        [[nodiscard]] inline const std::vector<Shard>& GetShards() const noexcept {
            return _shards;
        }

    protected:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_version, "version");
            ar <=> articuno::kv(_authors, "authors");
            ar <=> articuno::kv(_url, "url");
            ar <=> articuno::kv(_dependencies, "dependencies");
            articuno_super(Node, ar, flags);
            articuno_super(SchemaVersioned, ar, flags);
        }

    private:
        gluino::semantic_version _version;
        std::vector<Author> _authors;
        std::u8string _url;
        std::vector<Dependency> _dependencies;
        std::vector<Shard> _shards;

        friend class articuno::access;
    };
}
