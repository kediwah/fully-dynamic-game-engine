#pragma once

#include <cstdint>

#include <gluino/enumeration.h>
#include <gluino/polymorphic_any.h>

#include "Node.h"

namespace FDGE::Dymod {
    gluino_enum(PredicateResult, uint8_t,
                (NotApplicable, 0),
                (True, 1),
                (False, 2));

    class Predicate : public Node {
    public:
        virtual ~Predicate() noexcept = default;

        [[nodiscard]] inline PredicateResult Test(const gluino::polymorphic_any& target) const noexcept {
            switch (TestImpl(target).value) {
            case PredicateResult::enum_type::True:
                return _inverted ? PredicateResult::False : PredicateResult::True;
            case PredicateResult::enum_type::False:
                return _inverted ? PredicateResult::True : PredicateResult::False;
            default:
                return PredicateResult::NotApplicable;
            }
        }

        [[nodiscard]] inline bool IsInverted() const noexcept {
            return _inverted;
        }

        inline void SetInverted(bool inverted) noexcept {
            _inverted = inverted;
        }

    protected:
        inline Predicate() noexcept = default;

        [[nodiscard]] virtual PredicateResult TestImpl(const gluino::polymorphic_any& target) const noexcept = 0;

    private:
        bool _inverted{false};
    };
}
