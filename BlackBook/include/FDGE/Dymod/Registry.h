#pragma once

#include <articuno/archives/ryml/ryml.h>
#include <articuno/type_registry.h>
#include <FDGE/Plugin.h>
#include <FDGE/Util/Maps.h>
#include <gluino/ptr.h>
#include <parallel_hashmap/phmap.h>


namespace FDGE::Dymod {
    class Action;

    class RuleModule;

    class Predicate;

    template <class T>
    concept ActionType = std::derived_from<T, Action> && std::default_initializable<T>;

    template <class T>
    concept RuleModuleType = std::derived_from<T, RuleModule> && std::default_initializable<T>;

    template <class T>
    concept PredicateType = std::derived_from<T, Predicate> && std::default_initializable<T>;

    class Registry {
    public:
        gluino::optional_ptr<Action> CreateAction(std::u8string_view type);

        gluino::optional_ptr<RuleModule> CreateModule(std::u8string_view type);

        gluino::optional_ptr<Predicate> CreatePredicate(std::u8string_view type);

        template <ActionType T>
        inline bool Register(std::u8string_view type) {
            articuno::type_registry<typename articuno::ryml::yaml_source<>::archive_type>::get()
                    .template register_type<T>();
            return _actions.try_emplace(type.data(), []() { return new T(); }).second;
        }

        template <RuleModuleType T>
        inline bool Register(std::u8string_view type) {
            articuno::type_registry<typename articuno::ryml::yaml_source<>::archive_type>::get()
                    .template register_type<T>();
            return _modules.try_emplace(type.data(), []() { return new T(); }).second;
        }

        template <PredicateType T>
        inline bool Register(std::u8string_view type) {
            articuno::type_registry<typename articuno::ryml::yaml_source<>::archive_type>::get()
                    .template register_type<T>();
            return _predicates.try_emplace(type.data(), []() { return new T(); }).second;
        }

        [[nodiscard]] static Registry& GetSingleton() noexcept;

    private:
        inline Registry() noexcept = default;

        Util::istr_flat_hash_map<std::u8string, std::function<Action*()>> _actions;
        Util::istr_flat_hash_map<std::u8string, std::function<RuleModule*()>> _modules;
        Util::istr_flat_hash_map<std::u8string, std::function<Predicate*()>> _predicates;
    };
}

namespace articuno::serde {
    template <::articuno::deserializing_archive Archive, class T>
    requires(std::same_as<T, FDGE::Dymod::Action>)
    T* instantiate(Archive & ar, ::articuno::value_flags, ::std::size_t count = 1) {
        if (count > 1) {
            throw std::invalid_argument("Instantiating action arrays is not supported.");
        }
        std::u8string type;
        ar <=> ::articuno::kv(type, "type");
        decltype(auto) typeName = gluino::string_cast<char8_t>(type);
        return FDGE::Dymod::Registry::GetSingleton().CreateAction(typeName).get_raw();
    }

    template <::articuno::deserializing_archive Archive, class T>
    requires(std::same_as<T, FDGE::Dymod::RuleModule>)
    T* instantiate(Archive & ar, ::articuno::value_flags, ::std::size_t count = 1) {
        if (count > 1) {
            throw std::invalid_argument("Instantiating module arrays is not supported.");
        }
        std::u8string type;
        ar <=> ::articuno::kv(type, "type");
        decltype(auto) typeName = gluino::string_cast<char8_t>(type);
        return FDGE::Dymod::Registry::GetSingleton().CreateModule(typeName).get_raw();
    }

    template <::articuno::deserializing_archive Archive, class T>
    requires(std::same_as<T, FDGE::Dymod::Predicate>)
    T* instantiate(Archive & ar, ::articuno::value_flags, ::std::size_t count = 1) {
        if (count > 1) {
            throw std::invalid_argument("Instantiating predicate arrays is not supported.");
        }
        std::u8string type;
        ar <=> ::articuno::kv(type, "type");
        decltype(auto) typeName = gluino::string_cast<char8_t>(type);
        return FDGE::Dymod::Registry::GetSingleton().CreatePredicate(typeName).get_raw();
    }
}

#define DymodRegister(clazz, type) OnSKSELoad(10000) { ::FDGE::Dymod::Registry::GetSingleton().Register<clazz>(type); }
