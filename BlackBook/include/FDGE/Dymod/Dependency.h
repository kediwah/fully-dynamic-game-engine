#pragma once

#include <articuno/types/auto.h>

#include "Node.h"

namespace FDGE::Dymod {
    class Dependency : public Node {
    public:

    private:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_dependents, "dependents", flags);
            ar <=> articuno::kv(_dymod, "dymod", flags);
            ar <=> articuno::kv(_minimumVersion, "minimumVersion", flags);
            ar <=> articuno::kv(_maximumVersion, "maximumVersion", flags);
            articuno_super(Node, ar, flags);
        }

        std::vector<std::u8string> _dependents;
        bool _dymod{false};
        gluino::semantic_version _minimumVersion;
        gluino::semantic_version _maximumVersion;

        friend class articuno::access;
    };
}
