#pragma once

#include <optional>
#include <variant>

namespace FDGE::Dymod {
    class Scriptable {
    public:
        using script_type = std::variant<>;

        [[nodiscard]] inline const std::optional<script_type>& GetScript() const noexcept {
            return _script;
        }

    protected:
        inline Scriptable() noexcept = default;

    private:
        std::optional<script_type> _script;
    };
}
