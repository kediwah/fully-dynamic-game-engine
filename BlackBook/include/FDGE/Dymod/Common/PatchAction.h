#pragma once

#include "SnapshotStrategy.h"
#include "../Action.h"
#include "../Scriptable.h"

namespace FDGE::Dymod::Common {
    class PatchAction : public Action, public Scriptable {
    public:
        [[nodiscard]] inline SnapshotStrategy GetSnapshotStrategy() const noexcept {
            return _snapshotStrategy;
        }

        inline void SetSnapshotStrategy(SnapshotStrategy snapshotStrategy) noexcept {
            _snapshotStrategy = snapshotStrategy;
        }

    private:
        SnapshotStrategy _snapshotStrategy;
    };
}
