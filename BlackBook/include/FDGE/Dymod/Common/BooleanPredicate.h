#pragma once

#include <parallel_hashmap/phmap.h>

#include "../Predicate.h"

namespace FDGE::Dymod::Common {
    class BooleanPredicate : public Predicate {
    public:
        [[nodiscard]] inline bool IsNegated() const noexcept {
            return _negated;
        }

        inline void SetNegated(bool negated) noexcept {
            _negated = negated;
        }

    protected:
        virtual std::optional<int32_t> GetValue(const gluino::polymorphic_any& target) const noexcept = 0;

        [[nodiscard]] PredicateResult TestImpl(const gluino::polymorphic_any& target) const noexcept;

    private:
        bool _negated{false};
    };
}
