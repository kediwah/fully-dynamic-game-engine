#pragma once

#include <string>

#include <articuno/articuno.h>
#include <articuno/types/auto.h>

namespace FDGE::Dymod {
    class Node {
    public:
        [[nodiscard]] inline std::u8string_view GetID() const noexcept {
            return _id;
        }

        inline void SetID(std::u8string_view id) {
            _id = id.data();
        }

        [[nodiscard]] inline std::u8string_view GetName() const noexcept {
            return _name;
        }

        inline void SetName(std::u8string_view name) {
            _name = name.data();
        }

        [[nodiscard]] inline std::u8string_view GetDescription() const noexcept {
            return _description;
        }

        inline void SetDescription(std::u8string_view description) {
            _description = description.data();
        }

        [[nodiscard]] inline bool IsDisabled() const noexcept {
            return _disabled;
        }

        inline void SetDisabled(bool disabled) noexcept {
            _disabled = disabled;
        }

    protected:
        inline Node() noexcept = default;

        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_id, "id", flags);
            ar <=> articuno::kv(_name, "name", flags);
            ar <=> articuno::kv(_description, "description", flags);
            ar <=> articuno::kv(_disabled, "disabled", flags);
        }

    private:
        std::u8string _id;
        std::u8string _name;
        std::u8string _description;
        bool _disabled{false};

        friend class articuno::access;
    };
}