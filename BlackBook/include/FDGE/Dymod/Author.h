#pragma once

#include <articuno/articuno.h>

namespace FDGE::Dymod {
    class Author {
    public:
        [[nodiscard]] inline std::u8string_view GetName() const noexcept {
            return _name;
        }

        inline void SetName(std::u8string_view name) {
            _name = name.data();
        }

        [[nodiscard]] inline std::u8string_view GetEmail() const noexcept {
            return _email;
        }

        inline void SetEmail(std::u8string_view email) {
            _email = email.data();
        }

    private:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_name, "name", flags);
            ar <=> articuno::kv(_email, "email", flags);
        }

        std::u8string _name;
        std::u8string _email;

        friend class articuno::access;
    };
}
