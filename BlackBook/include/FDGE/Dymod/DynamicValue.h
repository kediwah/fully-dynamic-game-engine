#pragma once

#include "Scriptable.h"

namespace FDGE::Dymod {
    template <class T>
    class DynamicValue : public Scriptable {
    public:
        inline DynamicValue() noexcept = default;

        explicit inline DynamicValue(T value) noexcept
                : _value(value) {
        }

        T Inherit(const T& other) {
            if (GetScript().has_value()) {
                // TODO: Apply script to other and return.
            }
            return other;
        }

        T Inherit(const DynamicValue<T>& other) {
            return Inherit(other._value);
        }

        [[nodiscard]] inline T GetValue() const noexcept {
            if (GetScript().has_value()) {
                // Apply script to value and return.
            }
            return _value;
        }

    private:
        T _value{};
    };
}
