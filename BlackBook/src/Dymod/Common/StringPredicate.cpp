#include <FDGE/Dymod/Common/StringPredicate.h>

using namespace FDGE::Dymod;
using namespace FDGE::Dymod::Common;
using namespace srell;

bool StringPredicate::AddPrefix(std::string_view prefix) noexcept {
    std::string lower = prefix.data();
    std::transform(lower.begin(), lower.end(), lower.begin(),
                   [](char in) { return static_cast<char>(std::tolower(in)); });
    return _prefixes.emplace(std::move(lower)).second;
}

bool StringPredicate::RemovePrefix(std::string_view prefix) noexcept {
    std::string lower = prefix.data();
    std::transform(lower.begin(), lower.end(), lower.begin(),
                   [](char in) { return static_cast<char>(std::tolower(in)); });
    return _prefixes.erase(lower);
}

bool StringPredicate::AddSuffix(std::string_view suffix) noexcept {
    std::string lowerReversed = suffix.data();
    std::transform(lowerReversed.begin(), lowerReversed.end(), lowerReversed.begin(),
                   [](char in) { return static_cast<char>(std::tolower(in)); });
    std::reverse(lowerReversed.begin(), lowerReversed.end());
    return _suffixes.emplace(std::move(lowerReversed)).second;
}

bool StringPredicate::RemoveSuffix(std::string_view suffix) noexcept {
    std::string lowerReversed = suffix.data();
    std::transform(lowerReversed.begin(), lowerReversed.end(), lowerReversed.begin(),
                   [](char in) { return static_cast<char>(std::tolower(in)); });
    std::reverse(lowerReversed.begin(), lowerReversed.end());
    return _suffixes.erase(lowerReversed);
}

PredicateResult StringPredicate::TestImpl(const gluino::polymorphic_any& target) const noexcept {
    auto maybe = GetValue(target);
    if (!maybe.has_value()) {
        return PredicateResult::NotApplicable;
    }
    auto value = maybe.value();
    std::string lower;
    if (_values.contains(value)) {
        return PredicateResult::True;
    }
    if (!_prefixes.empty() || !_suffixes.empty()) {
        lower.reserve(value.size());
        std::transform(value.begin(), value.end(), lower.begin(),
                       [](char in) { return static_cast<char>(std::tolower(in)); });
        if (!_prefixes.empty() && _prefixes.longest_prefix(lower) != _prefixes.end()) {
            return PredicateResult::True;
        }
    }
    if (!_suffixes.empty()) {
        std::reverse(lower.begin(), lower.end());
        if (_suffixes.longest_prefix(lower) != _suffixes.end()) {
            return PredicateResult::True;
        }
    }
    for (auto& pattern : _patterns) {
        if (regex_match(value, pattern)) {
            return PredicateResult::True;
        }
    }
    return PredicateResult::False;
}
