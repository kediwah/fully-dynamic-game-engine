#include <FDGE/Dymod/Common/EditorIDPredicate.h>

#include <FDGE/Dymod/Registry.h>

using namespace FDGE::Dymod;
using namespace FDGE::Dymod::Common;

std::optional<std::string> EditorIDPredicate::GetValue(const gluino::polymorphic_any& target) const noexcept {
    auto form = std::any_cast<RE::TESForm*>(target);
    if (form.is_null()) {
        return {};
    }
    return form->GetFormEditorID();
}

namespace {
    DymodRegister(EditorIDPredicate, u8"edid");
}
