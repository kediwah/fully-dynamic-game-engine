#include <FDGE/Dymod/DymodLoader.h>

#include <FDGE/Dymod/Action.h>
#include <FDGE/Dymod/Predicate.h>
#include <FDGE/Dymod/Registry.h>
#include <FDGE/Dymod/Rule.h>
#include <FDGE/Dymod/RuleModule.h>
#include <FDGE/Logger.h>
#include <FDGE/Skyrim/Database.h>

#include "../FDGEConfig.h"

using namespace articuno;
using namespace articuno::ryml;
using namespace FDGE;
using namespace FDGE::Dymod;
using namespace FDGE::Skyrim;
using namespace FDGE::Util;
using namespace gluino;

namespace {
    OnSKSELoaded {
        if (!FDGEConfig::GetProxy()->GetDebug().GetModules().IsDymodsEnabled()) {
            Logger::Warn("Dymods module has been disabled, no dymods will be loaded.");
            return;
        }

        std::filesystem::create_directories("Data\\Dymods");
        DymodLoader::GetSingleton().Refresh();
    }
}

DymodLoader& DymodLoader::GetSingleton() noexcept {
    static DymodLoader instance;
    return instance;
}

void DymodLoader::Emit(const DynamicEvent& event) {
    std::shared_lock lock(_lock);
    for (auto& mod : _loadOrder) {

    }
}

void DymodLoader::Refresh() {
    std::unique_lock lock(_lock);
    _mods.clear();
    _loadOrder.clear();
    Database::GetSingleton().RevertAll();
    if (!std::filesystem::exists("Data\\Dymods")) {
        Logger::Debug("No Dymods directory found, skipping dymod loading.");
        return;
    }
    std::filesystem::directory_iterator iter("Data\\Dymods");
    for (auto& node : iter) {
        if (!node.is_directory()) {
            continue;
        }
        auto metaPath = node.path() / "dymod.yaml";
        if (!std::filesystem::exists(metaPath)) {
            Logger::Debug("Directory '{}' found without a dymod.yaml file.", node.path().string());
            continue;
        }
        std::ifstream dymodInput(metaPath);
        std::shared_ptr<Mod> dymod = std::make_unique<Mod>();
        try {
            yaml_source ar(dymodInput);
            ar >> dymod;
            dymodInput.close();
        } catch (const std::exception& e) {
            Logger::Error("Error loading dymod at '{}', unable to parse dymod.yaml: {}", node.path().string(),
                          e.what());
            continue;
        }
        std::filesystem::directory_iterator dymodIter(node.path());
        bool failed = false;
        for (auto& file : dymodIter) {
            if (file.path().extension() != ".yaml" || !file.path().stem().string().ends_with(".shard")) {
                continue;
            }
            std::ifstream shardInput(file.path());
            auto& shard = dymod->GetShards().emplace_back();
            try {
                yaml_source ar(shardInput);
                ar >> shard;
            } catch (const std::exception& e) {
                Logger::Error("Error loading shard at '{}': {}", file.path().string(), e.what());
                failed = true;
                break;
            }
        }
        if (failed) {
            continue;
        }
        if (dymod->GetShards().empty()) {
            auto id = dymod->GetID();
            Logger::Warn("Dymod '{}' has no shards.", string_cast<char>(id));
        }
        auto result = _mods.try_emplace(dymod->GetID().data(), std::move(dymod));
        if (!result.second) {
            auto id = dymod->GetID();
            Logger::Error("Conflicting dymod ID '{}'.", string_cast<char>(id));
        }
    }
    Logger::Debug("Successfully loaded {} dymods.", _mods.size());
    ValidateDependencies();
    SortLoadOrder();
}

void DymodLoader::ValidateDependencies() {
    for (auto& entry : _mods) {
        for (auto& dependency : entry.second->GetDependencies()) {

        }
    }
}

void DymodLoader::SortLoadOrder() {
    phmap::btree_multimap<uint64_t, std::shared_ptr<Mod>> sorted;
    for (auto& entry : _mods) {

    }
}

void DymodLoader::HandleFilesystemEvent(const std::basic_string<wchar_t>& path,
                                        FilesystemEvent eventType) {
    switch (eventType) {
        case FilesystemEvent::Modified:
        case FilesystemEvent::Added:
        case FilesystemEvent::Removed:
            Refresh();
            break;
        default:
            break;
    }
}
