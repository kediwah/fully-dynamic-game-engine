#include <FDGE/Dymod/Registry.h>

using namespace FDGE;
using namespace FDGE::Dymod;
using namespace gluino;

optional_ptr<Action> Registry::CreateAction(std::u8string_view type) {
    auto result = _actions.find(type);
    if (result == _actions.end()) {
        return {};
    }
    return result->second();
}

optional_ptr<RuleModule> Registry::CreateModule(std::u8string_view type) {
    auto result = _modules.find(type);
    if (result == _modules.end()) {
        return {};
    }
    return result->second();
}

optional_ptr<Predicate> Registry::CreatePredicate(std::u8string_view type) {
    auto result = _predicates.find(type);
    if (result == _predicates.end()) {
        return {};
    }
    return result->second();
}

Registry& Registry::GetSingleton() noexcept {
    static Registry instance;
    return instance;
}
