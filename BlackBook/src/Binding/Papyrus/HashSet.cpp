#include <FDGE/Binding/Papyrus/HashSet.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

Any* HashSet::GetValue(Any* value) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.find(value);
    if (result == _value.end()) {
        return nullptr;
    }
    return const_cast<Any*>(result->GetObject());
}

Any* HashSet::PutValue(Any* value) noexcept {
    std::unique_lock lock(_lock);
    ++_version;
    auto result = _value.emplace(value);
    if (result.second) {
        return nullptr;
    }
    auto* replaced = const_cast<Any*>(result.first->GetObject());
    const_cast<ScriptObjectHandle<Any>&>(*result.first) = value;
    return replaced;
}

bool HashSet::TryPutValue(Any* value) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.emplace(value);
    if (result.second) {
        ++_version;
    }
    return result.second;
}

bool HashSet::Contains(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    return _value.find(value) != _value.end();
}

Any* HashSet::Delete(Any* key) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.find(key);
    if (result == _value.end()) {
        return nullptr;
    }
    ++_version;
    auto* deleted = const_cast<Any*>(result->GetObject());
    _value.erase(result);
    return deleted;
}

bool HashSet::Clear() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return false;
    }
    ++_version;
    _value.clear();
    return true;
}

void HashSet::Reserve(int32_t capacity) {
    std::unique_lock lock(_lock);
    ++_version;
    _value.rehash(capacity);
}

int32_t HashSet::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

ValueIterator* HashSet::GetValueIterator() noexcept {
    return new HashSetIterator(this, false);
}

ValueIterator* HashSet::GetReverseValueIterator() noexcept {
    return new HashSetIterator(this, true);
}

ValueIterator* HashSet::GetValueIteratorFrom(Any* value) noexcept {
    return new HashSetIterator(this, false, value);
}

ValueIterator* HashSet::GetReverseValueIteratorFrom(Any* value) noexcept {
    return new HashSetIterator(this, true, value);
}

HashSetIterator::HashSetIterator(HashSet* parent, bool reverse) noexcept
    : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    if (parent->GetSize()) {
        _iter = reverse ? parent->_value.end() - 1 : parent->_value.begin();
        if (IsValid()) {
            _value = *_iter;
        }
    } else {
        _iter = parent->_value.end();
        if (_reverse) {
            _beforeBegin = true;
        }
    }
}

HashSetIterator::HashSetIterator(HashSet* parent, bool reverse, Any* key) noexcept
    : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    _iter = parent->_value.find(key);
    if (IsValid()) {
        _value = *_iter;
    } else if (reverse) {
        _beforeBegin = true;
    }
}

void HashSetIterator::OnGameLoadComplete() {
    auto* parent = _parent.GetObject();
    auto* value = _value.GetObject();
    if (!parent || !value) {
        return;
    }
    std::unique_lock lock(parent->_lock);
    _iter = parent->_value.find(value);
}

bool HashSetIterator::IsValid() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    return !_beforeBegin && _version == parent->_version && _iter >= parent->_value.begin() &&
           _iter < parent->_value.end();
}

bool HashSetIterator::HasMore() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    return !_beforeBegin && _iter != parent->_value.end();
}

Any* HashSetIterator::GetValue() noexcept {
    return _value.GetObject();
}

bool HashSetIterator::SetValue(Any* value) noexcept {
    HashSet* parent = _parent;
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (!IsValid()) {
        return false;
    }
    parent->_value.erase(_iter);
    auto result = parent->TryPutValue(value);
    _version = parent->_version;
    return result;
}

bool HashSetIterator::Next() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (parent->_version != _version) {
        return false;
    }
    if (_reverse) {
        if (_iter == parent->_value.begin()) {
            _beforeBegin = true;
            _value = {};
            return false;
        }
        --_iter;
    } else if (_iter != parent->_value.end()) {
        ++_iter;
    }
    if (!IsValid()) {
        _value = {};
        return false;
    }
    _value = *_iter;
    return true;
}

namespace {
    RegisterScriptType(HashSet)
    RegisterScriptType(HashSetIterator)

    PapyrusClass(HashSet) {
        PapyrusStaticFunction(Create, int32_t initialCapacity) -> HashSet* {
            if (initialCapacity < 0) {
                return nullptr;
            }
            auto* map = new HashSet();
            map->Reserve(initialCapacity);
            return map;
        };

        PapyrusFunction(Reserve, HashSet* self, int32_t capacity) {
            if (self && capacity > 0) {
                self->Reserve(capacity);
            }
        };

        PapyrusFunction(GetReverseValueIterator, HashSet* self) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseValueIterator();
        };

        PapyrusFunction(GetReverseValueIteratorFrom, HashSet* self, Any* value) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseValueIteratorFrom(value);
        };
    }
}
