#include <FDGE/Binding/Papyrus/RandomAccessCollection.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

namespace {
    RegisterScriptType(RandomAccessCollection);

    PapyrusClass(RandomAccessCollection) {
        PapyrusFunction(Get, RandomAccessCollection* self, int32_t index) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->GetValue(index);
        };

        PapyrusFunction(Set, RandomAccessCollection* self, int32_t index, Any* value) {
            if (!self) {
                return false;
            }
            return self->SetValue(index, value);
        };

        PapyrusFunction(Erase, RandomAccessCollection* self, int32_t index) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Erase(index);
        };

        PapyrusFunction(ReverseIndexOf, RandomAccessCollection* self, Any* value) {
            if (!self) {
                return -1;
            }
            return self->ReverseIndexOf(value);
        };

        PapyrusFunction(GetReverseValueIterator, RandomAccessCollection* self) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseValueIterator();
        };

        PapyrusFunction(GetValueIteratorFrom, RandomAccessCollection* self, int32_t index) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetValueIteratorFrom(index);
        };

        PapyrusFunction(GetReverseValueIteratorFrom, RandomAccessCollection* self, int32_t index) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseValueIteratorFrom(index);
        };
    };
}
