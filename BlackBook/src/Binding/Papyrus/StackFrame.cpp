#include <FDGE/Binding/Papyrus/StackFrame.h>

#include <boost/stacktrace.hpp>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace RE;

StackFrame::StackFrame(RE::BSScript::StackFrame* frame) {
    auto owner = frame->self.GetObject();
    _className = frame->owningObjectType->GetName();
    _fileName = frame->owningFunction->GetSourceFilename();
    _functionName = frame->owningFunction->GetName();
    // Owner not present on static call.
    if (owner) {
        _stateName = frame->self.GetObject()->currentState;
    }
    if (!frame->owningFunction->GetIsNative()) {
        auto* fn = skyrim_cast<RE::BSScript::Internal::ScriptFunction*>(frame->owningFunction.get());
        if (fn) {
            fn->TranslateIPToLineNumber(frame->instructionPointer, reinterpret_cast<uint32_t&>(_lineNumber));
        }
    }
}

BSFixedString StackFrame::ToString() const noexcept {
    std::string result;
    if (_native) {
        result = fmt::format("{}+{:X}", _moduleName, _memoryOffset);
    } else {
        result += _className;
        result += "::";
        if (!_stateName.empty()) {
            result += _stateName;
            result += "::";
        }
        result += _functionName;
    }
    if (!_fileName.empty()) {
        result += fmt::format(" ({}:{})", _fileName, _lineNumber);
    }
    return result.c_str();
}

namespace {
    RegisterScriptType(StackFrame);

    PapyrusClass(StackFrame) {
        PapyrusFunction(__GetClassName, StackFrame* self) {
            if (!self) {
                return ""sv;
            }
            return self->GetClassName();
        };

        PapyrusFunction(__GetStateName, StackFrame* self) {
            if (!self) {
                return ""sv;
            }
            return self->GetStateName();
        };

        PapyrusFunction(__GetFunctionName, StackFrame* self) {
            if (!self) {
                return ""sv;
            }
            return self->GetFunctionName();
        };

        PapyrusFunction(__GetFileName, StackFrame* self) {
            if (!self) {
                return ""sv;
            }
            return self->GetFileName();
        };

        PapyrusFunction(__GetModuleName, StackFrame* self) {
            if (!self) {
                return ""sv;
            }
            return self->GetModuleName();
        };

        PapyrusFunction(__IsNative, StackFrame* self) {
            if (!self) {
                return false;
            }
            return self->IsNative();
        };

        PapyrusFunction(__GetLineNumber, StackFrame* self) {
            if (!self) {
                return 0;
            }
            return self->GetLineNumber();
        };

        PapyrusFunction(__GetMemoryOffset, StackFrame* self) {
            if (!self) {
                return 0;
            }
            return self->GetMemoryOffset();
        };
    };
}
