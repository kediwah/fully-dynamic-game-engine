#include <FDGE/Binding/Papyrus/Proxy.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE::BSScript::Internal;

bool Proxy::RegisterForProxyUpdating(RE::VMHandle handle) {
    std::unique_lock lock(_lock);
    return _updatingHandles.emplace(handle).second;
}

bool Proxy::UnregisterForProxyUpdating(RE::VMHandle handle) {
    std::unique_lock lock(_lock);
    return _updatingHandles.erase(handle);
}

bool Proxy::RegisterForProxyUpdated(RE::VMHandle handle) {
    std::unique_lock lock(_lock);
    return _updatedHandles.emplace(handle).second;
}

bool Proxy::UnregisterForProxyUpdated(RE::VMHandle handle) {
    std::unique_lock lock(_lock);
    return _updatedHandles.erase(handle);
}

void Proxy::OnUpdating() {
    auto* vm = VirtualMachine::GetSingleton();
    if (!vm) {
        return;
    }
    std::unique_lock lock(_lock);
    for (auto handle : _updatingHandles) {
        // TODO: Investigate the function arguments being empty vs null.
        vm->SendEvent(handle, "OnProxyUpdating", nullptr);
    }
}

void Proxy::OnUpdated() {
    auto* vm = VirtualMachine::GetSingleton();
    if (!vm) {
        return;
    }
    std::unique_lock lock(_lock);
    for (auto handle : _updatingHandles) {
        // TODO: Investigate the function arguments being empty vs null.
        vm->SendEvent(handle, "OnProxyUpdated", nullptr);
    }
}

namespace {
    RegisterScriptType(Proxy)

    PapyrusClass(Proxy) {
        PapyrusFunction(Get, Proxy* self) -> MapCollection* {
            if (!self) {
                return nullptr;
            }
            return self->Get();
        };

        PapyrusFunction(RegisterForProxyUpdating, Proxy* self) {
            if (!self) {
                return false;
            }
            return self->RegisterForProxyUpdating(CurrentCallStack->top->previousFrame->self.GetObject()->GetHandle());
        };

        PapyrusFunction(UnregisterForProxyUpdating, Proxy* self) {
            if (!self) {
                return false;
            }
            return self->UnregisterForProxyUpdating(
                    CurrentCallStack->top->previousFrame->self.GetObject()->GetHandle());
        };

        PapyrusFunction(RegisterForProxyUpdated, Proxy* self) {
            if (!self) {
                return false;
            }
            return self->RegisterForProxyUpdated(CurrentCallStack->top->previousFrame->self.GetObject()->GetHandle());
        };

        PapyrusFunction(UnregisterForProxyUpdated, Proxy* self) {
            if (!self) {
                return false;
            }
            return self->UnregisterForProxyUpdated(CurrentCallStack->top->previousFrame->self.GetObject()->GetHandle());
        };
    }
}
