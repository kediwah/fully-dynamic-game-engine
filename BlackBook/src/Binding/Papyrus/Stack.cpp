#include <FDGE/Binding/Papyrus/Stack.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

int32_t Stack::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

void Stack::Push(Any* value) {
    std::unique_lock lock(_lock);
    _value.emplace(value);
}

Any* Stack::Pop() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return nullptr;
    }
    auto* result = _value.top().GetObject();
    _value.pop();
    return result;
}

Any* Stack::Peek() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return nullptr;
    }
    return _value.top().GetObject();
}

namespace {
    RegisterScriptType(Stack)

    PapyrusClass(Stack) {
        PapyrusStaticFunction(Create) {
            return new Stack();
        };

        PapyrusFunction(__GetSize, Stack* self) {
            return self ? self->GetSize() : 0;
        };

        PapyrusFunction(Push, Stack* self, Any* value) {
            if (self) {
                self->Push(value);
            }
        };

        PapyrusFunction(Pop, Stack* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Pop();
        };

        PapyrusFunction(Peek, Stack* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Peek();
        };
    }
}
