#include <FDGE/Binding/Papyrus/Any.h>
#include <FDGE/Binding/Papyrus/Error.h>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Binding/Papyrus/ScriptObject.h>
#include <FDGE/Hook/CallHook.h>
#include <FDGE/Hook/FunctionHook.h>
#include <FDGE/Logger.h>
#include <FDGE/Util/SafeAlloc.h>

#include <new>

#include "../../FDGEConfig.h"

using namespace articuno;
using namespace articuno::ryml;
using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Hook;
using namespace FDGE::Util;
using namespace gluino;
using namespace RE;
using namespace RE::BSScript;
using namespace RE::BSScript::Internal;
using namespace RE::BSScript::NF_util;
using namespace RE::SkyrimScript;
using namespace REL;

namespace {
    constexpr uint32_t StartingTypeID = 161;
    uint32_t NextTypeID{StartingTypeID};
    std::mutex TypesLock;
    phmap::flat_hash_map<std::string, std::function<ScriptObject*()>> TypeConstructors;
    phmap::flat_hash_set<std::string> TypeNames;
    std::vector<std::string> Types;
}  // namespace

gluino::optional_ptr<ScriptObject> ScriptObjectStore::FindObject(uint64_t id) const noexcept {
    std::unique_lock lock(_lock);
    auto result = _idsToObjects.find(id);
    if (result == _idsToObjects.end()) {
        return {};
    }
    return result->second;
}

uint64_t ScriptObjectStore::GetID(ScriptObject* object) const noexcept {
    std::unique_lock lock(_lock);
    auto result = _objectsToIDs.find(object);
    if (result == _objectsToIDs.end()) {
        return 0;
    }
    return result->second;
}

VMHandle ScriptObjectStore::Add(ScriptObject* object) {
    std::unique_lock lock(_lock);
    auto result = _objectsToIDs.try_emplace(object, 0);
    if (result.second) {
        VMHandle handle = 0x0004000000000001;
        uint64_t nextID;
        do {
            // Maximum hadles we can have, while staying compatible with existing handles and the save game format, is
            // 2^49. We lose 3 bits for the flags for special types in the high word, 1 for a flag in the second-highest
            // word (indicating alias), and 8 bits in the lower dword because only the raw form ID is kept in save
            // files. The top two bits of the remaining form ID are also used to indicate the source of the form, which
            // here are set to 0 to indicate sourced from a file, and the least significant bit is always 1 to ensure it
            // never appears the form is unattached, which could cause users of Resaver to spuriously clean the entry.
            if (_nextID == 0x2000000000000) {
                _nextID = 1;
            }
            nextID = _nextID++;

            handle |= (nextID << 1) & 0x3FFFFF;
            nextID = (nextID & 0xFFFFFFFFFF000000) << 11;
            handle |= nextID & 0x0000FFFE00000000;
            handle |= (nextID << 3) & 0xFFFC000000000000;
        } while (!_idsToObjects.try_emplace(handle, object).second);
        result.first->second = handle;

        ScriptObjectCreatedEvent event(object, handle);
        Emit(event);
    }
    return result.first->second;
}

bool ScriptObjectStore::Remove(uint64_t id) noexcept {
    std::unique_lock lock(_lock);
    auto result = _idsToObjects.find(id);
    if (result == _idsToObjects.end()) {
        return false;
    }

    auto* object = result->second;

    DestroyingScriptObjectEvent destroying(object, id);
    Emit(destroying);

    _objectsToIDs.erase(object);
    delete object;
    _idsToObjects.erase(result);

    ScriptObjectDestroyedEvent destroyed(id);
    Emit(destroyed);

    Logger::Trace("Garbage collected script object with handle {:X}.", id);
    return true;
}

ScriptObjectStore& ScriptObjectStore::GetSingleton() noexcept {
    static ScriptObjectStore singleton;
    return singleton;
}

void ScriptObjectStore::OnNewGame() {
    _nextID = 1;
    _objectsToIDs.clear();
    _idsToObjects.clear();
}

void ScriptObjectStore::OnGameSaved(std::ostream& out) {
    yaml_sink ar(out);
    ar << *this;
}

void ScriptObjectStore::OnGameLoaded(std::istream& in) {
    _objectsToIDs.clear();
    _idsToObjects.clear();
    yaml_source ar(in);
    ar >> *this;

    for (auto& entry : _idsToObjects) {
        if (entry.second) {
            entry.second->OnGameLoadComplete();
        }
    }
}

void* ScriptObject::operator new(std::size_t size) { return SafeAlloc(size); }

void* ScriptObject::operator new(std::size_t, void* object) { return object; }

void ScriptObject::operator delete(void* object) noexcept { SafeFree(object); }

void ScriptObject::operator delete(void* object, void*) noexcept { SafeFree(object); }

BSTSmartPointer<Object> ScriptObject::Bind(std::string_view classNameOverride) noexcept {
    if (classNameOverride.empty()) {
        classNameOverride = GetTypeName();
    }

    auto* vm = VirtualMachine::GetSingleton();
    if (!vm) {
        Logger::Warn("Script object of type {} bound before virtual machine was initialized.", GetTypeName());
        return {};
    }
    auto policy = vm->GetObjectHandlePolicy();

    auto typeLookup = vm->objectTypeToTypeID.find(GetTypeName());
    if (typeLookup == vm->objectTypeToTypeID.end()) {
        return {};
    }
    auto typeID = typeLookup->second;
    BSTSmartPointer<ObjectTypeInfo> classPtr;
    if (!vm->GetScriptObjectType(typeID, classPtr) || !classPtr) {
        return {};
    }

    auto handle = policy->GetHandleForObject(typeID, this);
    BSTSmartPointer<Object> objectPtr;
    if (!vm->FindBoundObject(handle, classPtr->GetName(), objectPtr)) {
        if (vm->CreateObject(classNameOverride, objectPtr) && objectPtr) {
            BSTSmartPointer<ObjectTypeInfo> typeInfo(objectPtr->GetTypeInfo());
            if (typeInfo) {
                auto handlePolicy = vm->GetObjectHandlePolicy();
                if (handlePolicy) {
                    if (handlePolicy->HandleIsType(typeID, handle) && handlePolicy->IsHandleObjectAvailable(handle)) {
                        auto bindPolicy = vm->GetObjectBindPolicy();
                        if (bindPolicy) {
                            bindPolicy->BindObject(objectPtr, handle);
                            ScriptObjectBoundEvent event(objectPtr, this, handle);
                            ScriptObjectStore::GetSingleton().Emit(event);
                        }
                    }
                }
            }
        }
    }
    return std::move(objectPtr);
}

BSTSmartPointer<Object> ScriptObject::GetPapyrusObject() const noexcept {
    auto* vm = VirtualMachine::GetSingleton();
    if (!vm) {
        return nullptr;
    }
    BSTSmartPointer<Object> out;
    vm->FindBoundObject(ScriptObjectStore::GetSingleton().GetID(const_cast<ScriptObject*>(this)), GetTypeName().data(),
                        out);
    return out;
}

optional_ptr<ScriptObject> ScriptObject::FromPapyrusObject(const BSTSmartPointer<Object>& object) noexcept {
    if (!object) {
        return nullptr;
    }
    auto* vm = VirtualMachine::GetSingleton();
    if (!vm) {
        return nullptr;
    }
    return ScriptObjectStore::GetSingleton().FindObject(object->GetHandle());
}

std::string_view ScriptObject::GetTypeName() const noexcept { return TypeName; }

void ScriptObject::OnGameLoadComplete() {}

BSFixedString ScriptObject::ToString() const noexcept { return "Papyrus Script Object"; }

std::size_t ScriptObject::GetHashCode() const noexcept {
    std::hash<VMHandle> hasher;
    return hasher(ScriptObjectStore::GetSingleton().GetID(const_cast<ScriptObject*>(this)));
}

bool ScriptObject::Equals(const ScriptObject* other) const noexcept { return this == other; }

void ScriptObject::Register(std::string_view typeName, const type_registration& source_reg,
                            const type_registration& sink_reg, std::function<ScriptObject*()> ctor) {
    std::unique_lock lock(TypesLock);
    auto result = TypeNames.emplace(typeName.data());
    if (result.second) {
        Types.emplace_back(*result.first);
        TypeConstructors.try_emplace(typeName.data(), std::move(ctor));
        type_registry<yaml_source<>::archive_type>::get().register_type(source_reg);
        type_registry<yaml_sink<>::archive_type>::get().register_type(sink_reg);
    }
}

void FDGE::Binding::Papyrus::PackScriptObjectHandle(Variable* destination, void* source, VMTypeID typeID) {
    if (!destination) {
        return;
    }
    destination->SetNone();
    auto* vm = Internal::VirtualMachine::GetSingleton();
    if (!source || !vm) {
        return;
    }
    auto policy = vm->GetObjectHandlePolicy();
    if (!policy) {
        return;
    }

    BSTSmartPointer<ObjectTypeInfo> classPtr;
    if (!vm->GetScriptObjectType(typeID, classPtr) || !classPtr) {
        return;
    }

    auto handle = policy->GetHandleForObject(typeID, source);
    BSTSmartPointer<Object> objectPtr;
    if (!vm->FindBoundObject(handle, classPtr->GetName(), objectPtr)) {
        if (vm->CreateObject(classPtr->GetName(), objectPtr) && objectPtr) {
            BindID(objectPtr, source, typeID);
            ScriptObjectBoundEvent event(objectPtr, reinterpret_cast<ScriptObject*>(source), handle);
            ScriptObjectStore::GetSingleton().Emit(event);
        }
    }

    if (objectPtr) {
        destination->SetObject(std::move(objectPtr), classPtr->GetRawType());
    }
}

namespace {
    thread_local std::vector<BSTSmartPointer<Object>> DeferredDecRefs;
    thread_local bool InNativeCall{false};

    OnSKSELoaded {
        if (!FDGEConfig::GetProxy()->GetDebug().GetModules().IsScriptObjectsEnabled()) {
            return;
        }

        discard(ScriptObjectStore::GetSingleton());

        static FunctionHook<void(VirtualMachine**)> RegisterTypes(
            RelocationID(55141, 55739), [](VirtualMachine** vm) {
                RegisterTypes(vm);
                std::unique_lock lock(TypesLock);
                for (auto& type : Types) {
                    (*vm)->RegisterObjectType(NextTypeID++, type.data());
                }
            });

        static FunctionHook<VMHandle __fastcall(HandlePolicy*, VMTypeID, void*)> GetHandleForObject(
            RelocationID(52655, 53508), [](auto* self, auto typeID, auto* source) {
                if (typeID > ActiveEffect::VMTYPEID) {
                    auto id = ScriptObjectStore::GetSingleton().Add(reinterpret_cast<ScriptObject*>(source));
                    if (!id) {
                        FDGE::Logger::Error(
                            "Unable to find handle for object, no matching native script object exists.");
                    }
                    return id;
                } else {
                    return GetHandleForObject(self, typeID, source);
                }
            });

        static FunctionHook<void* __fastcall(HandlePolicy*, VMTypeID, VMHandle)> GetObjectForHandle(
            RelocationID(52659, 53512), [](auto* self, auto typeID, auto handle) -> void* {
                if (typeID > ActiveEffect::VMTYPEID) {
                    auto object = ScriptObjectStore::GetSingleton().FindObject(handle);
                    auto* vm = Internal::VirtualMachine::GetSingleton();
                    if (!object || !vm) {
                        return nullptr;
                    }
                    BSTSmartPointer<ObjectTypeInfo> type;
                    vm->GetScriptObjectType(object->GetTypeName(), type);
                    for (; type; type.reset(type->GetParent())) {
                        auto result = vm->objectTypeToTypeID.find(type->GetName());
                        if (result == vm->objectTypeToTypeID.end()) {
                            return nullptr;
                        }
                        if (result->second == typeID) {
                            return object.get_raw();
                        }
                    }
                    return nullptr;
                } else {
                    return GetObjectForHandle(self, typeID, handle);
                }
            });

        static FunctionHook<bool __fastcall(HandlePolicy*, VMTypeID, VMHandle)> HandleIsType(
            RelocationID(52652, 53505), [](auto* self, auto typeID, auto handle) {
                if (typeID > ActiveEffect::VMTYPEID && ScriptObject::IsScriptObject(handle)) {
                    auto* vm = Internal::VirtualMachine::GetSingleton();
                    if (!vm || !vm->GetObjectHandlePolicy()) {
                        return false;
                    }
                    return vm->GetObjectHandlePolicy()->GetObjectForHandle(typeID, handle) != nullptr;
                }
                return HandleIsType(self, typeID, handle);
            });

        static FunctionHook<bool __fastcall(HandlePolicy*, VMHandle)> IsHandleObjectAvailable(
            RelocationID(52653, 53506), [](auto* self, VMHandle handle) {
                if (ScriptObject::IsScriptObject(handle)) {
                    return true;
                }
                return IsHandleObjectAvailable(self, handle);
            });

        static FunctionHook<void __fastcall(HandlePolicy*, VMHandle, BSFixedString&)> ConvertHandleToString(
            RelocationID(52662, 53515), [](auto* self, auto handle, auto& out) {
                if (ScriptObject::IsScriptObject(handle)) {
                    auto obj = ScriptObjectStore::GetSingleton().FindObject(handle);
                    if (obj) {
                        out = obj->ToString();
                    } else {
                        out = "Missing Object";
                    }
                } else {
                    ConvertHandleToString(self, handle, out);
                }
            });

        static FunctionHook<int32_t __fastcall(Object*)> ReleaseObjectReference(
            RelocationID(104253, 97469), [](Object* self) {
                auto handle = self->GetHandle();
                bool isScriptObject = ScriptObject::IsScriptObject(handle);

                if (InNativeCall && isScriptObject) {
                    // Defer script object DecRef calls while in native function handlers. This makes it easier to
                    // work with script objects without risking undesired deletion. A temporary ref count of zero
                    // will not prevent collection, if it recovers a new reference by the end of the native call.
                    DeferredDecRefs.emplace_back(self);
                }

                auto result = ReleaseObjectReference(self);
                if (isScriptObject && result <= 1) {
                    // Object is being collected, cleanup.
                    ScriptObjectStore::GetSingleton().Remove(self->GetHandle());
                }
                return result;
            });

        static FunctionHook<IFunction::CallResult(NF_util::NativeFunctionBase*, BSTSmartPointer<Stack>*, ErrorLogger*,
                                                  VirtualMachine*)>
            NativeFunctionBaseCall(RelocationID(97923, 104651),
                                   [](auto* self, auto* stack, auto* errorLogger, auto* vm) {
                                       InNativeCall = true;
                                       auto result = NativeFunctionBaseCall(self, stack, errorLogger, vm);
                                       InNativeCall = false;
                                       DeferredDecRefs.clear();
                                       return result;
                                   });

        static CallHook<bool(SaveFileHandleReaderWriter*, LoadStorageWrapper*, VMHandle*)> LoadHandleCall(
            RelocationID(98160, 104884), Relocate(0x20B, 0x263), [](auto* self, auto* loadWrapper, auto* out) {
                uint32_t values[8];
                std::memset(values, 0, sizeof(values));
                uint16_t shrt;
                uint16_t save_flags[2];
                if (loadWrapper->Read(2, reinterpret_cast<std::byte*>(save_flags)) !=
                        static_cast<BSStorageDefs::ErrorCode>(BSResource::ErrorCode::kNone) ||
                    loadWrapper->Read(2, reinterpret_cast<std::byte*>(&shrt)) !=
                        static_cast<BSStorageDefs::ErrorCode>(BSResource::ErrorCode::kNone) ||
                    !self->ParseScriptInstance(loadWrapper, values)) {
                    *out = 0xFFFF00000000;
                    return false;
                }
                auto flag = save_flags[0];
                auto type_bits = shrt;
                if (loadWrapper->unk0C) {
                    flag = save_flags[0] >> 8;
                    type_bits = shrt >> 8;
                }
                auto upper = static_cast<VMHandle>(type_bits) << 32;
                if (flag & 4) {
                    *out = upper | values[0] | (static_cast<VMHandle>(flag) << 48);
                } else if (flag & 1) {
                    *out = upper | values[0] | 0x1000000000000;
                } else if (flag & 2) {
                    *out = upper | values[0] | 0x2000000000000;
                } else {
                    *out = upper | values[0];
                }
                return true;
            });

        static CallHook<bool(SaveFileHandleReaderWriter*, SaveStorageWrapper*, VMHandle)> SaveHandleCall(
            RelocationID(98295, 105016), Relocate(0x11B, 0x11C), [](auto* self, auto* saveWrapper, auto handle) {
                REL::Relocation<bool (*)(SaveStorageWrapper*, uint16_t)> WriteOut(REL::ID(53533));

                uint16_t high{0};
                uint32_t low{0};
                if (!(handle & 0xFFFF000000000000) || (handle & 0x1000000000000) != 0 ||
                    (handle & 0x2000000000000) != 0 || (handle & 0x4000000000000) != 0) {
                    low = static_cast<uint32_t>(handle);
                    high = static_cast<uint16_t>(handle >> 32);
                }
                if (WriteOut(saveWrapper, static_cast<uint16_t>(handle >> 48))) {
                    return false;
                }
                if (WriteOut(saveWrapper, high)) {
                    return false;
                }
                if (!self->SaveRefID(saveWrapper, low)) {
                    return false;
                }
                return true;
            });

        static FunctionHook<void(::RE::SkyrimScript::ObjectBindPolicy*, BSTSmartPointer<Object>*, VMHandle)> BindObject(
            ::RE::Offset::BSScript::ObjectBindPolicy::BindObject, [](auto* self, auto* object, auto handle) {
                BindObject(self, object, handle);
                if (ScriptObject::IsScriptObject(handle)) {
                    (*object)->constructed = true;
                    (*object)->valid = true;
                }
            });
    }
}  // namespace

void ScriptObject::FlushDecRefs() { DeferredDecRefs.clear(); }

namespace {
    RegisterScriptType(ScriptObject);

    PapyrusClass(ScriptObject) {
        PapyrusStaticFunction(CreateNewThis)->ScriptObject* {
            auto* callingFrame = CurrentCallStack->top->previousFrame;
            if (!callingFrame) {
                return nullptr;
            }

            auto className = callingFrame->owningObjectType->name;
            ScriptObject* result{nullptr};
            for (auto* type = callingFrame->owningObjectType.get(); type; type = type->GetParent()) {
                auto ctorSearch = TypeConstructors.find(type->GetName());
                if (ctorSearch != TypeConstructors.end()) {
                    result = ctorSearch->second();
                    if (result) {
                        break;
                    } else {
                        auto err = fmt::format(
                            "Script object type {} extends an abstract native type. A parent type will be used.",
                            className.data());
                        FDGE::Logger::Warn(err.c_str());
                        VirtualMachine->GetErrorLogger()->PostErrorImpl(err.c_str(), ErrorLogger::Severity::kError);
                    }
                }
            }
            if (!result) {
                auto err = fmt::format("Attempt to use ScriptObject to instantiate non-ScriptObject class '{}'.",
                                       className.data());
                FDGE::Logger::Warn(err.c_str());
                VirtualMachine->GetErrorLogger()->PostErrorImpl(err.c_str(), ErrorLogger::Severity::kError);
                return nullptr;
            }

            auto obj = result->Bind(className);
            if (!obj) {
                return nullptr;
            }

            return result;
        };

        PapyrusStaticFunction(Initialize, ScriptObject * obj, std::string_view variableName, Any * value)->Error* {
            if (!obj) {
                return new Error("Provided script object was set to None.", CurrentCallStack->top->previousFrame);
            }

            auto handle = ScriptObjectStore::GetSingleton().GetID(obj);
            if (!handle) {
                return new Error("Object provided is not a proper script object.",
                                 CurrentCallStack->top->previousFrame);
            }

            auto* callingFrame = CurrentCallStack->top->previousFrame;
            if (!callingFrame) {
                return new Error("No calling stack frame information found.", CurrentCallStack->top->previousFrame);
            }
            auto className = callingFrame->owningObjectType->name;
            auto type = callingFrame->owningObjectType;

            uint32_t variableIndex{0};
            for (auto* parent = type->GetParent(); parent; parent = parent->GetParent()) {
                variableIndex += parent->GetNumVariables();
            }
            auto totalVars = type->GetNumVariables();
            ObjectTypeInfo::VariableInfo* varInfo = nullptr;
            for (std::uint32_t i = 0; i < totalVars; ++i, ++variableIndex) {
                auto& var = type->GetVariableIter()[i];
                if (var.name == variableName) {
                    varInfo = &var;
                    break;
                }
            }
            if (!varInfo) {
                return new Error("Object type of variable and provided Any do not match.",
                                 CurrentCallStack->top->previousFrame);
            }

            BSSpinLockGuard lock(VirtualMachine->attachedScriptsLock);
            auto lookup = VirtualMachine->attachedScripts.find(handle);
            if (lookup == VirtualMachine->attachedScripts.end()) {
                return new Error("No attached script found for script object instance.",
                                 CurrentCallStack->top->previousFrame);
            }
            for (auto& scriptInstance : lookup->second) {
                switch (varInfo->type.GetUnmangledRawType()) {
                    case TypeInfo::RawType::kNone:
                        scriptInstance->variables[variableIndex].SetNone();
                        break;
                    case TypeInfo::RawType::kBool:
                        scriptInstance->variables[variableIndex].SetBool(std::get<bool>(value->GetValue()));
                        break;
                    case TypeInfo::RawType::kInt:
                        scriptInstance->variables[variableIndex].SetBool(std::get<int32_t>(value->GetValue()));
                        break;
                    case TypeInfo::RawType::kFloat:
                        scriptInstance->variables[variableIndex].SetFloat(std::get<float_t>(value->GetValue()));
                        break;
                    case TypeInfo::RawType::kString:
                        scriptInstance->variables[variableIndex].SetString(std::get<std::string>(value->GetValue()));
                        break;
                    case TypeInfo::RawType::kObject:
                        if (std::holds_alternative<AttachedObjectHandle<TESForm>>(value->GetValue())) {
                        } else if (std::holds_alternative<AttachedObjectHandle<ActiveEffect>>(value->GetValue())) {
                        } else if (std::holds_alternative<AttachedObjectHandle<BGSBaseAlias>>(value->GetValue())) {
                        }
                        break;
                    case TypeInfo::RawType::kNoneArray:
                        break;
                    case TypeInfo::RawType::kBoolArray:
                        break;
                    case TypeInfo::RawType::kIntArray:
                        break;
                    case TypeInfo::RawType::kFloatArray:
                        break;
                    case TypeInfo::RawType::kStringArray:
                        break;
                    case TypeInfo::RawType::kObjectArray:
                        break;
                    default:
                        return new Error(fmt::format("Variable '{}' is of an unidentifiable type.", variableName),
                                         CurrentCallStack->top->previousFrame);
                }
            }

            return nullptr;
        };

        PapyrusFunction(ToString, ScriptObject * self)->BSFixedString {
            if (!self) {
                return "";
            }
            return self->ToString();
        };

        PapyrusFunction(Equals, ScriptObject * self, ScriptObject * other) {
            if (!self) {
                return other == nullptr;
            }
            return self->Equals(other);
        };

        PapyrusFunction(GetHashCode, ScriptObject * self) {
            std::vector<int32_t> results;
            results.resize(2);
            if (self) {
                auto hash = self->GetHashCode();
                results[0] = static_cast<int32_t>(hash);
                results[1] = static_cast<int32_t>(hash >> 32);
            }
            return results;
        };
    }
}  // namespace
