#include <FDGE/Binding/Papyrus/ScriptLoader.h>

#include <FDGE/Hook/CallHook.h>
#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>

using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Hook;
using namespace gluino;
using namespace RE;
using namespace RE::BSScript;
using namespace RE::BSScript::Internal;

void ScriptLoader::LoadScripts(std::filesystem::path directory) {
    auto* vm = VirtualMachine::GetSingleton();
    if (!vm) {
        return;
    }

    LoadContext context(::std::move(directory));

    const auto& overrideScriptPath = ScriptLoader::LoadContext::GetCurrentScriptDirectory();
    if (overrideScriptPath.empty()) {
        Logger::Warn("Loading all scripts in the standard scripts directory 'Data\\Scripts'. "
            "This can cause poor performance.");
    }
    std::filesystem::directory_iterator scriptsDir(
        overrideScriptPath.empty() ? L"Data\\Scripts" : overrideScriptPath.wstring());
    for (auto& file : scriptsDir) {
        if (!file.is_regular_file()) {
            continue;
        }
        auto scriptPath = file.path();
        auto extension = scriptPath.extension().string();
        if (str_equal_to<false>{}(extension, ".pex")) {
            BSFixedString name = scriptPath.stem().string().c_str();
            vm->linker.Process(name);
        }
    }
}

namespace {
    OnSKSELoaded {
        static CallHook<int(char* const, std::size_t, const char*, const char*, const char*, const char*)> GetScriptPath(
            53805, 0x75, [](auto* const buffer, auto size, const auto* format, const auto* path, const auto* className,
                            const auto* suffix) -> int {
                const auto& overrideScriptPath = ScriptLoader::LoadContext::GetCurrentScriptDirectory();
                if (overrideScriptPath.empty()) {
                    return sprintf_s(buffer, size, format, path, className, suffix);
                } else {
                    auto str = overrideScriptPath.string();
                    if (!str.ends_with("/")) {}
                    str += "/";
                    return sprintf_s(buffer, size, format, str.c_str(), className, suffix);
                }
            });
    }
}
