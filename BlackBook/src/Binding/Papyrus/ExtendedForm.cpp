#include <FDGE/Binding/Papyrus/ExtendedForm.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE;
using namespace RE::BSScript;
using namespace RE::BSScript::Internal;

void FDGE::Binding::Papyrus::PackExtendedFormHandle(Variable *destination, const void *form,
                                                    std::string_view typeName, VMTypeID typeID) {
    destination->SetNone();
    if (!form || !typeID || typeName.empty()) {
        return;
    }

    auto* vm = VirtualMachine::GetSingleton();
    if (!vm) {
        return;
    }

    BSTSmartPointer<ObjectTypeInfo> classPtr;
    if (!vm->GetScriptObjectType(typeName, classPtr) || !classPtr) {
        return;
    }

    auto handle = vm->GetObjectHandlePolicy()->GetHandleForObject(typeID, form);

    BSTSmartPointer<Object> objectPtr;
    if (!vm->FindBoundObject(handle, classPtr->GetName(), objectPtr) || !objectPtr) {
        if (vm->CreateObject(classPtr->GetName(), objectPtr) && objectPtr) {
            BSTSmartPointer<ObjectTypeInfo> typeInfo(objectPtr->GetTypeInfo());
            auto handlePolicy = vm->GetObjectHandlePolicy();
            if (handlePolicy) {
                if (handlePolicy->HandleIsType(typeID, handle) && handlePolicy->IsHandleObjectAvailable(handle)) {
                    auto bindPolicy = vm->GetObjectBindPolicy();
                    if (bindPolicy) {
                        bindPolicy->BindObject(objectPtr, handle);
                    }
                }
            }
        }
    }

    if (objectPtr) {
        destination->SetObject(objectPtr, classPtr->GetRawType());
    }
}
