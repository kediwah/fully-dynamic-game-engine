#include <FDGE/Binding/Papyrus/Queue.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

int32_t Queue::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

void Queue::Enqueue(Any* value) {
    std::unique_lock lock(_lock);
    _value.emplace(value);
}

Any* Queue::Dequeue() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return nullptr;
    }
    auto* result = _value.front().GetObject();
    _value.pop();
    return result;
}

Any* Queue::Peek() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return nullptr;
    }
    return _value.front().GetObject();
}

namespace {
    RegisterScriptType(Queue)

    PapyrusClass(Queue) {
        PapyrusStaticFunction(Create) {
            return new Queue();
        };

        PapyrusFunction(__GetSize, Queue* self) {
            return self ? self->GetSize() : 0;
        };

        PapyrusFunction(Enqueue, Queue* self, Any* value) {
            if (self) {
                self->Enqueue(value);
            }
        };

        PapyrusFunction(Dequeue, Queue* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Dequeue();
        };

        PapyrusFunction(Peek, Queue* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Peek();
        };
    }
}
