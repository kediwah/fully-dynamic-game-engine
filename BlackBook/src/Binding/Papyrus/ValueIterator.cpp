#include <FDGE/Binding/Papyrus/ValueIterator.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

Any* ValueIterator::NextAndGetValue() noexcept {
    Next();
    return GetValue();
}

Any* ValueIterator::GetValueAndNext() noexcept {
    auto* result = GetValue();
    Next();
    return result;
}

namespace {
    RegisterScriptType(ValueIterator);

    PapyrusClass(ValueIterator) {
        PapyrusFunction(__IsValid, ValueIterator* self) {
            if (!self) {
                return false;
            }
            return self->IsValid();
        };

        PapyrusFunction(__HasMore, ValueIterator* self) {
            if (!self) {
                return false;
            }
            return self->HasMore();
        };

        PapyrusFunction(__GetValue, ValueIterator* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->GetValue();
        };

        PapyrusFunction(Next, ValueIterator* self) {
            if (!self) {
                return false;
            }
            return self->Next();
        };

        PapyrusFunction(NextAndGetValue, ValueIterator* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->NextAndGetValue();
        };

        PapyrusFunction(GetValueAndNext, ValueIterator* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->GetValueAndNext();
        };
    };
}
