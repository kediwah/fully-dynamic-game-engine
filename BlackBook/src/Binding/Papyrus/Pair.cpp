#include <FDGE/Binding/Papyrus/Pair.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE;

BSFixedString Pair::ToString() const noexcept {
    std::string out;
    if (GetFirst()) {
        out += GetFirst()->ToString().c_str();
        out += ", ";
    } else {
        out += "None, ";
    }
    if (GetSecond()) {
        out += GetSecond()->ToString().c_str();
    } else {
        out += "None";
    }
    return out;
}

std::size_t Pair::GetHashCode() const noexcept {
    std::size_t seed = GetFirst() ? GetFirst()->GetHashCode() : 0;
    seed ^= (GetSecond() ? GetSecond()->GetHashCode() : 0) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    return seed;
}

namespace {
    RegisterScriptType(Pair);

    PapyrusClass(Pair) {
        PapyrusStaticFunction(Create, Any* first, Any* second) {
            return new Pair(first, second);
        };

        PapyrusFunction(__GetFirst, Pair* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->GetFirst();
        };

        PapyrusFunction(__GetSecond, Pair* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->GetSecond();
        };
    }
}
