#include <FDGE/Binding/Papyrus/MapCollection.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

namespace {
    RegisterScriptType(MapCollection)

    PapyrusClass(MapCollection) {
        PapyrusFunction(Get, MapCollection* self, Any* key) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Get(key);
        };

        PapyrusFunction(Put, MapCollection* self, Any* key, Any* value) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Put(key, value);
        };

        PapyrusFunction(TryPut, MapCollection* self, Any* key, Any* value) {
            if (!self) {
                return false;
            }
            return self->TryPut(key, value);
        };

        PapyrusFunction(Contains, MapCollection* self, Any* key) {
            if (!self) {
                return false;
            }
            return self->Contains(key);
        };

        PapyrusFunction(Delete, MapCollection* self, Any* key) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Delete(key);
        };

        PapyrusFunction(GetKeyValueIterator, MapCollection* self) -> KeyValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetKeyValueIterator();
        };
    }
}
