#include <FDGE/Binding/Papyrus/LinkedList.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

int32_t LinkedList::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

bool LinkedList::Clear() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return false;
    }
    _value.clear();
    _head = _tail = 0;
    return true;
}

void LinkedList::Append(Any* value) {
    std::unique_lock lock(_lock);
    ++_version;
    auto id = _nextID++;
    auto& node = _value[id];
    node.Value = value;
    if (!_tail) {
        _head = (_tail = id);
    } else {
        _value[_tail].Next = id;
        node.Previous = _tail;
        _tail = id;
    }
}

void LinkedList::Prepend(Any* value) {
    std::unique_lock lock(_lock);
    ++_version;
    auto id = ++_nextID;
    auto& node = _value[id];
    node.Value = value;
    if (!_head) {
        _head = (_tail = id);
    } else {
        _value[_head].Previous = id;
        node.Next = _head;
        _head = id;
    }
}

Any* LinkedList::PopFront() {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return nullptr;
    }
    auto result = _value.find(_head);
    auto* out = result->second.Value.GetObject();
    _head = result->second.Next;
    _value.erase(result);
    return out;
}

Any* LinkedList::PopBack() {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return nullptr;
    }
    auto result = _value.find(_tail);
    auto* out = result->second.Value.GetObject();
    _tail = result->second.Previous;
    _value.erase(result);
    return out;
}

LinkedListIterator* LinkedList::GetValueIterator() noexcept {
    return new LinkedListIterator(this, false);
}

LinkedListIterator* LinkedList::GetReverseLinkedListIterator() noexcept {
    return new LinkedListIterator(this, true);
}

LinkedListIterator* LinkedList::GetLinkedListIteratorFrom(int32_t index) noexcept {
    uint64_t id = _head;
    for (uint64_t i = 0; i < index && id != 0; ++i) {
        id = _value[id].Next;
    }
    return new LinkedListIterator(this, false, id);
}

LinkedListIterator* LinkedList::GetReverseLinkedListIteratorFrom(int32_t index) noexcept {
    uint64_t id = _tail;
    for (uint64_t i = 0; i < index && id != 0; ++i) {
        id = _value[id].Previous;
    }
    return new LinkedListIterator(this, false, id);
}

LinkedListIterator::LinkedListIterator(LinkedList* parent, bool reverse) noexcept
    : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    if (!parent->_value.empty()) {
        _id = reverse ? parent->_tail : parent->_head;
        _value = parent->_value[_id].Value;
    }
}

LinkedListIterator::LinkedListIterator(LinkedList* parent, bool reverse, uint64_t id) noexcept
    : _parent(parent), _reverse(reverse), _version(parent->_version) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    auto result = parent->_value.find(id);
    if (result != parent->_value.end()) {
        _id = id;
        _value = parent->_value[_id].Value;
    }
}

bool LinkedListIterator::IsValid() noexcept {
    if (!_id) {
        return false;
    }
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    return parent->_value.contains(_id) && parent->_version == _version;
}

bool LinkedListIterator::HasMore() noexcept {
    if (!_id) {
        return false;
    }
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    return parent->_value.contains(_id);
}

Any* LinkedListIterator::GetValue() noexcept {
    return _value.GetObject();
}

bool LinkedListIterator::SetValue(Any* value) noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (!IsValid()) {
        return false;
    }
    _version = ++parent->_version;
    parent->_value[_id].Value = value;
    return true;
}

bool LinkedListIterator::Next() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    auto search = parent->_value.find(_id);
    if (search == parent->_value.end() ||
        ((_reverse && search->first == parent->_head) || (!_reverse && search->first == parent->_tail))) {
        _id = 0;
        _value = {};
        return false;
    }
    _id = _reverse ? search->second.Previous : search->second.Next;
    if (_id) {
        _value = parent->_value.find(_id)->second.Value;
    } else {
        _value = {};
    }
    return true;
}

Any* LinkedListIterator::RemoveAndNext() {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return nullptr;
    }
    std::unique_lock lock(parent->_lock);
    if (!IsValid()) {
        return nullptr;
    }
    _version = ++parent->_version;
    auto search = parent->_value.find(_id);
    auto& node = search->second;
    auto* result = node.Value.GetObject();
    if (node.Previous) {
        parent->_value[node.Previous].Next = node.Next;
    } else {
        parent->_head = node.Next;
    }
    if (node.Next) {
        parent->_value[node.Next].Previous = node.Previous;
    } else {
        parent->_tail = node.Previous;
    }
    if (_reverse) {
        _id = node.Previous;
    } else {
        _id = node.Next;
    }
    parent->_value.erase(search);
    return result;
}

void LinkedListIterator::InsertBefore(Any* value) {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return;
    }
    std::unique_lock lock(parent->_lock);
    if (!IsValid()) {
        if (_version == parent->_version) {
            parent->Prepend(value);
        }
        return;
    }
    auto& thisNode = parent->_value[_id];

    _version = ++parent->_version;
    auto id = ++parent->_nextID;
    auto& node = parent->_value[id];
    node.Value = value;
    node.Next = _id;
    node.Previous = thisNode.Previous;
    thisNode.Previous = id;

    if (!node.Next) {
        parent->_head = id;
    }
}

void LinkedListIterator::InsertAfter(Any* value) {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return;
    }
    std::unique_lock lock(parent->_lock);
    if (!IsValid()) {
        if (_version == parent->_version) {
            parent->Append(value);
        }
        return;
    }
    auto& thisNode = parent->_value[_id];

    _version = ++parent->_version;
    auto id = ++parent->_nextID;
    auto& node = parent->_value[id];
    node.Value = value;
    node.Previous = _id;
    node.Next = thisNode.Next;
    thisNode.Next = id;

    if (!node.Next) {
        parent->_tail = id;
    }
}

namespace {
    RegisterScriptType(LinkedList)
    RegisterScriptType(LinkedListIterator)

    PapyrusClass(LinkedList) {
        PapyrusStaticFunction(Create) {
            return new LinkedList();
        };

        PapyrusFunction(PopFront, LinkedList* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->PopFront();
        };

        PapyrusFunction(PopBack, LinkedList* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->PopBack();
        };

        PapyrusFunction(Append, LinkedList* self, Any* value) {
            if (self) {
                self->Append(value);
            }
        };

        PapyrusFunction(Prepend, LinkedList* self, Any* value) {
            if (self) {
                self->Prepend(value);
            }
        };

        PapyrusFunction(GetListIterator, LinkedList* self) -> LinkedListIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetValueIterator();
        };

        PapyrusFunction(GetReverseListIterator, LinkedList* self) -> LinkedListIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseLinkedListIterator();
        };

        PapyrusFunction(GetReverseValueIterator, LinkedList* self) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseLinkedListIterator();
        };

        PapyrusFunction(GetListIteratorFrom, LinkedList* self, int32_t index) -> LinkedListIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetLinkedListIteratorFrom(index);
        };

        PapyrusFunction(GetValueIteratorFrom, LinkedList* self, int32_t index) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetLinkedListIteratorFrom(index);
        };

        PapyrusFunction(GetReverseListIteratorFrom, LinkedList* self, int32_t index) -> LinkedListIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseLinkedListIteratorFrom(index);
        };

        PapyrusFunction(GetReverseValueIteratorFrom, LinkedList* self, int32_t index) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseLinkedListIteratorFrom(index);
        };
    }
}
