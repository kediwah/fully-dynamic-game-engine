#include <FDGE/Binding/Papyrus/ConsoleCommand.h>

#include <FDGE/Binding/Papyrus/Any.h>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Console/Console.h>

using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Console;
using namespace RE;
using namespace RE::BSScript;

ConsoleCommandExecution::ConsoleCommandExecution(Execution execution) noexcept
    : Execution(std::move(execution)) {
}

ConsoleCommand::ConsoleCommand(std::string_view ns, std::string_view name, std::string_view shortName)
    : _namespace(ns.data()), _name(name.data()), _shortName(shortName.data()) {
}

bool ConsoleCommand::Register(VMHandle handle) {
    VMHandle empty = 0;
    if (!_target.compare_exchange_strong(empty, handle)) {
        return false;
    }
    try {
        _registration = std::move(FDGE::Console::CommandRepository::GetSingleton().Register(
                FDGE::Console::CommandSpec(_namespace, {_name, _shortName},
                                           [this](FDGE::Console::Execution& execution) {
                                               HandleCommand(execution);
                                           })));
    } catch (...) {
        _target = 0;
        return false;
    }
    return true;
}

bool ConsoleCommand::Unregister(VMHandle handle) {
    if (!_target || _target != handle) {
        return false;
    }
    _registration = {};
    _target = 0;
    return true;
}

void ConsoleCommand::HandleCommand(const Execution& execution) {
    VMHandle target = _target;
    if (!target) {
        return;
    }
    auto* vm = Internal::VirtualMachine::GetSingleton();
    if (!vm) {
        return;
    }
    auto* wrapped = new ConsoleCommandExecution(execution);
    auto obj = wrapped->Bind();
    RE::FunctionArguments<ConsoleCommandExecution*> args(std::move(wrapped));
    BSFixedString event("OnExecute");
    vm->SendEvent(target, event, &args);
}

namespace {
    RegisterScriptType(ConsoleCommandExecution)
    RegisterScriptType(ConsoleCommand)

    [[nodiscard]] VMHandle GetCallingHandle(BSTSmartPointer<Stack> stack) {
        if (!stack || !stack->top || !stack->top->previousFrame) {
            return {};
        }
        auto callerVar = stack->top->previousFrame->self;
        if (!callerVar.IsObject()) {
            return false;
        }
        auto caller = callerVar.GetObject();
        if (!caller) {
            return {};
        }
        return caller->GetHandle();
    }

    template <class T>
    [[nodiscard]] Any* GetTarget(ConsoleCommandExecution* self, Any* defaultValue, bool allowImplicit, bool isArray) {
        if (isArray) {
            auto result = self->template OptionalTarget<std::vector<T>>(allowImplicit);
            if (result.has_value()) {
                if constexpr (std::is_pointer_v<T>) {
                    std::vector<AttachedObjectHandle<std::remove_pointer_t<T>>> attached;
                    return new Any(attached);
                } else {
                    return new Any(result.value());
                }
            }
        } else {
            auto result = self->template OptionalTarget<T>(allowImplicit);
            if (result.has_value()) {
                if constexpr (std::is_pointer_v<T>) {
                    return new Any(AttachedObjectHandle<std::remove_pointer_t<T>>(result.value()));
                } else {
                    return new Any(result.value());
                }
            }
        }
        return defaultValue;
    }

    template <class T>
    [[nodiscard]] Any* GetParameter(ConsoleCommandExecution* self, std::string_view name, Any* defaultValue,
                                    bool isArray) {
        if (isArray) {
            auto result = self->template OptionalParameter<std::vector<T>>(name);
            if (result.has_value()) {
                if constexpr (std::is_pointer_v<T>) {
                    std::vector<AttachedObjectHandle<std::remove_pointer_t<T>>> attached;
                    return new Any(attached);
                } else {
                    return new Any(result.value());
                }
            }
        } else {
            auto result = self->template OptionalParameter<T>(name);
            if (result.has_value()) {
                if constexpr (std::is_pointer_v<T>) {
                    return new Any(AttachedObjectHandle<std::remove_pointer_t<T>>(result.value()));
                } else {
                    return new Any(result.value());
                }
            }
        }
        return defaultValue;
    }

    template <class T>
    [[nodiscard]] Any* GetNextArgument(ConsoleCommandExecution* self, Any* defaultValue, bool isArray) {
        if (isArray) {
            auto result = self->template NextOptionalPositional<std::vector<T>>();
            if (result.has_value()) {
                if constexpr (std::is_pointer_v<T>) {
                    std::vector<AttachedObjectHandle<std::remove_pointer_t<T>>> attached;
                    return new Any(attached);
                } else {
                    return new Any(result.value());
                }
            }
        } else {
            auto result = self->template NextOptionalPositional<T>();
            if (result.has_value()) {
                if constexpr (std::is_pointer_v<T>) {
                    return new Any(AttachedObjectHandle<std::remove_pointer_t<T>>(result.value()));
                } else {
                    return new Any(result.value());
                }
            }
        }
        return defaultValue;
    }

    PapyrusClass(ConsoleCommandExecution) {
        PapyrusFunction(__HasTarget, ConsoleCommandExecution* self) {
            if (!self) {
                return false;
            }
            try {
                self->NoTarget();
            } catch (...) {
                return true;
            }
            return false;
        };

        PapyrusFunction(__HasExplicitTarget, ConsoleCommandExecution* self) {
            if (!self) {
                return false;
            }
            try {
                self->NoExplicitTarget();
            } catch (...) {
                return true;
            }
            return false;
        };

        PapyrusFunction(__SetResult, ConsoleCommandExecution* self, Any* value) {
            if (!value) {
                self->SetResult(nullptr);
            }
            std::visit([&]([[maybe_unused]] auto&& val) {
                // TODO:
            }, value->GetValue());
        };

        PapyrusFunction(GetTarget, ConsoleCommandExecution* self, std::string typeName, Any* defaultValue,
                        bool allowImplicit) -> Any* {
            bool isArray = typeName.ends_with("[]");
            if (isArray) {
                typeName.resize(typeName.size() - 2);
            }

            if (typeName == "Bool") {
                return GetTarget<bool>(self, defaultValue, allowImplicit, isArray);
            } else if (typeName == "Int") {
                return GetTarget<int32_t>(self, defaultValue, allowImplicit, isArray);
            } else if (typeName == "Float") {
                return GetTarget<float_t>(self, defaultValue, allowImplicit, isArray);
            } else if (typeName == "String") {
                return GetTarget<std::string>(self, defaultValue, allowImplicit, isArray);
            } else if (typeName == "Form") {
                return GetTarget<TESForm*>(self, defaultValue, allowImplicit, isArray);
            }
            return defaultValue;
        };

        PapyrusFunction(GetParameter, ConsoleCommandExecution* self, std::string typeName,
                        std::string_view name, Any* defaultValue) -> Any* {
            bool isArray = typeName.ends_with("[]");
            if (isArray) {
                typeName.resize(typeName.size() - 2);
            }

            if (typeName == "Bool") {
                return GetParameter<bool>(self, name, defaultValue, isArray);
            } else if (typeName == "Int") {
                return GetParameter<int32_t>(self, name, defaultValue, isArray);
            } else if (typeName == "Float") {
                return GetParameter<float_t>(self, name, defaultValue, isArray);
            } else if (typeName == "String") {
                return GetParameter<std::string>(self, name, defaultValue, isArray);
            } else if (typeName == "Form") {
                return GetParameter<TESForm*>(self, name, defaultValue, isArray);
            }
            return defaultValue;
        };

        PapyrusFunction(HasFlag, ConsoleCommandExecution* self, std::string_view name, std::string_view shortName) {
            if (!self) {
                return false;
            }
            return self->Flag(name, shortName);
        };

        PapyrusFunction(NextArgument, ConsoleCommandExecution* self, std::string typeName,
                        Any* defaultValue) -> Any* {
            bool isArray = typeName.ends_with("[]");
            if (isArray) {
                typeName.resize(typeName.size() - 2);
            }

            if (typeName == "Bool") {
                return GetNextArgument<bool>(self, defaultValue, isArray);
            } else if (typeName == "Int") {
                return GetNextArgument<int32_t>(self, defaultValue, isArray);
            } else if (typeName == "Float") {
                return GetNextArgument<float_t>(self, defaultValue, isArray);
            } else if (typeName == "String") {
                return GetNextArgument<std::string>(self, defaultValue, isArray);
            } else if (typeName == "Form") {
                return GetNextArgument<TESForm*>(self, defaultValue, isArray);
            }
            return defaultValue;
        };
    }

    PapyrusClass(ConsoleCommand) {
        PapyrusStaticFunction(Run, std::string_view command) {
            auto* factory = IFormFactory::GetConcreteFormFactoryByType<Script>();
            if (!factory) {
                return false;
            }
            auto* script = factory ? factory->Create() : nullptr;
            if (!script) {
                return false;
            }
            script->SetCommand(command);
            script->CompileAndRun(RE::Console::GetSelectedRef().get());
            delete script;
            return true;
        };

        PapyrusStaticFunction(RunOn, std::string_view command, TESObjectREFR* target) {
            auto* factory = IFormFactory::GetConcreteFormFactoryByType<Script>();
            if (!factory) {
                return false;
            }
            auto* script = factory ? factory->Create() : nullptr;
            if (!script) {
                return false;
            }
            script->SetCommand(command);
            script->CompileAndRun(target);
            delete script;
            return true;
        };

        PapyrusStaticFunction(Create, std::string_view ns,
                              std::string_view name, std::string_view shortName) -> ConsoleCommand* {
            if (ns.empty() || (name.empty() && shortName.empty())) {
                return nullptr;
            }
            if (name.empty()) {
                name = shortName;
                shortName = "";
            }
            return new ConsoleCommand(ns, name, shortName);
        };

        PapyrusStaticFunction(Print, std::string_view text) {
            FDGE::Console::Print(text);
        };

        PapyrusFunction(__GetNamespace, ConsoleCommand* self) -> std::string_view {
            if (!self) {
                return "";
            }
            return self->GetNamespace();
        };

        PapyrusFunction(__GetName, ConsoleCommand* self) -> std::string_view {
            if (!self) {
                return "";
            }
            return self->GetName();
        };

        PapyrusFunction(__GetShortName, ConsoleCommand* self) -> std::string_view {
            if (!self) {
                return "";
            }
            return self->GetShortName();
        };

        PapyrusFunction(__HasRegistration, ConsoleCommand* self) {
            if (!self) {
                return false;
            }
            return self->GetTarget() != 0;
        };

        PapyrusFunction(RegisterForExecute, ConsoleCommand* self) {
            if (!self || self->GetTarget()) {
                return false;
            }
            return self->Register(GetCallingHandle(CurrentCallStack));
        };

        PapyrusFunction(UnregisterForExecute, ConsoleCommand* self) {
            if (!self) {
                return false;
            }
            return self->Unregister(GetCallingHandle(CurrentCallStack));
        };
    }
}
