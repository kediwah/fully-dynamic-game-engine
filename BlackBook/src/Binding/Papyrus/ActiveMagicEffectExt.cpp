#pragma once

#include <FDGE/Binding/Papyrus/ExtendedForm.h>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE;
using namespace RE::BSScript;

namespace {
    PapyrusClass(ActiveMagicEffectExt) {
        PapyrusStaticFunction(GetActiveMagicEffects, Actor* target, EffectSetting* effect) -> std::vector<ActiveEffect*> {
            std::vector<ActiveEffect*> results;
            if (!target || !effect) {
                return results;
            }
            for (auto* entry : *target->GetActiveEffectList()) {
                if (entry && entry->effect && entry->effect->baseEffect == effect) {
                    results.emplace_back(entry);
                }
            }
            return results;
        };

        PapyrusStaticFunction(GetActiveMagicEffectObject, ActiveEffect* effect,
                              std::string_view className) -> AttachedObjectHandle<ActiveEffect> {
            if (!effect || className.empty()) {
                return {};
            }
            auto handle = VirtualMachine->GetObjectHandlePolicy()->GetHandleForObject(ActiveEffect::VMTYPEID, effect);
            BSSpinLockGuard lock(VirtualMachine->attachedScriptsLock);
            auto result = VirtualMachine->attachedScripts.find(handle);
            if (result == VirtualMachine->attachedScripts.end()) {
                return {};
            }
            BSFixedString iClassName = className;
            for (auto& attachedScript : result->second) {
                if (iClassName == attachedScript->GetTypeInfo()->GetName()) {
                    return BSTSmartPointer<Object>(attachedScript.get());
                }
            }
            return {};
        };
    }
}
