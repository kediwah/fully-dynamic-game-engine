#include <FDGE/Binding/Papyrus/SequenceCollection.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

namespace {
    RegisterScriptType(SequenceCollection);

    PapyrusClass(SequenceCollection) {
        PapyrusFunction(Contains, SequenceCollection* self, Any* value) {
            return self ? self->Contains(value) : false;
        };

        PapyrusFunction(Count, SequenceCollection* self, Any* value) {
            return self ? self->Count(value) : 0;
        };

        PapyrusFunction(IndexOf, SequenceCollection* self, Any* value) {
            return self ? self->IndexOf(value) : -1;
        };
    };
}
