#pragma once

#include <FDGE/Binding/Papyrus/ExtendedForm.h>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE;
using namespace RE::BSScript;

namespace {
    PapyrusClass(AliasExt) {
        PapyrusStaticFunction(GetAliasByName, TESQuest* quest, std::string_view name) -> BGSBaseAlias* {
            if (!quest || name.empty()) {
                return nullptr;
            }
            for (auto* alias : quest->aliases) {
                if (alias && alias->aliasName == name) {
                    return alias;
                }
            }
            return nullptr;
        };

        PapyrusStaticFunction(GetAliasByID, TESQuest* quest, int32_t id) -> BGSBaseAlias* {
            if (!quest || id > 0x7FFFFFFF || id < 0) {
                return nullptr;
            }
            if (quest->aliases.size() <= static_cast<uint32_t>(id)) {
                return nullptr;
            }
            return quest->aliases[id];
        };

        PapyrusStaticFunction(GetAliasObject, BGSBaseAlias* alias,
                              std::string_view className) -> AttachedObjectHandle<BGSBaseAlias> {
            if (!alias || className.empty()) {
                return {};
            }
            auto handle = VirtualMachine->GetObjectHandlePolicy()->GetHandleForObject(alias->GetVMTypeID(), alias);
            BSSpinLockGuard lock(VirtualMachine->attachedScriptsLock);
            auto result = VirtualMachine->attachedScripts.find(handle);
            if (result == VirtualMachine->attachedScripts.end()) {
                return {};
            }
            BSFixedString iClassName = className;
            for (auto& attachedScript : result->second) {
                if (iClassName == attachedScript->GetTypeInfo()->GetName()) {
                    return BSTSmartPointer<Object>(attachedScript.get());
                }
            }
            return {};
        };
    }
}