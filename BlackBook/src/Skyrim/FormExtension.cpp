#include <FDGE/Hook/FunctionHook.h>
#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <FDGE/Skyrim/FormExtension.h>
#include <FDGE/System.h>
#include <FDGE/Util/Maps.h>

#include "../FDGEConfig.h"
#include "Database.h"

using namespace FDGE;
using namespace FDGE::Hook;
using namespace FDGE::Skyrim;
using namespace FDGE::Util;
using namespace RE;
using namespace REL;

namespace {
    struct PreLoadExteriorCellData {
        RE::FormID EncounterZoneFormID;
        RE::FormID LocationFormID;
        phmap::flat_hash_map<RE::TESRegionData::Type, RE::FormID> RegionFormIDs;
    };

    Util::parallel_flat_hash_map<RE::FormID, BSFixedString> formEditorIDs;

    istr_flat_hash_map<std::string_view, std::ifstream> fileInputs;

    std::ifstream* currentFileInput{nullptr};

    RE::FormID currentWorldSpace{0};

    phmap::flat_hash_map<RE::FormID, PreLoadExteriorCellData> preLoadExteriorCells;

    bool IsLoading{true};

    RE::FormID ToGlobalFormID(RE::TESFile* file, RE::FormID rawFormID) {
        if (!file || file->compileIndex == 0xFF) {
            return 0;
        }
        std::uint32_t rawIndex = (0xFF000000 & rawFormID) >> 24;
        std::uint32_t rawSmallIndex = (0x00FFF000 & rawFormID) >> 12;
        std::uint32_t fullMasters = 0;
        std::uint32_t smallMasters = 0;
        for (std::uint32_t i = 0; i < file->masterCount; ++i) {
            const auto* master = file->masterPtrs[i];
            if (master->compileIndex == 0xFE) {
                if (rawIndex == 0xFE && rawSmallIndex == smallMasters) {
                    return 0xFE000000 | (master->smallFileCompileIndex << 12) | (rawFormID & 0x00000FFF);
                }
                ++smallMasters;
            } else {
                if (rawIndex == fullMasters) {
                    return (master->compileIndex << 24) | (rawFormID & 0x00FFFFFF);
                }
                ++fullMasters;
            }
        }
        if (file->compileIndex == 0xFE) {
            return 0xFE000000 | (file->smallFileCompileIndex << 12) | (rawFormID & 0x00000FFF);
        }
        return (file->compileIndex << 24) | (rawFormID & 0x00FFFFFF);
    }

    void HookEditorIDs() {
        static FunctionHook<const char*(const RE::TESForm*)> GetFormEditorID(
            RelocationID(10827, 10879), [](auto* self) {
                if (!self) {
                    return "";
                }
                BSFixedString result;
                formEditorIDs.if_contains(self->GetFormID(), [&](auto& value) { result = value.second; });
                return result.c_str();
            });

        static FunctionHook<bool(RE::TESForm*, const char*)> SetFormEditorID(
            RelocationID(10883, 10926), [](auto* self, const auto* editorID) {
                if (!self) {
                    return false;
                }
                SetFormEditorID(self, editorID);
                bool isEmpty = editorID == nullptr || editorID[0] == '\0';
                auto allForms = TESForm::GetAllFormsByEditorID();
                RE::BSWriteLockGuard lock(allForms.second.get());
                auto oldResult = formEditorIDs.find(self->GetFormID());
                if (oldResult != formEditorIDs.end()) {
                    allForms.first->erase(oldResult->second);
                    if (isEmpty) {
                        formEditorIDs.erase(oldResult);
                        return true;
                    }
                }
                BSFixedString interned(editorID);
                allForms.first->emplace(interned, self);
                formEditorIDs[self->GetFormID()] = interned;
                return true;
            });
    }

    void ReserveEditorIDs() {
        auto reservation = FDGEConfig::GetProxy()->GetPerformance().GetEditorIDReservation();
        formEditorIDs.reserve(reservation);
        Logger::Debug("Reserved {} entries for editor ID population.", reservation);
    }

    void ReserveCells() {
        auto reservation = FDGEConfig::GetProxy()->GetPerformance().GetCellReservation();
        exteriorCells.reserve(reservation);
        Logger::Debug("Reserved {} entries for cell population.", reservation);
    }

    void HookExteriorCellData() {
        static FunctionHook<void*(RE::TESForm*, RE::TESFile*)> LoadWorldData(
            RelocationID(13594, 13690), [](auto* form, auto* file) {
                if (!IsLoading) {
                    return LoadWorldData(form, file);
                }
                auto* result = LoadWorldData(form, file);
                switch (form->GetFormType()) {
                    case RE::FormType::WorldSpace: {
                        currentWorldSpace = ToGlobalFormID(file, form->GetFormID());
                        const auto lookupResult = fileInputs.find(file->fileName);
                        if (lookupResult != fileInputs.end()) {
                            currentFileInput = &lookupResult->second;
                        } else {
                            std::string path = file->path;
                            path += file->fileName;
                            currentFileInput =
                                &fileInputs.emplace(file->fileName, std::ifstream(path, std::ios::binary))
                                     .first->second;
                        }
                    }
                    default:
                        break;
                }
                return result;
            });

        static FunctionHook<void*(RE::TESFile*)> SkipGrup(RelocationID(13889, 13973), [](auto* file) {
            if (!IsLoading) {
                return SkipGrup(file);
            }
            const auto allForms = RE::TESForm::GetAllFormsByEditorID();
            switch (file->currentform.formID) {
                case 0x4: {  // Exterior cell children
                    const auto startPosition = file->fileOffset;
                    const auto endOffset = startPosition + file->currentform.length;
                    std::string path = file->path;
                    path += file->fileName;
                    std::ifstream& pluginFile = *currentFileInput;
                    if (!pluginFile.is_open()) {
                        SKSE::log::error("Unable to read exterior cells skipped for file {}.", file->fileName);
                        break;
                    }
                    pluginFile.seekg(startPosition);

                    // 256 bytes is the soft EDID limit in the Creation Kit, although the ESP format has no limit.
                    // char editorID[256];
                    // Manually parse CELL and reference data for exterior cells, not loaded on game start.
                    while (pluginFile.tellg() < endOffset) {
                        // Read header.
                        std::uint32_t formType;
                        std::uint32_t size;
                        std::uint32_t flags;
                        RE::FormID formID;
                        pluginFile.read(reinterpret_cast<char*>(&formType), sizeof(formType));
                        pluginFile.read(reinterpret_cast<char*>(&size), sizeof(size));
                        pluginFile.read(reinterpret_cast<char*>(&flags), sizeof(flags));
                        pluginFile.read(reinterpret_cast<char*>(&formID), sizeof(formID));
                        pluginFile.ignore(8);
                        const auto formStart = pluginFile.tellg();
                        switch (_byteswap_ulong(formType)) {
                            case 'GRUP': {
                                if (formID != 0x4 && formID != 0x5) {
                                    pluginFile.seekg(size - 24, std::ios::cur);
                                }
                                break;
                            }
                            case 'CELL': {
                                auto cellGlobalFormID = ToGlobalFormID(file, formID);
                                exteriorCells.try_emplace(cellGlobalFormID);
                                worldSpaceCells[currentWorldSpace].emplace(cellGlobalFormID);
                                std::uint32_t leftToRead = size;
                                boost::iostreams::filtering_istream input;
                                if (flags & 0x40000) {
                                    pluginFile.read(reinterpret_cast<char*>(&leftToRead), sizeof(leftToRead));
                                    input.push(boost::iostreams::zlib_decompressor());
                                }
                                input.push(pluginFile);

                                PreLoadExteriorCellData data;
                                RE::FormID cellFormID = ToGlobalFormID(file, formID);

                                while (!input.eof() && leftToRead > 0) {
                                    std::uint32_t recordType;
                                    std::uint16_t recordSize;
                                    input.read(reinterpret_cast<char*>(&recordType), sizeof(recordType));
                                    input.read(reinterpret_cast<char*>(&recordSize), sizeof(recordSize));
                                    leftToRead -= (6 + recordSize);

                                    switch (_byteswap_ulong(recordType)) {
//                                        case 'EDID': // Editor ID
//                                        {
//                                            input.read(editorID, recordSize);
//                                            const auto& newEditorID = (editorIDs[formID] = editorID);
//                                            editorIDForms[newEditorID.c_str()] = formID;
//                                            break;
//                                        }
                                        case 'XLCN': {  // Location
                                            RE::FormID location;
                                            input.read(reinterpret_cast<char*>(&location), sizeof(location));
                                            data.LocationFormID = ToGlobalFormID(file, location);
                                            break;
                                        }
                                        case 'XEZN': {  // Encounter Zone
                                            RE::FormID zone;
                                            input.read(reinterpret_cast<char*>(&zone), sizeof(zone));
                                            data.EncounterZoneFormID = ToGlobalFormID(file, zone);
                                            break;
                                        }
                                        default:
                                            input.ignore(recordSize);
                                    }
                                }
                                preLoadExteriorCells[cellFormID] = data;
                                pluginFile.seekg(formStart, std::ios::beg);
                                pluginFile.seekg(size, std::ios::cur);
                                break;
                            }
                            case 'LAND':
                                pluginFile.seekg(size, std::ios::cur);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return SkipGrup(file);
        });
    }

    void CommitExteriorCells() {
        for (const auto& cell : preLoadExteriorCells) {
            if (cell.second.LocationFormID) {
                locationCells[cell.second.LocationFormID].emplace(cell.first);
            }
            if (cell.second.EncounterZoneFormID) {
                encounterZoneCells[cell.second.EncounterZoneFormID].emplace(cell.first);
            }
            for (const auto& regionEntry : cell.second.RegionFormIDs) {
                regionCells[regionEntry.second].emplace(cell.first);
            }
        }
        Logger::Info("Analyzed {} offline exterior cells.", exteriorCells.size());
        preLoadExteriorCells.clear();
    }

    OnSKSELoaded {
        if (FDGEConfig::GetProxy()->GetDebug().GetModules().IsEditorIDPreservationEnabled()) {
            HookEditorIDs();
            ReserveEditorIDs();

            System().ListenForever([](const SkyrimPluginsUnloadedEvent&) {
                formEditorIDs.clear();
                ReserveEditorIDs();
            });

            System().ListenForever([](const SkyrimPluginsLoadedEvent&) {
                Logger::Info("Preserved {} editor IDs in loaded mods.", formEditorIDs.size());
            });
        } else {
            Logger::Warn("Editor ID preservation module is disabled, skipping editor ID hooks.");
        }

        if (FDGEConfig::GetProxy()->GetDebug().GetModules().IsExteriorCellAnalysisEnabled()) {
            HookExteriorCellData();
            ReserveCells();

            System().ListenForever([](const SkyrimPluginsUnloadedEvent&) {
                IsLoading = true;
                exteriorCells.clear();
                ReserveCells();
            });

            System().ListenForever([](const SkyrimPluginsLoadedEvent&) {
                IsLoading = false;
                CommitExteriorCells();
                fileInputs.clear();
            });
        } else {
            Logger::Warn("Exterior cell analysis module is disabled, skipping world data load hooks.");
        }
    }
}  // namespace
