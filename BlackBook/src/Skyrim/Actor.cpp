#include <FDGE/Binding/Papyrus/EventDispatcher.h>
#include <FDGE/Plugin.h>
#include <FDGE/Skyrim/Game.h>

using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Skyrim;
using namespace RE;

namespace {
    [[nodiscard]] inline auto& GetOnActorLoaded() noexcept {
        static SelectivePapyrusEventDispatcher<Actor*, bool> value("ActorLoaded", "FormEvents");
        return value;
    }

    OnSKSELoaded {
        Game.ListenForever([](const CellsLoadingEvent& event) {
            for (auto& cell : event.GetCells()) {
                for (auto actor : cell.GetActors()) {
                    GetOnActorLoaded()(actor.GetForm().get(), actor.IsInitializing());
                }
            }
        });
    }
}
