#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

#include "Database.h"

using namespace FDGE;
using namespace FDGE::Skyrim;
using namespace gluino;
using namespace phmap;
using namespace RE;

namespace {
    struct snapshot_deleter {
        void operator()(void* ptr) const noexcept {
            std::free(ptr);
        }
    };

    sync_set<TESForm*> emptyFormSet;
    std::vector<TESForm*> emptyFormVector;
    sync_set<BGSListForm*> emptyFormListSet;
    sync_set<BGSLocation*> emptyLocationSet;
    sync_set<TESRegion*> emptyRegionSet;

    node_hash_map<FormType, std::vector<TESForm*>> formsByType;

    node_hash_map<FormID, sync_set<TESForm*>> formLeveledLists;
    node_hash_map<FormID, sync_set<BGSListForm*>> formFormLists;
    node_hash_map<FormID, sync_set<BGSLocation*>> locationChildren;
    node_hash_map<FormID, sync_set<TESRegion*>> worldSpaceRegions;
    node_hash_map<FormID, sync_set<TESForm*>> enchantmentItems;
    node_hash_map<FormID, sync_set<TESForm*>> magicEffectUsers;
    node_hash_map<FormID, sync_set<TESForm*>> spellUsers;
    node_hash_map<FormID, sync_set<TESForm*>> itemContainers;

    parallel_flat_hash_map<FormID, std::unique_ptr<void, snapshot_deleter>> snapshots;

    OnSKSELoad(10) {
        discard(Database::GetSingleton());
    }
}

node_hash_map<FormID, sync_set<std::variant<FormID, TESObjectCELL*>>> FDGE::Skyrim::locationCells;

node_hash_map<FormID, sync_set<std::variant<FormID, TESObjectCELL*>>> FDGE::Skyrim::worldSpaceCells;

node_hash_map<FormID, sync_set<std::variant<FormID, TESObjectCELL*>>> FDGE::Skyrim::encounterZoneCells;

node_hash_map<FormID, sync_set<std::variant<FormID, TESObjectCELL*>>> FDGE::Skyrim::regionCells;

phmap::node_hash_map<RE::FormID, ExteriorCellState> FDGE::Skyrim::exteriorCells;

Database& Database::GetSingleton() noexcept {
    static Database instance;
    return instance;
}

Database::Database() noexcept {
    System().ListenForever([](const SkyrimPluginsLoadedEvent) {
        auto allForms = TESForm::GetAllForms();
        BSReadLockGuard lock(allForms.second.get());
        for (auto& entry: *allForms.first) {
            formsByType.try_emplace(entry.second->GetFormType()).first->second.emplace_back(entry.second);
            switch (entry.second->GetFormType()) {
                case FormType::FormList:
                    for (auto& item: entry.second->As<BGSListForm>()->forms) {
                        if (reinterpret_cast<uintptr_t>(item) > REL::Module::get().base()) {
                            formFormLists[item->GetFormID()].emplace(entry.second->As<BGSListForm>());
                        }
                    }
                    break;
                case FormType::LeveledItem:
                case FormType::LeveledNPC:
                case FormType::LeveledSpell:
                    for (auto& item: entry.second->As<TESLeveledList>()->entries) {
                        if (reinterpret_cast<uintptr_t>(item.form) > REL::Module::get().base()) {
                            formLeveledLists[item.form->GetFormID()].emplace(entry.second);
                        }
                    }
                    break;
                case FormType::Location: {
                    auto* loc = entry.second->As<BGSLocation>();
                    if (loc->parentLoc &&
                        reinterpret_cast<uintptr_t>(loc->parentLoc) > REL::Module::get().base()) {
                        locationChildren[loc->parentLoc->GetFormID()].emplace(loc);
                    }
                    break;
                }
                case FormType::Region: {
                    auto* region = entry.second->As<TESRegion>();
                    if (region->worldSpace &&
                        reinterpret_cast<uintptr_t>(region->worldSpace) > REL::Module::get().base()) {
                        worldSpaceRegions[region->worldSpace->GetFormID()].emplace(region);
                    }
                    break;
                }
                case FormType::Weapon: {
                    auto* weap = entry.second->As<TESObjectWEAP>();
                    if (weap->formEnchanting &&
                        reinterpret_cast<uintptr_t>(weap->formEnchanting) > REL::Module::get().base()) {
                        enchantmentItems[weap->formEnchanting->GetFormID()].emplace(weap);
                    }
                    if (weap->criticalData.effect &&
                        reinterpret_cast<uintptr_t>(weap->criticalData.effect) >
                        REL::Module::get().base()) {
                        spellUsers[weap->criticalData.effect->GetFormID()].emplace(weap);
                    }
                    break;
                }
                case FormType::Armor: {
                    auto* armo = entry.second->As<TESObjectARMO>();
                    if (armo->formEnchanting &&
                        reinterpret_cast<uintptr_t>(armo->formEnchanting) > REL::Module::get().base()) {
                        enchantmentItems[armo->formEnchanting->GetFormID()].emplace(armo);
                    }
                    break;
                }
                case FormType::NPC: {
                    // TODO: Find spells.
                    // TODO: Find leveled spells.
                    // TODO: Find items.
                    break;
                }
                case FormType::ActorCharacter: {
                    auto* actor = entry.second->As<Actor>();
                    for (auto* addedSpell : actor->addedSpells) {
                        if (addedSpell && reinterpret_cast<uintptr_t>(addedSpell) > REL::Module::get().base()) {
                            enchantmentItems[addedSpell->GetFormID()].emplace(actor);
                        }
                    }
                    // TODO: Find added items.
                    break;
                }
                case FormType::Spell: {
                    for (auto* effect : entry.second->As<SpellItem>()->effects) {
                        if (effect->baseEffect &&
                            reinterpret_cast<uintptr_t>(effect->baseEffect) > REL::Module::get().base()) {
                            magicEffectUsers[effect->baseEffect->GetFormID()].emplace(entry.second);
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }
    });

    System().ListenForever([](const SkyrimPluginsUnloadedEvent) {
        formsByType.clear();

        formLeveledLists.clear();
        formFormLists.clear();
        locationChildren.clear();
        worldSpaceRegions.clear();
        enchantmentItems.clear();
        magicEffectUsers.clear();
        spellUsers.clear();
        itemContainers.clear();

        encounterZoneCells.clear();
        worldSpaceCells.clear();
        regionCells.clear();
    });
}

void Database::RevertAll() {
    // TODO:
}

FormData Database::LookupForm(TESForm* form) const noexcept {
    auto* formLists = &emptyFormListSet;
    auto result = formFormLists.find(form->GetFormID());
    if (result != formFormLists.end()) {
        formLists = &result->second;
    }
    // TODO: Handle snapshotting.
    return FormData(*formLists);
}

const std::vector<RE::TESForm*>& Database::GetForms(FormType formType) const noexcept {
    auto result = formsByType.find(formType);
    if (result == formsByType.end()) {
        return emptyFormVector;
    }
    return result->second;
}
