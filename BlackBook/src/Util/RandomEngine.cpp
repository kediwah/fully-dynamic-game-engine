#include <FDGE/Util/RandomEngine.h>

using namespace FDGE;
using namespace gluino;

#pragma warning(push)
// Suppress warning about failure to use "if constexpr" for a constant if originating in the PCG header.
#pragma warning(disable: 4127)
gluino::synchronized_random_engine<pcg32>& Util::GetDefaultRandomEngine() noexcept {
    static gluino::synchronized_random_engine<pcg32> defaultRandomEngine;
    return defaultRandomEngine;
}

gluino::synchronized_random_engine<pcg32_fast>& Util::GetFastRandomEngine() noexcept {
    static gluino::synchronized_random_engine<pcg32_fast> fastRandomEngine;
    return fastRandomEngine;
}
#pragma warning(pop)
