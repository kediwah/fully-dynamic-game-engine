#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Dymod/DymodLoader.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Dymod;
using namespace gluino;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"DymodList", "lsmods"}, [](Execution& execution) {
                        execution.NoExplicitTarget();
                        bool sort = execution.Flag("sort", "s");
                        auto loadOrder = DymodLoader::GetSingleton().GetLoadOrder();
                        if (sort) {
                            std::sort(loadOrder.begin(), loadOrder.end(), [](const auto& a, const auto& b) {
                                return std::less()(a->GetID(), b->GetID());
                            });
                        }
                        for (auto& mod : loadOrder) {
                            auto str = mod->GetID();
                            std::string id = string_cast<char>(str).data();
                            std::string info;
                            if (!mod->GetName().empty()) {
                                str = mod->GetName();
                                info += string_cast<char>(str).data();
                            }
                            if (!info.empty()) {
                                info += " ";
                            }
                            info += "v";
                            info += std::to_string(mod->GetVersion());
                            Print("{} ({})", id, info);
                        }
                        Print("Dymods refreshed, {0} mods loaded.", DymodLoader::GetSingleton().GetLoadOrder().size());
                    }));
        });
    }
}
