#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>
#include <FDGE/Util/Strings.h>
#include <srell.hpp>

#include "../../ThirdParty/versionlibdb.h"

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Dymod;
using namespace gluino;
using namespace srell;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                CommandSpec("fdge", {"DumpAddresses", "addrdump"}, [](Execution& execution) {
                    execution.NoExplicitTarget();

                    FDGE_WIN_CHAR_TYPE myDocumentsPath[MAX_PATH];
                    auto result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, myDocumentsPath);
                    if (result != S_OK) {
                        Console::Print("Unable to determine documents path, skipping Address Library dump.");
                        return;
                    }
                    std::filesystem::path basePath(myDocumentsPath);
                    basePath /= "My Games";
                    basePath /= "Skyrim Special Edition";
                    basePath /= "Address Library";
                    std::filesystem::create_directories(basePath);

                    regex versionLibPattern(R"(version(lib)?-((\d+)-(\d+)-(\d+)-(\d+)).bin)",
                                            regex::optimize | regex::icase);

                    int currentMajor;
                    int currentMinor;
                    int currentPatch;
                    int currentBuild;
                    VersionDb().GetExecutableVersion(currentMajor, currentMinor, currentPatch, currentBuild);

                    std::filesystem::directory_iterator dir(R"(Data\SKSE\Plugins)");
                    for (auto& entry : dir) {
                        smatch match;
                        auto filename = entry.path().filename().string();
                        if (!regex_match(filename, match, versionLibPattern)) {
                            continue;
                        }

                        VersionDb db;
                        int major = std::stoi(match[3].str());
                        int minor = std::stoi(match[4].str());
                        int patch = std::stoi(match[5].str());
                        int build = std::stoi(match[6].str());
                        if (major == currentMajor && minor == currentMinor && patch == currentPatch &&
                            build == currentBuild) {
                            Console::Print(
                                "Found version database file '{}' (match for current Skyrim runtime version).",
                                match[0].str());
                        } else {
                            Console::Print("Found version database file '{}'.", match[0].str());
                        }
                        db.Load(major, minor, patch, build);

                        std::filesystem::path outputPath(basePath);
                        outputPath /= "version";
                        outputPath += match[1].str();
                        outputPath += "-offsets-";
                        outputPath += match[2].str();
                        outputPath += ".txt";
                        auto outputPathStr = outputPath.string();
                        Console::Print("Dumping version database to '{}'.", outputPathStr);
                        db.Dump(outputPathStr);
                    }

                    Console::Print("Dump of Address Library IDs complete.");
                }));
        });
    }
}  // namespace
