#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"KillHostileActors", "killhostile"}, [](Execution& execution) {
                        auto* target = execution.OptionalTarget<Actor*>(PlayerCharacter::GetSingleton());
                        auto combatOnly = execution.Flag("combatOnly", "c");

                        for (auto actorHandle : RE::ProcessLists::GetSingleton()->highActorHandles) {
                            auto actor = actorHandle.get();
                            if (!actor) {
                                continue;
                            }
                            if (actor.get() != PlayerCharacter::GetSingleton() && actor.get() != target &&
                                actor->IsHostileToActor(target) && (!combatOnly || actor->IsInCombat())) {
                                actor->KillImpl(actor.get(), 0.0f, true, false);
                                actor->DecRefCount();
                            }
                        }
                    }));
        });
    }
}
