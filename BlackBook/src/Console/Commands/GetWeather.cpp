#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Skyrim;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                CommandSpec("fdge", {"GetCurrentWeather"}, [](Execution& execution) {
                    auto* target = execution.OptionalTarget<TESObjectREFR*>(PlayerCharacter::GetSingleton());
                    auto* cell = target->GetParentCell();

                    if (!cell) {
                        Print("Unable to determine parent cell.");
                        return;
                    }

                    auto* regions = cell->extraList.GetByType<ExtraRegionList>();
                    if (!regions) {
                        Print("Unable to determine parent regions.");
                        return;
                    }

                    // TODO: Get only the weather region.
                    TESWeather* currentWeather = nullptr;
                    for (auto region = regions->list->begin(); region != regions->list->end(); ++region) {
                        currentWeather = (*region)->currentWeather;
                        if (currentWeather) {
                            break;
                        }
                    }

                    if (!currentWeather) {
                        Print("Unable to determine current weather.");
                        return;
                    }

                    auto* edid = currentWeather->GetFormEditorID();
                    if (edid && edid[0] != '\0') {
                        Print("Current Weather: {:X} ({})",  currentWeather->GetFormID(), edid);
                    } else {
                        Print("Current Weather: {:X}", currentWeather->GetFormID());
                    }
                    std::string weatherType;
                    if (currentWeather->data.flags.all(TESWeather::WeatherDataFlag::kPleasant)) {
                        weatherType += "Pleasant";
                    }
                    if (currentWeather->data.flags.all(TESWeather::WeatherDataFlag::kRainy)) {
                        if (!weatherType.empty()) {
                            weatherType += ", ";
                        }
                        weatherType += "Rainy";
                    }
                    if (currentWeather->data.flags.all(TESWeather::WeatherDataFlag::kSnow)) {
                        if (!weatherType.empty()) {
                            weatherType += ", ";
                        }
                        weatherType += "Snowy";
                    }
                    if (currentWeather->data.flags.all(TESWeather::WeatherDataFlag::kCloudy)) {
                        if (!weatherType.empty()) {
                            weatherType += ", ";
                        }
                        weatherType += "Cloudy";
                    }
                    Print("Weather Type: {}", weatherType);
                }));
        });
    }
}
