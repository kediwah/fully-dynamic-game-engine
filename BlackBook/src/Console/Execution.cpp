#include <FDGE/Console/Execution.h>

using namespace FDGE;
using namespace FDGE::Console;

void Execution::NoExplicitTarget() const {
    if (!_parser.GetExplicitTarget().empty()) {
        throw std::invalid_argument("Command does not execute on a target object.");
    }
}

void Execution::NoTarget() const {
    NoExplicitTarget();
    if (RE::Console::GetSelectedRef()) {
        throw std::invalid_argument("Command does not execute on a target object "
                                    "(console has a implicit target reference).");
    }
}
