#include <FDGE/System.h>

#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <SKSE/SKSE.h>

using namespace articuno;
using namespace articuno::ryml;
using namespace FDGE;
using namespace SKSE;

namespace {
	std::filesystem::path lastSaveLoaded;
}

namespace FDGE::detail {
    class SystemManager {
    public:
        static inline System CreateSystem() noexcept {
            return System();
        }

        static void InitializeSystem() {
            static std::atomic_bool initialized;
            if (initialized.exchange(true)) {
                return;
            }
            Logger::Debug("Initializing core system...");
            GetMessagingInterface()->RegisterListener(&HandleMessage);
            Logger::Debug("Core system initialized.");
        }

        static void HandleMessage(MessagingInterface::Message* msg) {
            switch (msg->type) {
                case MessagingInterface::kDataLoaded: {
                    {
                        std::unique_lock lock(FDGE::System()._lock);
                        FDGE::System()._dataLoaded = true;
                        for (auto& callback: FDGE::System()._dataLoadedCallbacks) {
                            callback();
                        }
                        FDGE::System()._dataLoadedCallbacks.clear();
                    }
                    SkyrimPluginsLoadedEvent event;
                    FDGE::System().Emit(event);
                    break;
                }
                case MessagingInterface::kDeleteGame: {
                    SaveGameDeletingEvent event(reinterpret_cast<const char*>(msg->data));
                    FDGE::System().Emit(event);
                    break;
                }
                case MessagingInterface::kInputLoaded: {
                    InputLoadedEvent event;
                    FDGE::System().Emit(event);
                    break;
                }
                case MessagingInterface::kNewGame: {
                    NewGameEvent event;
                    FDGE::System().Emit(event);
                    break;
                }
                case MessagingInterface::kPostLoad: {
                    {
                        std::unique_lock lock(FDGE::System()._lock);
                        FDGE::System()._skseInitialized = true;
                        for (auto& callback: FDGE::System()._initCallbacks) {
                            callback();
                        }
                        FDGE::System()._initCallbacks.clear();
                    }
                    SKSEInitializedEvent event;
                    FDGE::System().Emit(event);
                    break;
                }
                case MessagingInterface::kPostPostLoad: {
                    SKSEPluginsLoadedEvent event;
                    FDGE::System().Emit(event);
                    break;
                }
                case MessagingInterface::kPostLoadGame: {
                    SaveGameLoadedEvent event(std::move(lastSaveLoaded), static_cast<bool>(msg->data));
                    FDGE::System().Emit(event);
                    break;
                }
                case MessagingInterface::kPreLoadGame: {
                    std::filesystem::path saveName = reinterpret_cast<const char*>(msg->data);
                    saveName = saveName.stem();
                    lastSaveLoaded = saveName;
                    SaveGameLoadingEvent event(std::move(saveName));
                    FDGE::System().Emit(event);
                    break;
                }
                case MessagingInterface::kSaveGame: {
                    GameSavingEvent event(reinterpret_cast<const char*>(msg->data));
                    FDGE::System().Emit(event);
                    break;
                }
            }
        }

    private:
    };
}

FDGE::detail::System& FDGE::System() noexcept {
    static FDGE::detail::System instance = FDGE::detail::SystemManager::CreateSystem();
    return instance;
}

void FDGE::detail::System::AfterInitialization(std::function<void()> callback) const {
    std::unique_lock lock(_lock);
    if (_skseInitialized) {
        Logger::Trace("Post-SKSE-initialization callback registered after initialization, running it immediately.");
        callback();
    } else {
        _initCallbacks.emplace_back(std::move(callback));
        Logger::Trace("Registered post-SKSE-initialization callback.");
    }
}

void FDGE::detail::System::AfterDataLoaded(std::function<void()> callback) const {
    std::unique_lock lock(_lock);
    if (_dataLoaded) {
        Logger::Trace("Post-data-loaded callback registered after data loaded, running it immediately.");
        callback();
    } else {
        _dataLoadedCallbacks.emplace_back(std::move(callback));
        Logger::Trace("Registered post-data-loaded callback.");
    }
}

void FDGE::detail::System::RegisterForSerde(type_registration source_reg, type_registration sink_reg) const {
    type_registry<yaml_source<>::archive_type>::get().register_type(source_reg);
    type_registry<yaml_sink<>::archive_type>::get().register_type(sink_reg);
}

EXTERN_C __declspec(dllexport) void _AfterInitialization(std::function<void()> callback) {
    FDGE::System().AfterInitialization(std::move(callback));
}

namespace {
    OnSKSELoading {
        FDGE::detail::SystemManager::InitializeSystem();
    }
}
