#include <FDGE/UI/Display.h>

#include <new>

#include <d3d11_3.h>
#include <FDGE/Hook/FunctionHook.h>
#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <QtCore/QLibraryInfo>

#include "../FDGEConfig.h"

using namespace FDGE;
using namespace FDGE::Config;
using namespace FDGE::Hook;
using namespace FDGE::UI;
using namespace RE;
using namespace REL;

namespace FDGE::UI {
    class DisplayImpl {
    public:
        static void Initialize() {
            static std::atomic_bool initialized;
            if (initialized.exchange(true)) {
                return;
            }
            if (InitializeDirect3D()) {
                InitializeQt();
            } else {
                Logger::Error("Cannot initialize Qt interface, unable to determine Direct3D device information.");
            }
        }

    private:
        static bool InitializeDirect3D() {
            auto& display = Display::GetSingleton();
            auto* renderManager = BSRenderManager::GetSingleton();

            display._swapChain = renderManager->GetRuntimeData().swapChain;
            DXGI_SWAP_CHAIN_DESC swapChainDesc;
            if (!display._swapChain || display._swapChain->GetDesc(&swapChainDesc)) {
                Logger::Error("Unable to get D3D swap chain description.");
                return false;
            }

            display._device = renderManager->GetRuntimeData().forwarder;
            display._deviceContext = renderManager->GetRuntimeData().context;
            display._bufferHeight = swapChainDesc.BufferDesc.Height;
            display._bufferWidth = swapChainDesc.BufferDesc.Width;

            return true;
        }

        static void ConfigureUIDebug() {
            if (FDGEConfig::GetProxy()->GetUI().IsDebug()) {
                _putenv_s("QT_DEBUG_PLUGINS", "1");
                _putenv_s("QML_IMPORT_TRACE", "1");

            } else {
                _putenv_s("QT_DEBUG_PLUGINS", "");
                _putenv_s("QML_IMPORT_TRACE", "");
            }
        }

        static void InitializeQt() {
            auto& display = Display::GetSingleton();
            display._renderThread = std::thread([]() {
                auto& display = Display::GetSingleton();

                SetThreadDescription(GetCurrentThread(), L"FuDGE Qt Application");

                Logger::Debug("Creating Qt application...");

                FDGEConfig::GetProxy().ListenForever([](const DataRefreshedEvent&) {
                    ConfigureUIDebug();
                });
                ConfigureUIDebug();
                qInstallMessageHandler([](QtMsgType type, const QMessageLogContext& context, const QString& msg) {
                    spdlog::level::level_enum level;
                    switch (type) {
                        case QtFatalMsg:
                            level = spdlog::level::critical;
                            break;
                        case QtCriticalMsg:
                            level = spdlog::level::err;
                            break;
                        case QtWarningMsg:
                            level = spdlog::level::warn;
                            break;
                        case QtInfoMsg:
                            level = spdlog::level::info;
                            break;
                        case QtDebugMsg:
                            level = spdlog::level::debug;
                            break;
                        default:
                            __assume(false);
                    }
                    auto str = msg.toStdString();
                    Logger::Log<char>(context.file ? context.file : "Unknown File",
                        context.function ? context.function : "Unknown Function",
                        context.line, level, std::string_view(str));
                });

                QCoreApplication::setLibraryPaths(QStringList{R"(Data\SKSE\Plugins\FullyDynamicGameEngine)"});
                int argc = 1;
                const char* argv[] = {"FullyDynamicGameEngine.dll"};
                display._application = new QGuiApplication(argc, const_cast<char**>(argv));
                QQuickWindow::setGraphicsApi(QSGRendererInterface::Direct3D11);
                Logger::Info("Qt application created.");

                Logger::Debug("Configuring Qt D3D texture rendering...");
                display._renderControl = new QQuickRenderControl();

                auto& uiConfig = FDGEConfig::GetProxy()->GetUI();
                auto width = static_cast<int>(std::min(
                    uiConfig.GetMaximumWidth().value_or(std::numeric_limits<uint32_t>::max()), display._bufferWidth));
                auto height = static_cast<int>(std::min(
                    uiConfig.GetMaximumHeight().value_or(std::numeric_limits<uint32_t>::max()), display._bufferHeight));

                auto* texture = BSRenderManager::GetSingleton()->CreateRenderTexture(width, height);
                display._uiTexture = texture->texture;
                display._renderTarget = QQuickRenderTarget::fromD3D11Texture(
                    texture->texture, QSize(width, height), uiConfig.GetSampleCount());
                display._overlayWindow = new OverlayWindow(display._renderTarget);
                Logger::Debug("Qt D3D texture rendering configured.");

                Logger::Info("Starting Qt main event loop.");
                QGuiApplication::exec();
            });
            display._renderThread.detach();
        }
    };
}

namespace {
    OnSKSELoaded {
        if (!FDGEConfig::GetProxy()->GetDebug().GetModules().IsDisplayEnabled()) {
            return;
        }

        static FunctionHook<bool(uintptr_t, uintptr_t, uintptr_t, uintptr_t*)> CreateSkyrimWindow(
            // TODO: VR Offset is 0xD6F530
            RELOCATION_ID(75595, 77226), [](auto unk0, auto unk1, auto unk2, auto* unk3) {
                auto result = CreateSkyrimWindow(unk0, unk1, unk2, unk3);
                DisplayImpl::Initialize();
                return result;
            });
    }
}

Display& Display::GetSingleton() noexcept {
    static Display instance;
    return instance;
}
