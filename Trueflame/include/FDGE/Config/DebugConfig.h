#pragma once

#include <articuno/types/auto.h>

#include "DebuggerLoggerConfig.h"
#include "EventViewerLoggerConfig.h"
#include "FileLoggerConfig.h"

namespace FDGE::Config {
    gluino_enum(DefaultLogger, uint8_t,
                (fileLogger, 0),
                (debuggerLogger, 1),
                (eventViewerLogger, 2));

    class DebugConfig {
    public:
        [[nodiscard]] inline bool IsAsyncLogging() const noexcept {
            return _asyncLogging;
        }

        inline void SetAsyncLogging(bool asyncLogging) noexcept {
            _asyncLogging = asyncLogging;
        }

        [[nodiscard]] inline uint16_t GetLoggingThreadCount() const noexcept {
            return _loggingThreadCount;
        }

        inline void SetLoggingThreadCount(uint16_t loggingThreadCount) noexcept {
            _loggingThreadCount = loggingThreadCount;
        }

        [[nodiscard]] inline uint16_t GetLoggingQueueSize() const noexcept {
            return _loggingQueueSize;
        }

        inline void SetLoggingQueueSize(uint16_t loggingQueueSize) noexcept {
            _loggingQueueSize = loggingQueueSize;
        }

        [[nodiscard]] inline const std::chrono::duration<uint64_t>& GetFlushPeriod() const noexcept {
            return _flushPeriod;
        }

        inline void SetFlushPeriod(const std::chrono::duration<uint64_t>& flushPeriod) noexcept {
            _flushPeriod = flushPeriod;
        }

        [[nodiscard]] inline DefaultLogger GetDefaultLogger() const noexcept {
            return _defaultLogger;
        }

        inline void SetDefaultLogger(DefaultLogger defaultLogger) noexcept {
            _defaultLogger = defaultLogger;
        }

        [[nodiscard]] inline bool IsPapyrusLoggingEnabled() const noexcept {
            return _enablePapyrusLogging;
        }

        inline void SetPapyrusLoggingEnabled(bool papyrusLoggingEnabled) noexcept {
            _enablePapyrusLogging = papyrusLoggingEnabled;
        }

        [[nodiscard]] inline FileLoggerConfig& GetFileLogger() noexcept {
            return _fileLogger;
        }

        [[nodiscard]] inline const FileLoggerConfig& GetFileLogger() const noexcept {
            return _fileLogger;
        }

        [[nodiscard]] inline DebuggerLoggerConfig& GetDebuggerLogger() noexcept {
            return _debuggerLogger;
        }

        [[nodiscard]] inline const DebuggerLoggerConfig& GetDebuggerLogger() const noexcept {
            return _debuggerLogger;
        }

        [[nodiscard]] inline EventViewerLoggerConfig& GetEventViewerLogger() noexcept {
            return _eventViewerLogger;
        }

        [[nodiscard]] inline const EventViewerLoggerConfig& GetEventViewerLogger() const noexcept {
            return _eventViewerLogger;
        }

    protected:
        articuno_serialize(ar) {
            ar <=> articuno::kv(_asyncLogging, "asyncLogging");
            ar <=> articuno::kv(_loggingThreadCount, "loggingThreadCount");
            ar <=> articuno::kv(_loggingQueueSize, "loggingQueueSize");
            ar <=> articuno::kv(_flushPeriod, "flushPeriod");
            ar <=> articuno::kv(_defaultLogger, "defaultLogger");
            ar <=> articuno::kv(_enablePapyrusLogging, "papyrusLoggingEnabled");
            ar <=> articuno::kv(_fileLogger, "fileLogger");
            ar <=> articuno::kv(_debuggerLogger, "debuggerLogger");
            ar <=> articuno::kv(_eventViewerLogger, "eventViewerLogger");
        }

        articuno_deserialize(ar) {
            ar <=> articuno::kv(_asyncLogging, "asyncLogging");
            ar <=> articuno::kv(_loggingThreadCount, "loggingThreadCount");
            ar <=> articuno::kv(_loggingQueueSize, "loggingQueueSize");
            ar <=> articuno::kv(_flushPeriod, "flushPeriod");
            ar <=> articuno::kv(_defaultLogger, "defaultLogger");
            ar <=> articuno::kv(_enablePapyrusLogging, "papyrusLoggingEnabled");
            ar <=> articuno::kv(_fileLogger, "fileLogger");
            ar <=> articuno::kv(_debuggerLogger, "debuggerLogger");
            ar <=> articuno::kv(_eventViewerLogger, "eventViewerLogger");
        }

    private:
        bool _asyncLogging;
        uint16_t _loggingThreadCount{1};
        uint16_t _loggingQueueSize{256};
        std::chrono::duration<uint64_t> _flushPeriod;
        DefaultLogger _defaultLogger;
        bool _enablePapyrusLogging{true};
        FileLoggerConfig _fileLogger;
        DebuggerLoggerConfig _debuggerLogger;
        EventViewerLoggerConfig _eventViewerLogger;

        friend class articuno::access;
    };
}
