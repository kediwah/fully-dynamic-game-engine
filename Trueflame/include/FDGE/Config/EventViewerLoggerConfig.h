#pragma once

#include "LoggerConfig.h"

namespace FDGE::Config {
    class EventViewerLoggerConfig : public LoggerConfig {
    public:
        inline EventViewerLoggerConfig() noexcept
                : LoggerConfig(false, log_level_type::warn, log_level_type::trace) {
        }
    };
}
