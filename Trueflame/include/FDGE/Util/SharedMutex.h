#pragma once

#include <shared_mutex>

#pragma warning(push)
#pragma warning(disable: 4251)
/**
 * A shared mutex class made for use across DLL boundaries, equivalent to <code>std::shared_mutex</code>.
 */
namespace FDGE::Util {
    class __declspec(dllexport) SharedMutex {
    public:
        SharedMutex() noexcept;

        ~SharedMutex() noexcept;

        void lock() const noexcept;

        void lock_shared() const noexcept;

        bool try_lock() const noexcept;

        bool try_lock_shared() const noexcept;

        void unlock() const noexcept;

        void unlock_shared() const noexcept;

    private:
        mutable std::shared_mutex _lock;
    };
}
#pragma warning(pop)
