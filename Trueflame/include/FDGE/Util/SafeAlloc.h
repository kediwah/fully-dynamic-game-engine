#pragma once

#include <new>

#include <heapapi.h>

namespace FDGE::Util {
    namespace {
        inline HANDLE ProcessHeap;
    }

    /**
     * An allocation function which can be used to implement a custom <code>new</code> operator for cross-DLL use.
     *
     * <p>
     * It is normally risky to pass C++ objects with automatic memory management across DLL boundaries. Memory is
     * allocated in a module's private heap by default, and other modules cannot free it. <code>SafeAlloc</code>
     * implements <code>new</code> allocation logic using the process heap, which all modules can access freely.
     * </p>
     *
     * @param size The number of bytes to allocate.
     *
     * @return A pointer to the newly-allocated memory.
     */
    [[nodiscard]] inline void* SafeAlloc(std::size_t size) {
        if (!ProcessHeap) {
            ProcessHeap = GetProcessHeap();
        }
        auto* result = HeapAlloc(ProcessHeap, HEAP_ZERO_MEMORY, size);
        if (!result) {
            throw std::bad_alloc();
        }
        return result;
    }

    /**
     * A deallocator for use on types that cross DLL-boundaries.
     *
     * <p>
     * This is meant for use in overloading <code>delete</code> operators on classes that are created in one DLL and
     * passed to another. It is to be used with types allocated with <code>SafeAlloc</code> above, and frees memory from
     * the process heap.
     * </p>
     *
     * @param object
     */
    inline void SafeFree(void* object) {
        if (!ProcessHeap) {
            ProcessHeap = GetProcessHeap();
        }
        HeapFree(ProcessHeap, 0, object);
    }
}
