#pragma once

#include <filesystem>
#include <functional>

#include <efsw/efsw.hpp>

#include "Strings.h"

namespace FDGE::Util {
    gluino_enum(FilesystemEvent, uint8_t,
        (Added, 0),
        (Removed, 1),
        (Modified, 2));

#pragma warning(push)
#pragma warning(disable: 4251)
    /**
     * Implements a filesystem watcher that is safe to initialize at any point in the SKSE lifecycle.
     *
     * <p>
     * This class is a thin wrapper around the filewatch::FileWatch class template, and adapts that template to whatever
     * encoding is used for Windows APIs at compile time. Of particular importance is that deadlocks will occur if a new
     * thread is created during SKSE initialization. Since a new thread is used to watch the filesystem, this class
     * handles delaying file watcher start until after SKSE initialization is complete. This detection requires the
     * Black Book component of FuDGE (Fully Dynamic Game Engine.dll) be present in the process. If absent, the watcher
     * will proceed to start immediately; therefore any intention to use this class without requiring Fully Dynamic Game
     * Engine.dll requires care be taken to create a watcher only after all SKSE plugins are loaded (e.g. by listening
     * for the kPostLoad message).
     * </p>
     */
    class __declspec(dllexport) FilesystemWatcher {
    public:
        /**
         * The type of the callback function on filesystem events.
         */
        using callback_type = std::function<void(const std::filesystem::path&, FilesystemEvent)>;

        /**
         * Creates a new filesystem watcher for the given path.
         *
         * <p>If the path is a path to a file then that particular file will be watched. If it is a path to a directory
         * then that directory and all subdirectories will be watched.</p>
         *
         * <p>If Fully Dynamic Game Engine.dll is present, then it will be used to determine if SKSE is still loading.
         * During SKSE loading thread creation results in a deadlock, and therefore filesystem watchers must not be
         * created. If the watcher can determine loading is still in progress the filesystem watching will be delayed
         * until after initialization completes, avoiding the deadlock. If your plugin should not require the Black Book
         * component of FuDGE, then ensure you do not create a filesystem watcher before the kPostLoad message.</p>
         *
         * @param path The path to watch.
         * @param callback The callback to invoke on filesystem events for the given path.
         * @param recursive Whether to recursively watch all subdirectories.
         */
        FilesystemWatcher(const std::filesystem::path& path, callback_type callback, bool recursive = false);

        FilesystemWatcher(const FilesystemWatcher&) = delete;

    private:
        class Listener : public efsw::FileWatchListener {
        public:
            inline explicit Listener(FilesystemWatcher::callback_type callback) noexcept
                    : _callback(std::move(callback)) {
            }

            void handleFileAction(efsw::WatchID watchID, const std::string& dir, const std::string& filename,
                                  efsw::Action action, std::string oldFilename) override;

        private:
            FilesystemWatcher::callback_type _callback;
        };

        std::unique_ptr<efsw::FileWatcher> _watcher;
        Listener _listener;
    };
#pragma warning(pop)
}
