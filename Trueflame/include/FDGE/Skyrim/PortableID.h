#pragma once

#include <RE/Skyrim.h>
#include <articuno/articuno.h>

#include "Concepts.h"

namespace FDGE::Skyrim {
    class PortableID {
       public:
        inline PortableID() noexcept = default;

        PortableID(const RE::TESForm* form);

        explicit PortableID(std::string_view editorID);

        explicit PortableID(std::initializer_list<std::string_view> editorIDs);

        explicit PortableID(const std::vector<std::string_view>& editorIDs);

        explicit PortableID(std::initializer_list<std::string>&& editorIDs);

        explicit PortableID(const std::vector<std::string>& editorIDs);

        explicit PortableID(std::string_view file, RE::FormID formID);

        explicit PortableID(std::initializer_list<std::string_view> files, RE::FormID formID);

        explicit PortableID(std::initializer_list<std::string>&& files, RE::FormID formID);

        explicit PortableID(const std::vector<std::string_view>& files, RE::FormID formID);

        explicit PortableID(const std::vector<std::string>& files, RE::FormID formID);

        explicit PortableID(std::initializer_list<std::string_view> files, std::initializer_list<RE::FormID> formIDs);

        explicit PortableID(std::initializer_list<std::string>&& files, std::initializer_list<RE::FormID> formIDs);

        explicit PortableID(const std::vector<std::string_view>& files, std::vector<RE::FormID> formIDs);

        explicit PortableID(const std::vector<std::string>& files, std::vector<RE::FormID> formIDs);

        explicit PortableID(std::string_view file, std::initializer_list<RE::FormID> formIDs);

        explicit PortableID(std::string_view file, std::vector<RE::FormID> formIDs);

        explicit PortableID(RE::FormID saveGameFormID);

        explicit PortableID(std::initializer_list<RE::FormID> saveGameFormIDs);

        explicit PortableID(std::vector<RE::FormID> saveGameFormIDs);

        [[nodiscard]] std::string ToString() const;

        [[nodiscard]] gluino::optional_ptr<RE::TESForm> Lookup(RE::FormType formType = RE::FormType::None);

        [[nodiscard]] gluino::optional_ptr<const RE::TESForm> Lookup(RE::FormType formType = RE::FormType::None) const;

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<T> Lookup() {
            return reinterpret_cast<T*>(Lookup(T::FORMTYPE).get());
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<const T> Lookup() const {
            return reinterpret_cast<const T*>(Lookup(T::FORMTYPE));
        }

        [[nodiscard]] bool IsNull() const noexcept;

        [[nodiscard]] inline bool UsesFormIDs() const noexcept { return !_ids.empty(); }

        [[nodiscard]] inline bool UsesEditorIDs() const noexcept { return !UsesFormIDs(); }

        [[nodiscard]] inline bool IsSaveGameID() const noexcept { return !_ids.empty() && _names.empty(); }

        [[nodiscard]] inline std::span<const std::string> GetFileNames() const noexcept {
            if (_ids.empty()) {
                return {};
            }
            return std::span<const std::string>(_names.data(), _names.size());
        }

        [[nodiscard]] inline std::span<const std::string> GetEditorIDs() const noexcept {
            if (!_ids.empty()) {
                return {};
            }
            return std::span<const std::string>(_names.data(), _names.size());
        }

        [[nodiscard]] inline std::span<const RE::FormID> GetFormIDs() const noexcept {
            return std::span<const RE::FormID>(_ids.data(), _ids.size());
        }

        [[nodiscard]] std::strong_ordering operator<=>(const PortableID& other) const noexcept;

        [[nodiscard]] inline bool operator==(const PortableID& other) const noexcept {
            return _names == other._names && _ids == other._ids;
        }

        [[nodiscard]] static std::optional<PortableID> Parse(std::string_view string);

       private:
        articuno_serialize(ar) {
            auto str = ToString();
            ar <=> articuno::self(str);
        }

        articuno_deserialize(ar) {
            std::string str;
            ar <=> articuno::self(str);
            std::optional<PortableID> id;
            id = Parse(str);
            if (id.has_value()) {
                *this = id.value();
            }
        }

        std::vector<std::string> _names;
        std::vector<RE::FormID> _ids;

        friend class articuno::access;
    };
}  // namespace FDGE::Skyrim

namespace std {
    [[nodiscard]] inline std::string to_string(const FDGE::Skyrim::PortableID& value) { return value.ToString(); }

    template <>
    struct hash<FDGE::Skyrim::PortableID> {
        [[nodiscard]] std::size_t operator()(const FDGE::Skyrim::PortableID& value) const noexcept;
    };
}  // namespace std

namespace articuno::serde {
    template <::articuno::serializing_archive Archive, class T>
    requires(::gluino::subtype_of<T, RE::TESForm>) void serde(Archive& ar, T*& value, articuno::value_flags flags) {
        FDGE::Skyrim::PortableID id(value);
        ar <=> articuno::self(id, flags);
    }

    template <::articuno::deserializing_archive Archive, class T>
    requires(::gluino::subtype_of<T, RE::TESForm>) void serde(Archive& ar, T*& value, articuno::value_flags flags) {
        FDGE::Skyrim::PortableID id;
        ar <=> articuno::self(id, flags);
        value = id.Lookup<T>().get_raw();
    }
}  // namespace articuno::serde
