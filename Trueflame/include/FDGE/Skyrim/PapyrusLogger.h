#pragma once

#include <RE/Skyrim.h>

#include "../Util/Maps.h"

namespace FDGE::Skyrim {
    class PapyrusLogger : public RE::BSTEventSink<RE::BSScript::LogEvent> {
    public:
        using level_type = RE::BSScript::ErrorLogger::Severity;

        RE::BSEventNotifyControl ProcessEvent(
                const RE::BSScript::LogEvent* event, RE::BSTEventSource<RE::BSScript::LogEvent>*) override;

        [[nodiscard]] inline bool IsEnabled() const noexcept {
            return _enabled;
        }

        inline void SetEnabled(bool enabled) noexcept {
            _enabled = enabled;
        }

        inline bool RegisterPapyrusClass(std::string_view papyrusClasses) {
            return _papyrusClasses.emplace(papyrusClasses).second;
        }

    private:
        volatile bool _enabled{true};
        Util::istr_parallel_flat_hash_set<std::string_view> _papyrusClasses{};
    };
}
