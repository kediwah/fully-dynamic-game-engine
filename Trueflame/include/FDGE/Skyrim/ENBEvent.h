#pragma once

#include <gluino/enumeration.h>

namespace FDGE::Skyrim {
    gluino_enum(ENBEventType, long,
                (EndFrame, 1),
                (BeginFrame, 2),
                (PreSave, 3),
                (PostLoad, 4),
                (OnInit, 5),
                (OnExit, 6),
                (PreReset, 7),
                (PostReset, 8));

    class ENBEvent {
    public:
        inline explicit ENBEvent(ENBEventType type) noexcept
                : _type(type) {
        }

        [[nodiscard]] inline ENBEventType GetType() const noexcept {
            return _type;
        }

    private:
        ENBEventType _type;
    };
}
