#include <FDGE/Skyrim/ENB.h>

using namespace FDGE::Skyrim;

namespace {
    gluino_enum(ENBState, long,
                (IsEditorActive, 1),
                (IsEffectsWindowActive, 2),
                (CursorPositionX, 3),
                (CursorPositionY, 4),
                (IsMouseLeftButtonPressed, 5),
                (IsMouseRightButtonPressed, 6),
                (IsMouseMiddleButtonPressed, 7));

    class DefaultENB : public ENB {
    public:
        inline DefaultENB() noexcept {
            LinkFunctions();
        }

        [[nodiscard]] std::optional<ENB::version_type> GetSDKVersion() const noexcept override {
            return _sdkVersion;
        }

        [[nodiscard]] std::optional<ENB::version_type> GetVersion() const noexcept override {
            return _version;
        }

        [[nodiscard]] gluino::optional_ptr<ENBRenderInfo> GetRenderInfo() const noexcept override {
            return _getRenderInfoFunction();
        }

        [[nodiscard]] bool IsEnabled() const noexcept override {
            if (!_getParameterFunction) {
                return false;
            }
            ENBParameter param;
            _getParameterFunction("enbseries.ini", "GLOBAL", "UseEffect", &param);
            return param.GetBoolean().value_or(false);
        }

        [[nodiscard]] bool SetEnabled(bool enabled) override {
            if (!_setParameterFunction) {
                return false;
            }
            ENBParameter param(enabled);
            return _setParameterFunction("enbseries.ini", "GLOBAL", "UseEffect", &param);
        }

        [[nodiscard]] bool IsEditorActive() const noexcept override {
            return _getStateFunction(ENBState::IsEditorActive);
        }

        [[nodiscard]] bool IsEffectsWindowActive() const noexcept override {
            return _getStateFunction(ENBState::IsEffectsWindowActive);
        }

        [[nodiscard]] position_type GetCursorXPosition() const noexcept override {
            return _getStateFunction(ENBState::CursorPositionX);
        }

        [[nodiscard]] position_type GetCursorYPosition() const noexcept override {
            return _getStateFunction(ENBState::CursorPositionY);
        }

        [[nodiscard]] std::pair<position_type, position_type> GetCursorPosition() const noexcept override {
            return {_getStateFunction(ENBState::CursorPositionX), _getStateFunction(ENBState::CursorPositionY)};
        }

        [[nodiscard]] bool IsLeftMouseButtonPressed() const noexcept override {
            return _getStateFunction(ENBState::IsMouseLeftButtonPressed);
        }

        [[nodiscard]] bool IsMiddleMouseButtonPressed() const noexcept override {
            return _getStateFunction(ENBState::IsMouseMiddleButtonPressed);
        }

        [[nodiscard]] bool IsRightMouseButtonPressed() const noexcept override {
            return _getStateFunction(ENBState::IsMouseRightButtonPressed);
        }

        [[nodiscard]] bool ReadParameter(std::string_view fileName, std::string_view category, std::string_view key,
                                         ENBParameter& out) const noexcept override {
            if (!_sdkVersion.has_value()) {
                return false;
            }
            return _getParameterFunction(fileName.data(), category.data(), key.data(), &out);
        }

        [[nodiscard]] bool WriteParameter(std::string_view fileName, std::string_view category, std::string_view key,
                                         const ENBParameter& value) noexcept override {
            if (!_sdkVersion.has_value()) {
                return false;
            }
            return _setParameterFunction(fileName.data(), category.data(), key.data(), &value);
        }

    private:
        static void HandleCallback(ENBEventType eventType) noexcept {
            ENBEvent event(eventType);
            dynamic_cast<DefaultENB&>(ENB::GetSingleton()).Emit(event);
        }

        static ENBRenderInfo* GetZero() noexcept {
            return nullptr;
        }

        static long GetZero(ENBState) noexcept {
            return 0;
        }

        void LinkFunctions() noexcept {
            DWORD sizeNeeded = 0;
            HANDLE process = GetCurrentProcess();
            EnumProcessModules(process, nullptr, 0, &sizeNeeded);
            auto moduleCount = sizeNeeded / sizeof(HMODULE);
            auto* modules = new HMODULE[moduleCount];
            memset(modules, 0, sizeNeeded);
            HMODULE enbModule = nullptr;
            ENB::version_type(*enbGetSDKVersion)() = nullptr;
            if (EnumProcessModules(process, modules, sizeNeeded, &sizeNeeded)) {
                for (long i = 0; i < moduleCount; i++) {
                    if (modules[i] == nullptr) {
                        break;
                    }
                    enbGetSDKVersion = reinterpret_cast<decltype(enbGetSDKVersion)>(
                            GetProcAddress(modules[i], "ENBGetSDKVersion"));
                    if (enbGetSDKVersion) {
                        enbModule = modules[i];
                        break;
                    }
                }
            }
            delete[] modules;
            if (!enbModule) {
                return;
            }

            auto* enbGetVersion = reinterpret_cast<ENB::version_type(*)()>(
                    GetProcAddress(enbModule, "ENBGetVersion"));
            auto* enbGetGameIdentifier = reinterpret_cast<int32_t(*)()>(
                    GetProcAddress(enbModule, "ENBGetGameIdentifier"));
            auto* enbSetCallbackFunction = reinterpret_cast<void(*)(void(*)(ENBEventType))>(
                    GetProcAddress(enbModule, "ENBSetCallbackFunction"));
            _getParameterFunction = reinterpret_cast<decltype(_getParameterFunction)>(
                    GetProcAddress(enbModule, "ENBGetParameter"));
            _setParameterFunction = reinterpret_cast<decltype(_setParameterFunction)>(
                    GetProcAddress(enbModule, "ENBSetParameter"));
            _getStateFunction = reinterpret_cast<decltype(_getStateFunction)>(
                    GetProcAddress(enbModule, "ENBGetState"));
            _getRenderInfoFunction = reinterpret_cast<decltype(_getRenderInfoFunction)>(
                    GetProcAddress(enbModule, "ENBGetRenderInfo"));

            if (enbGetSDKVersion && enbGetVersion && enbGetGameIdentifier && enbSetCallbackFunction &&
                    _getParameterFunction && _setParameterFunction && _getStateFunction && _getRenderInfoFunction) {
                _sdkVersion = enbGetSDKVersion();
                _version = enbGetVersion();
                enbSetCallbackFunction(HandleCallback);
            }
        }

        std::optional<ENB::version_type> _sdkVersion;
        std::optional<ENB::version_type> _version;
        bool(*_getParameterFunction)(const char*, const char*, const char*, ENBParameter*){nullptr};
        bool(*_setParameterFunction)(const char*, const char*, const char*, const ENBParameter*){nullptr};
        long(*_getStateFunction)(ENBState){GetZero};
        ENBRenderInfo*(*_getRenderInfoFunction)(){GetZero};
    };
}

ENB& ENB::GetSingleton() noexcept {
    static DefaultENB enb;
    return enb;
}
