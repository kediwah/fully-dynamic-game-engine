#include <FDGE/Util/Mutex.h>

using namespace FDGE::Util;

Mutex::Mutex() noexcept {
}

Mutex::~Mutex() noexcept {
}

void Mutex::lock() const noexcept {
    return _lock.lock();
}

bool Mutex::try_lock() const noexcept {
    return _lock.try_lock();
}

void Mutex::unlock() const noexcept {
    return _lock.unlock();
}
