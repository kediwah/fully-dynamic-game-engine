#include <FDGE/Util/RecursiveTimedMutex.h>

using namespace FDGE::Util;

RecursiveTimedMutex::RecursiveTimedMutex() noexcept {
}

RecursiveTimedMutex::~RecursiveTimedMutex() noexcept {
}

void RecursiveTimedMutex::lock() const noexcept {
    return _lock.lock();
}

bool RecursiveTimedMutex::try_lock() const noexcept {
    return _lock.try_lock();
}

bool RecursiveTimedMutex::try_lock_for_impl(const std::chrono::milliseconds& timeout_duration) {
    return _lock.try_lock_for(timeout_duration);
}

bool RecursiveTimedMutex::try_lock_until_impl(
        const std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>& timeout_time) {
    return _lock.try_lock_until(timeout_time);
}

void RecursiveTimedMutex::unlock() const noexcept {
    return _lock.unlock();
}
