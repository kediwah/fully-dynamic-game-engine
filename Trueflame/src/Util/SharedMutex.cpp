#include <FDGE/Util/SharedMutex.h>

using namespace FDGE::Util;

SharedMutex::SharedMutex() noexcept {
}

SharedMutex::~SharedMutex() noexcept {
}

void SharedMutex::lock() const noexcept {
    return _lock.lock();
}

void SharedMutex::lock_shared() const noexcept {
    return _lock.lock_shared();
}

bool SharedMutex::try_lock() const noexcept {
    return _lock.try_lock();
}

bool SharedMutex::try_lock_shared() const noexcept {
    return _lock.try_lock_shared();
}

void SharedMutex::unlock() const noexcept {
    return _lock.unlock();
}

void SharedMutex::unlock_shared() const noexcept {
    return _lock.unlock_shared();
}
