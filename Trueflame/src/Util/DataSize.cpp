#include <FDGE/Util/DataSize.h>

using namespace FDGE::Util;

DataSize::DataSize(std::string_view string) {
    std::stringstream value;
    value << string;
    value.seekg(0, std::ios::beg);
    uint64_t num;
    value >> num;
    if (value.eof()) {
        _bytes = num;
        return;
    }
    std::string suffix;
    value >> suffix;
    std::transform(suffix.begin(), suffix.end(), suffix.begin(),
                   [](char in) { return static_cast<char>(std::toupper(in)); });
    if (suffix == "B") {
        _bytes = num;
    } else if (suffix == "KB") {
        _bytes = num * 1000;
    } else if (suffix == "MB") {
        _bytes = num * 1000 * 1000;
    } else if (suffix == "GB") {
        _bytes = num * 1000 * 1000 * 1000;
    } else if (suffix == "TB") {
        _bytes = num * 1000 * 1000 * 1000 * 1000;
    } else if (suffix == "PB") {
        _bytes = num * 1000 * 1000 * 1000 * 1000 * 1000;
    } else if (suffix == "EB") {
        _bytes = num * 1000 * 1000 * 1000 * 1000 * 1000 * 1000;
    } else if (suffix == "ZB") {
        _bytes = num * 1000 * 1000 * 1000 * 1000 * 1000 * 1000 * 1000;
    } else if (suffix == "YB") {
        _bytes = num * 1000 * 1000 * 1000 * 1000 * 1000 * 1000 * 1000 * 1000;
    } else if (suffix == "KIB") {
        _bytes = num * 1024;
    } else if (suffix == "MIB") {
        _bytes = num * 1024 * 1024;
    } else if (suffix == "GIB") {
        _bytes = num * 1024 * 1024 * 1024;
    } else if (suffix == "TIB") {
        _bytes = num * 1024 * 1024 * 1024 * 1024;
    } else if (suffix == "PIB") {
        _bytes = num * 1024 * 1024 * 1024 * 1024 * 1024;
    } else if (suffix == "EIB") {
        _bytes = num * 1024 * 1024 * 1024 * 1024 * 1024 * 1024;
    } else if (suffix == "ZIB") {
        _bytes = num * 1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024;
    } else if (suffix == "YIB") {
        _bytes = num * 1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024;
    } else {
        throw std::invalid_argument(fmt::format("Data size unit '{}' is not valid.", suffix));
    }
}

std::string std::to_string(DataSize value) noexcept {
    auto kibibytes = value.ToBytes() / 1024;
    if (value.ToBytes() == kibibytes * 1024) {
        auto mibibytes = kibibytes / 1024;
        if (kibibytes == mibibytes * 1024) {
            auto gibibytes = mibibytes / 1024;
            if (mibibytes == gibibytes * 1024) {
                return std::to_string(gibibytes) + " GiB";
            } else {
                return std::to_string(mibibytes) + " MiB";
            }
        } else {
            return std::to_string(kibibytes) + " KiB";
        }
    } else {
        auto kilobytes =  value.ToBytes() / 1000;
        if ( value.ToBytes() == kilobytes * 1000) {
            auto megabytes = kilobytes / 1000;
            if (kilobytes == megabytes * 1000) {
                auto gigabytes = megabytes / 1000;
                if (megabytes == gigabytes * 1000) {
                    return std::to_string(gigabytes) + " GB";
                } else {
                    return std::to_string(megabytes) + " MB";
                }
            } else {
                return std::to_string(kilobytes) + " KB";
            }
        } else {
            return std::to_string(value.ToBytes()) + " B";
        }
    }
}
