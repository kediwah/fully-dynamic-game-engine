cmake_minimum_required(VERSION 3.18)

set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>" CACHE STRING "")
set(CMAKE_TOOLCHAIN_FILE "$ENV{VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake" CACHE STRING "")
set(VCPKG_TARGET_TRIPLET "x64-windows-static" CACHE STRING "")

# ---- Project ----

project(
        FullyDynamicGameEnginePython
        VERSION 1.0.0
        LANGUAGES CXX
)

configure_file(
        ${CMAKE_CURRENT_SOURCE_DIR}/version.rc.in
        ${CMAKE_CURRENT_BINARY_DIR}/version.rc
        @ONLY
)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# ---- Include guards ----

if (PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
    message(
            FATAL_ERROR
            "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there."
    )
endif ()

# ---- Google Test ----

include(FetchContent)
FetchContent_Declare(
        googletest
        URL https://github.com/google/googletest/archive/609281088cfefc76f9d0ce82e1ff6c30cc3591e5.zip
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

# ---- Dependencies ----

set(CMAKE_INTERPROCEDURAL_OPTIMIZATION ON)
set(CMAKE_INTERPROCEDURAL_OPTIMIZATION_DEBUG OFF)

set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_STATIC_RUNTIME ON CACHE BOOL "")
set(Boost_USE_DEBUG_RUNTIME ON)

add_compile_definitions(
        BOOST_BIND_GLOBAL_PLACEHOLDERS
        UNICODE
        _UNICODE
)

add_compile_definitions(FDGE_USE_CITYHASH)
add_compile_definitions(CEREAL_THREAD_SAFE)

if (WIN32)
    add_compile_definitions(NOMINMAX)
    add_compile_definitions(WIN32_LEAN_AND_MEAN)
endif()

if (MSVC)
    add_compile_definitions(_CRT_USE_BUILTIN_OFFSETOF)

    set(COMPILE_OPTIONS_DEBUG
            /Zc:inline- # Keep unreferenced COMDAT
            /JMC # Just My Code debugging
            )

    set(COMPILE_OPTIONS_RELEASE
            /Zc:inline # Remove unreferenced COMDAT
            /JMC- # Disable Just My Code debugging
            )

    add_compile_options(
            /Gy # Enable Function-Level Linking
            /Gr # Enable RTTI
            /MP # Build with Multiple Processes
            /Oi # Generate Intrinsic Functions
            /sdl # Enable Additional Security Checks
            /Zi # Debug Information Format

            /permissive- # Standards conformance

            /Zc:alignedNew # C++17 over-aligned allocation
            /Zc:auto # Deduce Variable Type
            /Zc:char8_t
            /Zc:__cplusplus # Enable updated __cplusplus macro
            /Zc:externC
            /Zc:externConstexpr # Enable extern constexpr variables
            /Zc:forScope # Force Conformance in for Loop Scope
            /Zc:hiddenFriend
            /Zc:implicitNoexcept # Implicit Exception Specifiers
            /Zc:lambda
            /Zc:noexceptTypes # C++17 noexcept rules
            /Zc:preprocessor	# Enable preprocessor conformance mode
            /Zc:referenceBinding # Enforce reference binding rules
            /Zc:rvalueCast # Enforce type conversion rules
            /Zc:sizedDealloc # Enable Global Sized Deallocation Functions
            /Zc:strictStrings # Disable string literal type conversion
            /Zc:ternary # Enforce conditional operator rules
            /Zc:threadSafeInit # Thread-safe Local Static Initialization
            /Zc:tlsGuards
            /Zc:trigraphs # Trigraphs Substitution
            /Zc:wchar_t # wchar_t Is Native Type

            /experimental:external
            /external:anglebrackets
            /external:W0

            /bigobj

            /W4 # Warning level (all warnings)
            #/WX # Warning level (warnings are errors)

            "$<$<CONFIG:Debug>:${COMPILE_OPTIONS_DEBUG}>"
            "$<$<CONFIG:Release>:${COMPILE_OPTIONS_RELEASE}>"
    )

    set(LINK_OPTIONS_DEBUG
            /INCREMENTAL # Link Incrementally
            /OPT:NOREF # Optimizations (keep functions/data never referenced)
            /OPT:NOICF # Optimizations (prevent identical COMDAT folding)
            )

    set(LINK_OPTIONS_RELEASE
            /INCREMENTAL:NO # Link Incrementally
            /OPT:REF # Optimizations (eliminate functions/data never referenced)
            /OPT:ICF # Optimizations (perform identical COMDAT folding)
            )

    add_link_options(
            "$<$<CONFIG:Debug>:${LINK_OPTIONS_DEBUG}>"
            "$<$<CONFIG:Release>:${LINK_OPTIONS_RELEASE}>"
    )
endif ()

FetchContent_Declare(
        gluino
        GIT_REPOSITORY "file:///C:/Users/John Stewart/Development/gluino"
        GIT_TAG "fd4d5ad2a34465594ea4aa4952e7f1a8f96df808"
)
FetchContent_Declare(
        articuno
        GIT_REPOSITORY "file:///C:/Users/John Stewart/Development/articuno"
        GIT_TAG "e5d1ab388cedc79a1630053f23123cbe81158e60"
)
FetchContent_Declare(
        CommonLibSSE
        GIT_REPOSITORY "https://github.com/Ryan-rsm-McKenzie/CommonLibSSE.git"
        GIT_TAG "84738878bc33a445edb834b008cc882380455243"
)
FetchContent_MakeAvailable(gluino articuno CommonLibSSE)
find_package(spdlog REQUIRED)
find_package(fmt REQUIRED)
find_package(Boost COMPONENTS iostreams locale REQUIRED)
find_package(ZLIB REQUIRED)
find_package(Python3 COMPONENTS Interpreter Development REQUIRED)
find_package(pybind11 CONFIG REQUIRED)
find_package(cityhash CONFIG REQUIRED)

# ---- Add source files ----

set(headers
        include/FDGE/FDGE.h
        )

set(sources
        src/Main.cpp
        src/Binding/Papyrus/ActorBaseExtension.cpp
        src/Binding/Papyrus/ActorExtension.cpp
        src/Binding/Papyrus/ArrayExtension.cpp
        src/Binding/Papyrus/CellExtension.cpp
        src/Binding/Papyrus/EncounterZoneExtension.cpp
        src/Binding/Papyrus/FormExtension.cpp
        src/Binding/Papyrus/LocationExtension.cpp
        src/Binding/Papyrus/MathExtension.cpp
        src/Binding/Papyrus/ManagedObject.cpp
        src/Binding/Papyrus/ManagedStore.cpp
        src/Binding/Papyrus/Papyrus.cpp
        src/Binding/Papyrus/PapyrusEventDispatcher.cpp
        src/Binding/Papyrus/RegionExtension.cpp
        src/Binding/Papyrus/Scripting.cpp
        src/Binding/Papyrus/StringExtension.cpp
        src/Binding/Papyrus/WorldSpaceExtension.cpp
        src/Hooks/CallHook.cpp
        src/Hooks/FunctionHook.cpp
        src/Skyrim/ActorBaseExtension.cpp
        src/Skyrim/ActorExtension.cpp
        src/Skyrim/CellExtension.cpp
        src/Skyrim/EncounterZoneExtension.cpp
        src/Skyrim/EventSource.cpp
        src/Skyrim/ExtensionProperty.cpp
        src/Skyrim/FileExtension.cpp
        src/Skyrim/FormExtension.cpp
        src/Skyrim/FormIndex.cpp
        src/Skyrim/FormReference.cpp
        src/Skyrim/Hooks.cpp
        src/Skyrim/LocationExtension.cpp
        src/Skyrim/RegionExtension.cpp
        src/Skyrim/SKSEExtension.cpp
        src/Skyrim/WorldSpaceExtension.cpp
        )

set(tests
        )

source_group(
        TREE ${CMAKE_CURRENT_SOURCE_DIR}
        FILES
        ${headers}
        ${sources}
)

# ---- Create DLL ----

add_library(${PROJECT_NAME} SHARED
        ${headers}
        ${sources}
        ${CMAKE_CURRENT_BINARY_DIR}/version.rc
        .clang-format
        )

target_compile_features(${PROJECT_NAME}
        PUBLIC
        cxx_std_20
        )

target_compile_options(${PROJECT_NAME}
        PRIVATE
        "$<$<BOOL:${MSVC}>:/TP>"
        /std:c++latest
        )

target_include_directories(${PROJECT_NAME}
        PRIVATE
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
        $<INSTALL_INTERFACE:src>
        )

target_include_directories(${PROJECT_NAME}
        PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        )

target_link_libraries(${PROJECT_NAME}
        PUBLIC
	FullyDynamicGameEngineCore
        CommonLibSSE::CommonLibSSE
        Boost::algorithm
        Boost::fusion
        spdlog::spdlog
        cityhash
        articuno
        gluino
        )

target_link_libraries(${PROJECT_NAME}
        PRIVATE
        Python3::Python
        pybind11::lto
        pybind11::embed
        pybind11::module
        )

target_link_options(${PROJECT_NAME}
        PUBLIC
        ${LINK_OPTIONS_${CONFIG}}
        )

target_precompile_headers(${PROJECT_NAME}
        PRIVATE
        src/PCH.h
        )

if (WIN32)
    if (CMAKE_BUILD_TYPE STREQUAL "Debug")
        message("Building on debug mode.")
        if (DEFINED ENV{FuDGEDebugTarget})
            message("Adding debug build copy target.")
            add_custom_command(TARGET FullyDynamicGameEngine POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:FullyDynamicGameEngine> $ENV{FuDGEDebugTarget}
            )
            add_custom_command(TARGET FullyDynamicGameEngine POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_PDB_FILE:FullyDynamicGameEngine> $ENV{FuDGEDebugTarget}
            )
        endif()
    else()
        if (DEFINED ENV{FuDGEReleaseTarget})
            message("Adding release build copy target.")
            add_custom_command(TARGET FullyDynamicGameEngine POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:FullyDynamicGameEngine> $ENV{FuDGEReleaseTarget}
            )
            add_custom_command(TARGET FullyDynamicGameEngine POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_PDB_FILE:FullyDynamicGameEngine> $ENV{FuDGEReleaseTarget}
            )
        endif()
    endif()
endif()

# ---- Unit tests ----

enable_testing()

add_executable(
        ${PROJECT_NAME}_tests
        ${headers}
        ${tests}
)

target_compile_features(${PROJECT_NAME}_tests
        PUBLIC
        cxx_std_20
        )

target_compile_options(${PROJECT_NAME}_tests
        PRIVATE
        "$<$<BOOL:${MSVC}>:/TP>"
        /std:c++latest
        )

target_link_options(${PROJECT_NAME}_tests
        PUBLIC
        ${LINK_OPTIONS_${CONFIG}}
        )

target_link_libraries(
        ${PROJECT_NAME}_tests
        ${PROJECT_NAME}
        gtest_main
)

include(GoogleTest)
gtest_discover_tests(${PROJECT_NAME}_tests)
