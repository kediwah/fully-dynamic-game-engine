#pragma once

#include <FDGE/BlackBook.h>

#include "Console/Command.h"
#include "Console/DynamicCommand.h"
#include "Console/StaticCommand.h"

#include "Hook/LambdaSerializationHook.h"
#include "Hook/SerializationHook.h"
