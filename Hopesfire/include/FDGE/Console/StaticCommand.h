#pragma once

#include <FDGE/Console/CommandRepository.h>
#include <FDGE/System.h>

#include "Command.h"

namespace FDGE::Console {
    template <class T>
    class StaticCommand : public Command {
    public:
        [[nodiscard]] std::string_view GetNamespace() const noexcept override {
            return _ns;
        }

        [[nodiscard]] std::span<const std::string> GetNames() const noexcept override {
            return std::span<const std::string>(_names.data(), _names.size());
        }

        virtual void Execute(const Execution& Context) const = 0;

    protected:
        template <class... Args>
        requires (std::same_as<Args, std::string_view> && ...)
        explicit StaticCommand(std::string_view ns, Args... names) : _ns(ns.data()) {
            _names.reserve(sizeof...(names));
            for (auto& entry : {names...}) {
                _names.emplace_back(entry.data());
            }
        }

    private:
        struct Registration {
            inline Registration() noexcept {
                static T command;
                static Util::EventRegistration listener = System().Listen([&](const SkyrimPluginsLoadedEvent& e) {
                    if (!e.IsReload()) {
                        CommandRepository::GetSingleton().RegisterForever(
                                CommandSpec(command.GetNamespace(), command.GetNames(), [this](Execution& execution) {
                                    command.Execute(execution);
                                }));
                        listener = {};
                    }
                });
            }
        };

        static inline Registration _registration;

        std::string _ns;
        std::vector<std::string> _names;
    };
}

#define __FDGECommandStringify(x, c) ::std::string_view(#x)

#define __FDGECommand(className, ns, name, ...) class className : public ::FDGE::Console::StaticCommand<className> { \
public:                                                                                                              \
    inline className() noexcept : FDGE::Console::StaticCommand<className>(                                           \
        ::std::string_view(#ns), ::std::string_view(#name) __VA_OPT__(,) GLUINO_MAP(__FDGECommandStringify, , __VA_ARGS__) ) {}    \
                                                                                                                     \
    void Execute(const ::FDGE::Console::Execution& Context) const override;                                          \
};                                                                                                                   \
void className::Execute([[maybe_unused]] const ::FDGE::Console::Execution& Context) const

#define FDGECommand(ns, name, ...) __FDGECommand(__FDGECommand_ ## ns ## _ ## name, ns, name, __VA_ARGS__)
